/*
Navicat MariaDB Data Transfer

Source Server         : viettel-edms
Source Server Version : 100316
Source Host           : 10.30.1.38:8005
Source Database       : MoneyUp

Target Server Type    : MariaDB
Target Server Version : 100316
File Encoding         : 65001

Date: 2021-01-23 09:50:30
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for Authority
-- ----------------------------
DROP TABLE IF EXISTS `Authority`;
CREATE TABLE `Authority` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `Code` varchar(20) COLLATE utf8mb4_vietnamese_ci DEFAULT NULL,
  `Name` varchar(100) COLLATE utf8mb4_vietnamese_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=60 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_vietnamese_ci;

-- ----------------------------
-- Records of Authority
-- ----------------------------
INSERT INTO `Authority` VALUES ('46', 'TL0000', 'Default');
INSERT INTO `Authority` VALUES ('47', 'TL0001', 'Dashboard');
INSERT INTO `Authority` VALUES ('48', 'TL0002', 'Quản lí chức vụ');
INSERT INTO `Authority` VALUES ('49', 'TL0003', 'Quản lí tài khoản Admin');
INSERT INTO `Authority` VALUES ('50', 'TL0004', 'Quản lí địa chỉ');
INSERT INTO `Authority` VALUES ('51', 'TL0005', 'Quản lí giao dịch');
INSERT INTO `Authority` VALUES ('52', 'TL0006', 'Quản lí kênh');
INSERT INTO `Authority` VALUES ('53', 'TL0007', 'Quản lí video');
INSERT INTO `Authority` VALUES ('54', 'TL0008', 'Quản lí sản phẩm');
INSERT INTO `Authority` VALUES ('55', 'TL0009', 'Quản lí ngân hàng');
INSERT INTO `Authority` VALUES ('56', 'TL0010', 'Quản lí  người dùng');
INSERT INTO `Authority` VALUES ('57', 'TL0011', 'Quản lí menu');
INSERT INTO `Authority` VALUES ('58', 'TL0012', 'Default-Admin');
INSERT INTO `Authority` VALUES ('59', 'TL0013', 'Quản lí sản phẩm');

-- ----------------------------
-- Table structure for Batkiemtien
-- ----------------------------
DROP TABLE IF EXISTS `Batkiemtien`;
CREATE TABLE `Batkiemtien` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `Username` varchar(255) DEFAULT NULL,
  `Sotien` decimal(10,0) DEFAULT NULL,
  `Sotaikhoan` varchar(255) DEFAULT NULL,
  `Nganhang` varchar(255) DEFAULT NULL,
  `CreateDate` datetime DEFAULT NULL,
  `TenChuThe` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of Batkiemtien
-- ----------------------------
INSERT INTO `Batkiemtien` VALUES ('3', 'longchin', '200000', '123321890', null, '2020-09-14 11:18:37', 'LE VAN LONG');
INSERT INTO `Batkiemtien` VALUES ('4', 'huyennguyen', '200000', '0989080867686', null, '2020-09-14 11:21:44', 'NGUYEN THI HUYEN');
INSERT INTO `Batkiemtien` VALUES ('5', 'ngocle405', '200000', '100345324343', null, '2020-09-14 11:37:19', 'LE THANH NGOC');
INSERT INTO `Batkiemtien` VALUES ('6', 'nhole', '200000', '123456654123', null, '2020-11-02 15:55:17', 'lê ngọc');

-- ----------------------------
-- Table structure for CaiDatChung
-- ----------------------------
DROP TABLE IF EXISTS `CaiDatChung`;
CREATE TABLE `CaiDatChung` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `Name` varchar(255) COLLATE utf8mb4_vietnamese_ci NOT NULL DEFAULT '',
  `Content` text COLLATE utf8mb4_vietnamese_ci DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_vietnamese_ci;

-- ----------------------------
-- Records of CaiDatChung
-- ----------------------------
INSERT INTO `CaiDatChung` VALUES ('2', 'Logo_TrangChu', '/Images/images/120291115_2730713790517953_7098160643787802967_n.jpg');
INSERT INTO `CaiDatChung` VALUES ('3', 'Gioithieu', 'Kenhkiemtienonline.com là nền tảng giúp cho các thành viên tận dụng thời gian rãnh rỗi của mình để kiếm thêm thu nhập bằng cách thực hiện một số nhiệm vụ đơn giản như đăng ký tài khoản, xem quảng cáO...');
INSERT INTO `CaiDatChung` VALUES ('4', 'Email_1', 'lvlong96k@gmail.com');
INSERT INTO `CaiDatChung` VALUES ('5', 'Email_2', null);
INSERT INTO `CaiDatChung` VALUES ('6', 'Email_3', null);
INSERT INTO `CaiDatChung` VALUES ('7', 'Hotline_1', 'longchin196');
INSERT INTO `CaiDatChung` VALUES ('8', 'Hotline_2', '12345678');
INSERT INTO `CaiDatChung` VALUES ('9', 'Hotline_3', null);
INSERT INTO `CaiDatChung` VALUES ('10', 'LinkFanpage', null);
INSERT INTO `CaiDatChung` VALUES ('11', 'LinkZalo', null);
INSERT INTO `CaiDatChung` VALUES ('12', 'LinkInstagram', null);
INSERT INTO `CaiDatChung` VALUES ('13', 'LinkYoutube', null);
INSERT INTO `CaiDatChung` VALUES ('14', 'LinkGoogleAnalytic', null);
INSERT INTO `CaiDatChung` VALUES ('15', 'LinkGoogleMaster', null);
INSERT INTO `CaiDatChung` VALUES ('16', 'AnhQR_1', '/Images/images/long1.jpg');
INSERT INTO `CaiDatChung` VALUES ('17', 'AnhQR_2', null);
INSERT INTO `CaiDatChung` VALUES ('18', 'AnhQR_3', null);
INSERT INTO `CaiDatChung` VALUES ('19', 'TruSo', null);
INSERT INTO `CaiDatChung` VALUES ('21', 'ggMap', null);

-- ----------------------------
-- Table structure for ChannelsConnect
-- ----------------------------
DROP TABLE IF EXISTS `ChannelsConnect`;
CREATE TABLE `ChannelsConnect` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `AccessToken` text DEFAULT NULL,
  `RefershToken` text DEFAULT NULL,
  `ChannelsID` text DEFAULT NULL,
  `ChannelsTitle` varchar(255) DEFAULT NULL,
  `UpdateDate` datetime DEFAULT NULL,
  `isActive` bit(1) DEFAULT b'0',
  `ProjectID` int(11) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `ForeignKeyMyProject` (`ProjectID`),
  CONSTRAINT `ForeignKeyMyProject` FOREIGN KEY (`ProjectID`) REFERENCES `MyProject` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of ChannelsConnect
-- ----------------------------
INSERT INTO `ChannelsConnect` VALUES ('4', 'ya29.a0AfH6SMB9tmCtaM-hKqGuL5Jux4rtR9rdyharKchfrzNr_0wBOC8QXtOrAQza7ehomQ_5Am6diZaLXRLW5LXZRevwoVqYVtvzJB_5XZ67mgpMmWZAOBSpoQ5hFY0A3GwCpwCgOUKtEttKcEBhfVqsgj-W463QztB-Byc', '1//0emSJvpmtnPwICgYIARAAGA4SNwF-L9IrbzavdQfqpYx_opEKILv-7PkiFCgtnpl0NnVLX8wV8hrqBbNJ4n7GeOGQ7I23yE5mBkc', 'UCX-N1_F1soE1flx6XP3xQBA', 'le long', '2020-11-02 15:59:37', '', '1');
INSERT INTO `ChannelsConnect` VALUES ('5', 'ya29.A0AfH6SMCQ4-ia5rgCUuW6L5T_BTlxyDYzyfhZO9LeBDfUzgoYUsdeFJ6Z8yY9b3y43TPRMmPWhHprFOwdIvyy58oRzkGvalyLuP-qzndFXk_CTbpdps4SGn_a63zaPCwgqlmsRPNccUNDIaa-cC5bhyBJhsxLhD7q6mXaInkgBe-X', '1//0exxL6tCUzPjjCgYIARAAGA4SNwF-L9IrczQUZroaW-7NNLXMKTW96-0zU1LzoMfmWGRtXgb3Dgd8XIKJfqkiCtJjyBrZyXAId1Y', 'UCFsKuWzh0TmAdfhQ7djM6xA', 'lv LloOng', '2020-11-02 15:10:38', '', '1');

-- ----------------------------
-- Table structure for ChannelsToUser
-- ----------------------------
DROP TABLE IF EXISTS `ChannelsToUser`;
CREATE TABLE `ChannelsToUser` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `UserID` bigint(20) DEFAULT NULL,
  `ChannelsConnectID` bigint(20) DEFAULT NULL,
  `CreateDate` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `foreignkeyUser` (`UserID`),
  KEY `fk_channeltouser_channelsconnec` (`ChannelsConnectID`),
  CONSTRAINT `fk_channeltouser_channelsconnec` FOREIGN KEY (`ChannelsConnectID`) REFERENCES `ChannelsConnect` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `foreignkeyUser` FOREIGN KEY (`UserID`) REFERENCES `KhachHang` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=33 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of ChannelsToUser
-- ----------------------------
INSERT INTO `ChannelsToUser` VALUES ('30', '24', '4', '2020-10-29 23:14:36');
INSERT INTO `ChannelsToUser` VALUES ('31', '24', '5', '2020-11-01 13:56:35');
INSERT INTO `ChannelsToUser` VALUES ('32', '37', '4', '2020-11-02 15:59:37');

-- ----------------------------
-- Table structure for ChiTietDonHang
-- ----------------------------
DROP TABLE IF EXISTS `ChiTietDonHang`;
CREATE TABLE `ChiTietDonHang` (
  `MaChiTietDH` bigint(255) NOT NULL AUTO_INCREMENT,
  `MaDonHang` char(20) COLLATE utf8mb4_vietnamese_ci NOT NULL,
  `MaSanPham` bigint(255) NOT NULL,
  `TenSanPham` varchar(255) COLLATE utf8mb4_vietnamese_ci NOT NULL,
  `DonGia` decimal(10,0) NOT NULL,
  `SoLuong` int(255) NOT NULL,
  `TenKhachHang` varchar(255) COLLATE utf8mb4_vietnamese_ci NOT NULL,
  `TrangThai` int(11) NOT NULL,
  `NgayTao` datetime NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`MaChiTietDH`),
  KEY `fk_ctdh_dh` (`MaDonHang`),
  CONSTRAINT `fk_ctdh_dh` FOREIGN KEY (`MaDonHang`) REFERENCES `DonHang` (`MaDonHang`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_vietnamese_ci;

-- ----------------------------
-- Records of ChiTietDonHang
-- ----------------------------

-- ----------------------------
-- Table structure for Decentralization
-- ----------------------------
DROP TABLE IF EXISTS `Decentralization`;
CREATE TABLE `Decentralization` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `RoleID` int(11) DEFAULT NULL,
  `AuthorID` int(11) DEFAULT NULL,
  `isActive` bit(1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_dz_rl` (`RoleID`),
  KEY `fk_dz_auth` (`AuthorID`),
  CONSTRAINT `fk_dz_auth` FOREIGN KEY (`AuthorID`) REFERENCES `Authority` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_dz_rl` FOREIGN KEY (`RoleID`) REFERENCES `Role` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=550 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_vietnamese_ci;

-- ----------------------------
-- Records of Decentralization
-- ----------------------------
INSERT INTO `Decentralization` VALUES ('420', '4', '46', '\0');
INSERT INTO `Decentralization` VALUES ('495', '3', '58', '\0');
INSERT INTO `Decentralization` VALUES ('496', '3', '53', '\0');
INSERT INTO `Decentralization` VALUES ('497', '3', '52', '\0');
INSERT INTO `Decentralization` VALUES ('523', '2', '58', '\0');
INSERT INTO `Decentralization` VALUES ('524', '2', '59', '\0');
INSERT INTO `Decentralization` VALUES ('525', '2', '57', '\0');
INSERT INTO `Decentralization` VALUES ('526', '2', '56', '\0');
INSERT INTO `Decentralization` VALUES ('527', '2', '55', '\0');
INSERT INTO `Decentralization` VALUES ('528', '2', '54', '\0');
INSERT INTO `Decentralization` VALUES ('529', '2', '53', '\0');
INSERT INTO `Decentralization` VALUES ('530', '2', '52', '\0');
INSERT INTO `Decentralization` VALUES ('531', '2', '51', '\0');
INSERT INTO `Decentralization` VALUES ('532', '2', '50', '\0');
INSERT INTO `Decentralization` VALUES ('533', '2', '49', '\0');
INSERT INTO `Decentralization` VALUES ('534', '2', '48', '\0');
INSERT INTO `Decentralization` VALUES ('535', '2', '47', '\0');
INSERT INTO `Decentralization` VALUES ('536', '16', '47', '');
INSERT INTO `Decentralization` VALUES ('537', '1', '58', null);
INSERT INTO `Decentralization` VALUES ('538', '1', '59', null);
INSERT INTO `Decentralization` VALUES ('539', '1', '57', null);
INSERT INTO `Decentralization` VALUES ('540', '1', '56', null);
INSERT INTO `Decentralization` VALUES ('541', '1', '55', null);
INSERT INTO `Decentralization` VALUES ('542', '1', '54', null);
INSERT INTO `Decentralization` VALUES ('543', '1', '53', null);
INSERT INTO `Decentralization` VALUES ('544', '1', '52', null);
INSERT INTO `Decentralization` VALUES ('545', '1', '51', null);
INSERT INTO `Decentralization` VALUES ('546', '1', '50', null);
INSERT INTO `Decentralization` VALUES ('547', '1', '49', null);
INSERT INTO `Decentralization` VALUES ('548', '1', '48', null);
INSERT INTO `Decentralization` VALUES ('549', '1', '47', null);

-- ----------------------------
-- Table structure for DonHang
-- ----------------------------
DROP TABLE IF EXISTS `DonHang`;
CREATE TABLE `DonHang` (
  `MaDonHang` char(20) COLLATE utf8mb4_vietnamese_ci NOT NULL,
  `MaKhachHang` bigint(20) NOT NULL,
  `DiaChiGiao` varchar(255) COLLATE utf8mb4_vietnamese_ci NOT NULL,
  `TongTien` int(20) NOT NULL,
  `SDTNhanHang` varchar(255) COLLATE utf8mb4_vietnamese_ci NOT NULL,
  `TenNguoiNhan` varchar(255) COLLATE utf8mb4_vietnamese_ci NOT NULL,
  `EmailKhachHang` varchar(255) COLLATE utf8mb4_vietnamese_ci DEFAULT NULL,
  `TrangThaiDuyet` int(255) NOT NULL,
  `TrangThaiChuyenHang` int(11) NOT NULL,
  `MaNhanVienDuyet` int(20) DEFAULT NULL,
  `NgayTao` datetime NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`MaDonHang`),
  KEY `fk_dh_MaKhachHang` (`MaKhachHang`),
  KEY `fk_dh_MaNhanVienDuyet` (`MaNhanVienDuyet`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_vietnamese_ci;

-- ----------------------------
-- Records of DonHang
-- ----------------------------

-- ----------------------------
-- Table structure for ErrorForm
-- ----------------------------
DROP TABLE IF EXISTS `ErrorForm`;
CREATE TABLE `ErrorForm` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `Username` varchar(255) DEFAULT NULL,
  `FormText` varchar(255) DEFAULT NULL,
  `ErrorText` text DEFAULT NULL,
  `CreateDate` datetime DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of ErrorForm
-- ----------------------------
INSERT INTO `ErrorForm` VALUES ('13', 'longchin', 'SubcriberController->InsertPrice->SubcriberMonneys', 'System.Data.Entity.Infrastructure.DbUpdateException: An error occurred while updating the entries. See the inner exception for details. ---> System.Data.Entity.Core.UpdateException: An error occurred while updating the entries. See the inner exception for details. ---> MySql.Data.MySqlClient.MySqlException: Timeout expired.  The timeout period elapsed prior to completion of the operation or the server is not responding. ---> System.TimeoutException: Timeout in IO operation\r\n   at MySql.Data.MySqlClient.TimedStream.StopTimer()\r\n   at MySql.Data.MySqlClient.TimedStream.Read(Byte[] buffer, Int32 offset, Int32 count)\r\n   at System.IO.BufferedStream.Read(Byte[] array, Int32 offset, Int32 count)\r\n   at MySql.Data.MySqlClient.MySqlStream.ReadFully(Stream stream, Byte[] buffer, Int32 offset, Int32 count)\r\n   at MySql.Data.MySqlClient.MySqlStream.LoadPacket()\r\n   at MySql.Data.MySqlClient.MySqlStream.ReadPacket()\r\n   at MySql.Data.MySqlClient.NativeDriver.GetResult(Int32& affectedRow, Int64& insertedId)\r\n   at MySql.Data.MySqlClient.Driver.GetResult(Int32 statementId, Int32& affectedRows, Int64& insertedId)\r\n   at MySql.Data.MySqlClient.Driver.NextResult(Int32 statementId, Boolean force)\r\n   at MySql.Data.MySqlClient.MySqlDataReader.NextResult()\r\n   at MySql.Data.MySqlClient.MySqlCommand.ExecuteReader(CommandBehavior behavior)\r\n   --- End of inner exception stack trace ---\r\n   at MySql.Data.MySqlClient.ExceptionInterceptor.Throw(Exception exception)\r\n   at MySql.Data.MySqlClient.MySqlConnection.Throw(Exception ex)\r\n   at MySql.Data.MySqlClient.MySqlConnection.HandleTimeoutOrThreadAbort(Exception ex)\r\n   at MySql.Data.MySqlClient.MySqlCommand.ExecuteReader(CommandBehavior behavior)\r\n   at MySql.Data.Entity.EFMySqlCommand.ExecuteDbDataReader(CommandBehavior behavior)\r\n   at System.Data.Common.DbCommand.ExecuteReader(CommandBehavior behavior)\r\n   at System.Data.Entity.Infrastructure.Interception.DbCommandDispatcher.<Reader>b__c(DbCommand t, DbCommandInterceptionContext`1 c)\r\n   at System.Data.Entity.Infrastructure.Interception.InternalDispatcher`1.Dispatch[TTarget,TInterceptionContext,TResult](TTarget target, Func`3 operation, TInterceptionContext interceptionContext, Action`3 executing, Action`3 executed)\r\n   at System.Data.Entity.Infrastructure.Interception.DbCommandDispatcher.Reader(DbCommand command, DbCommandInterceptionContext interceptionContext)\r\n   at System.Data.Entity.Internal.InterceptableDbCommand.ExecuteDbDataReader(CommandBehavior behavior)\r\n   at System.Data.Common.DbCommand.ExecuteReader(CommandBehavior behavior)\r\n   at System.Data.Entity.Core.Mapping.Update.Internal.DynamicUpdateCommand.Execute(Dictionary`2 identifierValues, List`1 generatedValues)\r\n   at System.Data.Entity.Core.Mapping.Update.Internal.UpdateTranslator.Update()\r\n   --- End of inner exception stack trace ---\r\n   at System.Data.Entity.Core.Mapping.Update.Internal.UpdateTranslator.Update()\r\n   at System.Data.Entity.Core.EntityClient.Internal.EntityAdapter.<Update>b__2(UpdateTranslator ut)\r\n   at System.Data.Entity.Core.EntityClient.Internal.EntityAdapter.Update[T](T noChangesResult, Func`2 updateFunction)\r\n   at System.Data.Entity.Core.EntityClient.Internal.EntityAdapter.Update()\r\n   at System.Data.Entity.Core.Objects.ObjectContext.<SaveChangesToStore>b__35()\r\n   at System.Data.Entity.Core.Objects.ObjectContext.ExecuteInTransaction[T](Func`1 func, IDbExecutionStrategy executionStrategy, Boolean startLocalTransaction, Boolean releaseConnectionOnSuccess)\r\n   at System.Data.Entity.Core.Objects.ObjectContext.SaveChangesToStore(SaveOptions options, IDbExecutionStrategy executionStrategy, Boolean startLocalTransaction)\r\n   at System.Data.Entity.Core.Objects.ObjectContext.<>c__DisplayClass2a.<SaveChangesInternal>b__27()\r\n   at System.Data.Entity.Infrastructure.DefaultExecutionStrategy.Execute[TResult](Func`1 operation)\r\n   at System.Data.Entity.Core.Objects.ObjectContext.SaveChangesInternal(SaveOptions options, Boolean executeInExistingTransaction)\r\n   at System.Data.Entity.Core.Objects.ObjectContext.SaveChanges(SaveOptions options)\r\n   at System.Data.Entity.Internal.InternalContext.SaveChanges()\r\n   --- End of inner exception stack trace ---\r\n   at System.Data.Entity.Internal.InternalContext.SaveChanges()\r\n   at System.Data.Entity.Internal.LazyInternalContext.SaveChanges()\r\n   at System.Data.Entity.DbContext.SaveChanges()\r\n   at ProjectMakeMoney.v1.Controllers.SubcriberController.InsertPrice(SubcriberMonney sub) in E:\\git\\MonneyUp-Push\\moneyup\\ProjectMakeMoney.v1\\ProjectMakeMoney.v1\\Controllers\\SubcriberController.cs:line 242', '2020-09-07 10:29:33');
INSERT INTO `ErrorForm` VALUES ('14', 'longchin', 'SubcriberController->RefershToken->updateChannelsToUser', 'System.Data.Entity.Infrastructure.DbUpdateException: An error occurred while updating the entries. See the inner exception for details. ---> System.Data.Entity.Core.UpdateException: An error occurred while updating the entries. See the inner exception for details. ---> MySql.Data.MySqlClient.MySqlException: Timeout expired.  The timeout period elapsed prior to completion of the operation or the server is not responding. ---> System.TimeoutException: Timeout in IO operation\r\n   at MySql.Data.MySqlClient.TimedStream.StopTimer()\r\n   at MySql.Data.MySqlClient.TimedStream.Read(Byte[] buffer, Int32 offset, Int32 count)\r\n   at System.IO.BufferedStream.Read(Byte[] array, Int32 offset, Int32 count)\r\n   at MySql.Data.MySqlClient.MySqlStream.ReadFully(Stream stream, Byte[] buffer, Int32 offset, Int32 count)\r\n   at MySql.Data.MySqlClient.MySqlStream.LoadPacket()\r\n   at MySql.Data.MySqlClient.MySqlStream.ReadPacket()\r\n   at MySql.Data.MySqlClient.NativeDriver.GetResult(Int32& affectedRow, Int64& insertedId)\r\n   at MySql.Data.MySqlClient.Driver.GetResult(Int32 statementId, Int32& affectedRows, Int64& insertedId)\r\n   at MySql.Data.MySqlClient.Driver.NextResult(Int32 statementId, Boolean force)\r\n   at MySql.Data.MySqlClient.MySqlDataReader.NextResult()\r\n   at MySql.Data.MySqlClient.MySqlCommand.ExecuteReader(CommandBehavior behavior)\r\n   --- End of inner exception stack trace ---\r\n   at MySql.Data.MySqlClient.ExceptionInterceptor.Throw(Exception exception)\r\n   at MySql.Data.MySqlClient.MySqlConnection.Throw(Exception ex)\r\n   at MySql.Data.MySqlClient.MySqlConnection.HandleTimeoutOrThreadAbort(Exception ex)\r\n   at MySql.Data.MySqlClient.MySqlCommand.ExecuteReader(CommandBehavior behavior)\r\n   at MySql.Data.MySqlClient.MySqlCommand.ExecuteNonQuery()\r\n   at MySql.Data.Entity.EFMySqlCommand.ExecuteNonQuery()\r\n   at System.Data.Entity.Infrastructure.Interception.DbCommandDispatcher.<NonQuery>b__0(DbCommand t, DbCommandInterceptionContext`1 c)\r\n   at System.Data.Entity.Infrastructure.Interception.InternalDispatcher`1.Dispatch[TTarget,TInterceptionContext,TResult](TTarget target, Func`3 operation, TInterceptionContext interceptionContext, Action`3 executing, Action`3 executed)\r\n   at System.Data.Entity.Infrastructure.Interception.DbCommandDispatcher.NonQuery(DbCommand command, DbCommandInterceptionContext interceptionContext)\r\n   at System.Data.Entity.Internal.InterceptableDbCommand.ExecuteNonQuery()\r\n   at System.Data.Entity.Core.Mapping.Update.Internal.DynamicUpdateCommand.Execute(Dictionary`2 identifierValues, List`1 generatedValues)\r\n   at System.Data.Entity.Core.Mapping.Update.Internal.UpdateTranslator.Update()\r\n   --- End of inner exception stack trace ---\r\n   at System.Data.Entity.Core.Mapping.Update.Internal.UpdateTranslator.Update()\r\n   at System.Data.Entity.Core.EntityClient.Internal.EntityAdapter.<Update>b__2(UpdateTranslator ut)\r\n   at System.Data.Entity.Core.EntityClient.Internal.EntityAdapter.Update[T](T noChangesResult, Func`2 updateFunction)\r\n   at System.Data.Entity.Core.EntityClient.Internal.EntityAdapter.Update()\r\n   at System.Data.Entity.Core.Objects.ObjectContext.<SaveChangesToStore>b__35()\r\n   at System.Data.Entity.Core.Objects.ObjectContext.ExecuteInTransaction[T](Func`1 func, IDbExecutionStrategy executionStrategy, Boolean startLocalTransaction, Boolean releaseConnectionOnSuccess)\r\n   at System.Data.Entity.Core.Objects.ObjectContext.SaveChangesToStore(SaveOptions options, IDbExecutionStrategy executionStrategy, Boolean startLocalTransaction)\r\n   at System.Data.Entity.Core.Objects.ObjectContext.<>c__DisplayClass2a.<SaveChangesInternal>b__27()\r\n   at System.Data.Entity.Infrastructure.DefaultExecutionStrategy.Execute[TResult](Func`1 operation)\r\n   at System.Data.Entity.Core.Objects.ObjectContext.SaveChangesInternal(SaveOptions options, Boolean executeInExistingTransaction)\r\n   at System.Data.Entity.Core.Objects.ObjectContext.SaveChanges(SaveOptions options)\r\n   at System.Data.Entity.Internal.InternalContext.SaveChanges()\r\n   --- End of inner exception stack trace ---\r\n   at System.Data.Entity.Internal.InternalContext.SaveChanges()\r\n   at System.Data.Entity.Internal.LazyInternalContext.SaveChanges()\r\n   at System.Data.Entity.DbContext.SaveChanges()\r\n   at ProjectMakeMoney.v1.Controllers.SubcriberController.RefershToken(String ChangeID, String AccessToken, String RefreshToken) in E:\\git\\MonneyUp-Push\\moneyup\\ProjectMakeMoney.v1\\ProjectMakeMoney.v1\\Controllers\\SubcriberController.cs:line 212', '2020-09-07 15:01:58');
INSERT INTO `ErrorForm` VALUES ('15', 'longchin', 'SubcriberController->RefershToken->updateChannelsToUser', 'System.Data.Entity.Infrastructure.DbUpdateException: An error occurred while updating the entries. See the inner exception for details. ---> System.Data.Entity.Core.UpdateException: An error occurred while updating the entries. See the inner exception for details. ---> MySql.Data.MySqlClient.MySqlException: Timeout expired.  The timeout period elapsed prior to completion of the operation or the server is not responding. ---> System.TimeoutException: Timeout in IO operation\r\n   at MySql.Data.MySqlClient.TimedStream.StopTimer()\r\n   at MySql.Data.MySqlClient.TimedStream.Read(Byte[] buffer, Int32 offset, Int32 count)\r\n   at System.IO.BufferedStream.Read(Byte[] array, Int32 offset, Int32 count)\r\n   at MySql.Data.MySqlClient.MySqlStream.ReadFully(Stream stream, Byte[] buffer, Int32 offset, Int32 count)\r\n   at MySql.Data.MySqlClient.MySqlStream.LoadPacket()\r\n   at MySql.Data.MySqlClient.MySqlStream.ReadPacket()\r\n   at MySql.Data.MySqlClient.NativeDriver.GetResult(Int32& affectedRow, Int64& insertedId)\r\n   at MySql.Data.MySqlClient.Driver.GetResult(Int32 statementId, Int32& affectedRows, Int64& insertedId)\r\n   at MySql.Data.MySqlClient.Driver.NextResult(Int32 statementId, Boolean force)\r\n   at MySql.Data.MySqlClient.MySqlDataReader.NextResult()\r\n   at MySql.Data.MySqlClient.MySqlCommand.ExecuteReader(CommandBehavior behavior)\r\n   --- End of inner exception stack trace ---\r\n   at MySql.Data.MySqlClient.ExceptionInterceptor.Throw(Exception exception)\r\n   at MySql.Data.MySqlClient.MySqlConnection.Throw(Exception ex)\r\n   at MySql.Data.MySqlClient.MySqlConnection.HandleTimeoutOrThreadAbort(Exception ex)\r\n   at MySql.Data.MySqlClient.MySqlCommand.ExecuteReader(CommandBehavior behavior)\r\n   at MySql.Data.MySqlClient.MySqlCommand.ExecuteNonQuery()\r\n   at MySql.Data.Entity.EFMySqlCommand.ExecuteNonQuery()\r\n   at System.Data.Entity.Infrastructure.Interception.DbCommandDispatcher.<NonQuery>b__0(DbCommand t, DbCommandInterceptionContext`1 c)\r\n   at System.Data.Entity.Infrastructure.Interception.InternalDispatcher`1.Dispatch[TTarget,TInterceptionContext,TResult](TTarget target, Func`3 operation, TInterceptionContext interceptionContext, Action`3 executing, Action`3 executed)\r\n   at System.Data.Entity.Infrastructure.Interception.DbCommandDispatcher.NonQuery(DbCommand command, DbCommandInterceptionContext interceptionContext)\r\n   at System.Data.Entity.Internal.InterceptableDbCommand.ExecuteNonQuery()\r\n   at System.Data.Entity.Core.Mapping.Update.Internal.DynamicUpdateCommand.Execute(Dictionary`2 identifierValues, List`1 generatedValues)\r\n   at System.Data.Entity.Core.Mapping.Update.Internal.UpdateTranslator.Update()\r\n   --- End of inner exception stack trace ---\r\n   at System.Data.Entity.Core.Mapping.Update.Internal.UpdateTranslator.Update()\r\n   at System.Data.Entity.Core.EntityClient.Internal.EntityAdapter.<Update>b__2(UpdateTranslator ut)\r\n   at System.Data.Entity.Core.EntityClient.Internal.EntityAdapter.Update[T](T noChangesResult, Func`2 updateFunction)\r\n   at System.Data.Entity.Core.EntityClient.Internal.EntityAdapter.Update()\r\n   at System.Data.Entity.Core.Objects.ObjectContext.<SaveChangesToStore>b__35()\r\n   at System.Data.Entity.Core.Objects.ObjectContext.ExecuteInTransaction[T](Func`1 func, IDbExecutionStrategy executionStrategy, Boolean startLocalTransaction, Boolean releaseConnectionOnSuccess)\r\n   at System.Data.Entity.Core.Objects.ObjectContext.SaveChangesToStore(SaveOptions options, IDbExecutionStrategy executionStrategy, Boolean startLocalTransaction)\r\n   at System.Data.Entity.Core.Objects.ObjectContext.<>c__DisplayClass2a.<SaveChangesInternal>b__27()\r\n   at System.Data.Entity.Infrastructure.DefaultExecutionStrategy.Execute[TResult](Func`1 operation)\r\n   at System.Data.Entity.Core.Objects.ObjectContext.SaveChangesInternal(SaveOptions options, Boolean executeInExistingTransaction)\r\n   at System.Data.Entity.Core.Objects.ObjectContext.SaveChanges(SaveOptions options)\r\n   at System.Data.Entity.Internal.InternalContext.SaveChanges()\r\n   --- End of inner exception stack trace ---\r\n   at System.Data.Entity.Internal.InternalContext.SaveChanges()\r\n   at System.Data.Entity.Internal.LazyInternalContext.SaveChanges()\r\n   at System.Data.Entity.DbContext.SaveChanges()\r\n   at ProjectMakeMoney.v1.Controllers.SubcriberController.RefershToken(String ChangeID, String AccessToken, String RefreshToken) in E:\\git\\MonneyUp-Push\\moneyup\\ProjectMakeMoney.v1\\ProjectMakeMoney.v1\\Controllers\\SubcriberController.cs:line 211', '2020-09-08 16:04:16');
INSERT INTO `ErrorForm` VALUES ('16', 'longchin', 'SubcriberController->RefershToken->updateChannelsToUser', 'System.Data.Entity.Infrastructure.DbUpdateException: An error occurred while updating the entries. See the inner exception for details. ---> System.Data.Entity.Core.UpdateException: An error occurred while updating the entries. See the inner exception for details. ---> MySql.Data.MySqlClient.MySqlException: Timeout expired.  The timeout period elapsed prior to completion of the operation or the server is not responding. ---> System.TimeoutException: Timeout in IO operation\r\n   at MySql.Data.MySqlClient.TimedStream.StopTimer()\r\n   at MySql.Data.MySqlClient.TimedStream.Read(Byte[] buffer, Int32 offset, Int32 count)\r\n   at System.IO.BufferedStream.Read(Byte[] array, Int32 offset, Int32 count)\r\n   at MySql.Data.MySqlClient.MySqlStream.ReadFully(Stream stream, Byte[] buffer, Int32 offset, Int32 count)\r\n   at MySql.Data.MySqlClient.MySqlStream.LoadPacket()\r\n   at MySql.Data.MySqlClient.MySqlStream.ReadPacket()\r\n   at MySql.Data.MySqlClient.NativeDriver.GetResult(Int32& affectedRow, Int64& insertedId)\r\n   at MySql.Data.MySqlClient.Driver.GetResult(Int32 statementId, Int32& affectedRows, Int64& insertedId)\r\n   at MySql.Data.MySqlClient.Driver.NextResult(Int32 statementId, Boolean force)\r\n   at MySql.Data.MySqlClient.MySqlDataReader.NextResult()\r\n   at MySql.Data.MySqlClient.MySqlCommand.ExecuteReader(CommandBehavior behavior)\r\n   --- End of inner exception stack trace ---\r\n   at MySql.Data.MySqlClient.ExceptionInterceptor.Throw(Exception exception)\r\n   at MySql.Data.MySqlClient.MySqlConnection.Throw(Exception ex)\r\n   at MySql.Data.MySqlClient.MySqlConnection.HandleTimeoutOrThreadAbort(Exception ex)\r\n   at MySql.Data.MySqlClient.MySqlCommand.ExecuteReader(CommandBehavior behavior)\r\n   at MySql.Data.MySqlClient.MySqlCommand.ExecuteNonQuery()\r\n   at MySql.Data.Entity.EFMySqlCommand.ExecuteNonQuery()\r\n   at System.Data.Entity.Infrastructure.Interception.DbCommandDispatcher.<NonQuery>b__0(DbCommand t, DbCommandInterceptionContext`1 c)\r\n   at System.Data.Entity.Infrastructure.Interception.InternalDispatcher`1.Dispatch[TTarget,TInterceptionContext,TResult](TTarget target, Func`3 operation, TInterceptionContext interceptionContext, Action`3 executing, Action`3 executed)\r\n   at System.Data.Entity.Infrastructure.Interception.DbCommandDispatcher.NonQuery(DbCommand command, DbCommandInterceptionContext interceptionContext)\r\n   at System.Data.Entity.Internal.InterceptableDbCommand.ExecuteNonQuery()\r\n   at System.Data.Entity.Core.Mapping.Update.Internal.DynamicUpdateCommand.Execute(Dictionary`2 identifierValues, List`1 generatedValues)\r\n   at System.Data.Entity.Core.Mapping.Update.Internal.UpdateTranslator.Update()\r\n   --- End of inner exception stack trace ---\r\n   at System.Data.Entity.Core.Mapping.Update.Internal.UpdateTranslator.Update()\r\n   at System.Data.Entity.Core.EntityClient.Internal.EntityAdapter.<Update>b__2(UpdateTranslator ut)\r\n   at System.Data.Entity.Core.EntityClient.Internal.EntityAdapter.Update[T](T noChangesResult, Func`2 updateFunction)\r\n   at System.Data.Entity.Core.EntityClient.Internal.EntityAdapter.Update()\r\n   at System.Data.Entity.Core.Objects.ObjectContext.<SaveChangesToStore>b__35()\r\n   at System.Data.Entity.Core.Objects.ObjectContext.ExecuteInTransaction[T](Func`1 func, IDbExecutionStrategy executionStrategy, Boolean startLocalTransaction, Boolean releaseConnectionOnSuccess)\r\n   at System.Data.Entity.Core.Objects.ObjectContext.SaveChangesToStore(SaveOptions options, IDbExecutionStrategy executionStrategy, Boolean startLocalTransaction)\r\n   at System.Data.Entity.Core.Objects.ObjectContext.<>c__DisplayClass2a.<SaveChangesInternal>b__27()\r\n   at System.Data.Entity.Infrastructure.DefaultExecutionStrategy.Execute[TResult](Func`1 operation)\r\n   at System.Data.Entity.Core.Objects.ObjectContext.SaveChangesInternal(SaveOptions options, Boolean executeInExistingTransaction)\r\n   at System.Data.Entity.Core.Objects.ObjectContext.SaveChanges(SaveOptions options)\r\n   at System.Data.Entity.Internal.InternalContext.SaveChanges()\r\n   --- End of inner exception stack trace ---\r\n   at System.Data.Entity.Internal.InternalContext.SaveChanges()\r\n   at System.Data.Entity.Internal.LazyInternalContext.SaveChanges()\r\n   at System.Data.Entity.DbContext.SaveChanges()\r\n   at ProjectMakeMoney.v1.Controllers.SubcriberController.RefershToken(String ChangeID, String AccessToken, String RefreshToken) in E:\\git\\MonneyUp-Push\\moneyup\\ProjectMakeMoney.v1\\ProjectMakeMoney.v1\\Controllers\\SubcriberController.cs:line 211', '2020-09-08 16:05:19');
INSERT INTO `ErrorForm` VALUES ('17', 'longchin', 'SubcriberController->RefershToken->updateChannelsToUser', 'System.Data.Entity.Infrastructure.DbUpdateException: An error occurred while updating the entries. See the inner exception for details. ---> System.Data.Entity.Core.UpdateException: An error occurred while updating the entries. See the inner exception for details. ---> MySql.Data.MySqlClient.MySqlException: Timeout expired.  The timeout period elapsed prior to completion of the operation or the server is not responding. ---> System.TimeoutException: Timeout in IO operation\r\n   at MySql.Data.MySqlClient.TimedStream.StopTimer()\r\n   at MySql.Data.MySqlClient.TimedStream.Read(Byte[] buffer, Int32 offset, Int32 count)\r\n   at System.IO.BufferedStream.Read(Byte[] array, Int32 offset, Int32 count)\r\n   at MySql.Data.MySqlClient.MySqlStream.ReadFully(Stream stream, Byte[] buffer, Int32 offset, Int32 count)\r\n   at MySql.Data.MySqlClient.MySqlStream.LoadPacket()\r\n   at MySql.Data.MySqlClient.MySqlStream.ReadPacket()\r\n   at MySql.Data.MySqlClient.NativeDriver.GetResult(Int32& affectedRow, Int64& insertedId)\r\n   at MySql.Data.MySqlClient.Driver.GetResult(Int32 statementId, Int32& affectedRows, Int64& insertedId)\r\n   at MySql.Data.MySqlClient.Driver.NextResult(Int32 statementId, Boolean force)\r\n   at MySql.Data.MySqlClient.MySqlDataReader.NextResult()\r\n   at MySql.Data.MySqlClient.MySqlCommand.ExecuteReader(CommandBehavior behavior)\r\n   --- End of inner exception stack trace ---\r\n   at MySql.Data.MySqlClient.ExceptionInterceptor.Throw(Exception exception)\r\n   at MySql.Data.MySqlClient.MySqlConnection.Throw(Exception ex)\r\n   at MySql.Data.MySqlClient.MySqlConnection.HandleTimeoutOrThreadAbort(Exception ex)\r\n   at MySql.Data.MySqlClient.MySqlCommand.ExecuteReader(CommandBehavior behavior)\r\n   at MySql.Data.MySqlClient.MySqlCommand.ExecuteNonQuery()\r\n   at MySql.Data.Entity.EFMySqlCommand.ExecuteNonQuery()\r\n   at System.Data.Entity.Infrastructure.Interception.DbCommandDispatcher.<NonQuery>b__0(DbCommand t, DbCommandInterceptionContext`1 c)\r\n   at System.Data.Entity.Infrastructure.Interception.InternalDispatcher`1.Dispatch[TTarget,TInterceptionContext,TResult](TTarget target, Func`3 operation, TInterceptionContext interceptionContext, Action`3 executing, Action`3 executed)\r\n   at System.Data.Entity.Infrastructure.Interception.DbCommandDispatcher.NonQuery(DbCommand command, DbCommandInterceptionContext interceptionContext)\r\n   at System.Data.Entity.Internal.InterceptableDbCommand.ExecuteNonQuery()\r\n   at System.Data.Entity.Core.Mapping.Update.Internal.DynamicUpdateCommand.Execute(Dictionary`2 identifierValues, List`1 generatedValues)\r\n   at System.Data.Entity.Core.Mapping.Update.Internal.UpdateTranslator.Update()\r\n   --- End of inner exception stack trace ---\r\n   at System.Data.Entity.Core.Mapping.Update.Internal.UpdateTranslator.Update()\r\n   at System.Data.Entity.Core.EntityClient.Internal.EntityAdapter.<Update>b__2(UpdateTranslator ut)\r\n   at System.Data.Entity.Core.EntityClient.Internal.EntityAdapter.Update[T](T noChangesResult, Func`2 updateFunction)\r\n   at System.Data.Entity.Core.EntityClient.Internal.EntityAdapter.Update()\r\n   at System.Data.Entity.Core.Objects.ObjectContext.<SaveChangesToStore>b__35()\r\n   at System.Data.Entity.Core.Objects.ObjectContext.ExecuteInTransaction[T](Func`1 func, IDbExecutionStrategy executionStrategy, Boolean startLocalTransaction, Boolean releaseConnectionOnSuccess)\r\n   at System.Data.Entity.Core.Objects.ObjectContext.SaveChangesToStore(SaveOptions options, IDbExecutionStrategy executionStrategy, Boolean startLocalTransaction)\r\n   at System.Data.Entity.Core.Objects.ObjectContext.<>c__DisplayClass2a.<SaveChangesInternal>b__27()\r\n   at System.Data.Entity.Infrastructure.DefaultExecutionStrategy.Execute[TResult](Func`1 operation)\r\n   at System.Data.Entity.Core.Objects.ObjectContext.SaveChangesInternal(SaveOptions options, Boolean executeInExistingTransaction)\r\n   at System.Data.Entity.Core.Objects.ObjectContext.SaveChanges(SaveOptions options)\r\n   at System.Data.Entity.Internal.InternalContext.SaveChanges()\r\n   --- End of inner exception stack trace ---\r\n   at System.Data.Entity.Internal.InternalContext.SaveChanges()\r\n   at System.Data.Entity.Internal.LazyInternalContext.SaveChanges()\r\n   at System.Data.Entity.DbContext.SaveChanges()\r\n   at ProjectMakeMoney.v1.Controllers.SubcriberController.RefershToken(String ChangeID, String AccessToken, String RefreshToken) in E:\\git\\MonneyUp-Push\\moneyup\\ProjectMakeMoney.v1\\ProjectMakeMoney.v1\\Controllers\\SubcriberController.cs:line 211', '2020-09-08 16:08:36');
INSERT INTO `ErrorForm` VALUES ('18', 'longchin', 'SubcriberController->RefershToken->updateChannelsToUser', 'System.Data.Entity.Infrastructure.DbUpdateException: An error occurred while updating the entries. See the inner exception for details. ---> System.Data.Entity.Core.UpdateException: An error occurred while updating the entries. See the inner exception for details. ---> MySql.Data.MySqlClient.MySqlException: Timeout expired.  The timeout period elapsed prior to completion of the operation or the server is not responding. ---> System.TimeoutException: Timeout in IO operation\r\n   at MySql.Data.MySqlClient.TimedStream.StopTimer()\r\n   at MySql.Data.MySqlClient.TimedStream.Read(Byte[] buffer, Int32 offset, Int32 count)\r\n   at System.IO.BufferedStream.Read(Byte[] array, Int32 offset, Int32 count)\r\n   at MySql.Data.MySqlClient.MySqlStream.ReadFully(Stream stream, Byte[] buffer, Int32 offset, Int32 count)\r\n   at MySql.Data.MySqlClient.MySqlStream.LoadPacket()\r\n   at MySql.Data.MySqlClient.MySqlStream.ReadPacket()\r\n   at MySql.Data.MySqlClient.NativeDriver.GetResult(Int32& affectedRow, Int64& insertedId)\r\n   at MySql.Data.MySqlClient.Driver.GetResult(Int32 statementId, Int32& affectedRows, Int64& insertedId)\r\n   at MySql.Data.MySqlClient.Driver.NextResult(Int32 statementId, Boolean force)\r\n   at MySql.Data.MySqlClient.MySqlDataReader.NextResult()\r\n   at MySql.Data.MySqlClient.MySqlCommand.ExecuteReader(CommandBehavior behavior)\r\n   --- End of inner exception stack trace ---\r\n   at MySql.Data.MySqlClient.ExceptionInterceptor.Throw(Exception exception)\r\n   at MySql.Data.MySqlClient.MySqlConnection.Throw(Exception ex)\r\n   at MySql.Data.MySqlClient.MySqlConnection.HandleTimeoutOrThreadAbort(Exception ex)\r\n   at MySql.Data.MySqlClient.MySqlCommand.ExecuteReader(CommandBehavior behavior)\r\n   at MySql.Data.MySqlClient.MySqlCommand.ExecuteNonQuery()\r\n   at MySql.Data.Entity.EFMySqlCommand.ExecuteNonQuery()\r\n   at System.Data.Entity.Infrastructure.Interception.DbCommandDispatcher.<NonQuery>b__0(DbCommand t, DbCommandInterceptionContext`1 c)\r\n   at System.Data.Entity.Infrastructure.Interception.InternalDispatcher`1.Dispatch[TTarget,TInterceptionContext,TResult](TTarget target, Func`3 operation, TInterceptionContext interceptionContext, Action`3 executing, Action`3 executed)\r\n   at System.Data.Entity.Infrastructure.Interception.DbCommandDispatcher.NonQuery(DbCommand command, DbCommandInterceptionContext interceptionContext)\r\n   at System.Data.Entity.Internal.InterceptableDbCommand.ExecuteNonQuery()\r\n   at System.Data.Entity.Core.Mapping.Update.Internal.DynamicUpdateCommand.Execute(Dictionary`2 identifierValues, List`1 generatedValues)\r\n   at System.Data.Entity.Core.Mapping.Update.Internal.UpdateTranslator.Update()\r\n   --- End of inner exception stack trace ---\r\n   at System.Data.Entity.Core.Mapping.Update.Internal.UpdateTranslator.Update()\r\n   at System.Data.Entity.Core.EntityClient.Internal.EntityAdapter.<Update>b__2(UpdateTranslator ut)\r\n   at System.Data.Entity.Core.EntityClient.Internal.EntityAdapter.Update[T](T noChangesResult, Func`2 updateFunction)\r\n   at System.Data.Entity.Core.EntityClient.Internal.EntityAdapter.Update()\r\n   at System.Data.Entity.Core.Objects.ObjectContext.<SaveChangesToStore>b__35()\r\n   at System.Data.Entity.Core.Objects.ObjectContext.ExecuteInTransaction[T](Func`1 func, IDbExecutionStrategy executionStrategy, Boolean startLocalTransaction, Boolean releaseConnectionOnSuccess)\r\n   at System.Data.Entity.Core.Objects.ObjectContext.SaveChangesToStore(SaveOptions options, IDbExecutionStrategy executionStrategy, Boolean startLocalTransaction)\r\n   at System.Data.Entity.Core.Objects.ObjectContext.<>c__DisplayClass2a.<SaveChangesInternal>b__27()\r\n   at System.Data.Entity.Infrastructure.DefaultExecutionStrategy.Execute[TResult](Func`1 operation)\r\n   at System.Data.Entity.Core.Objects.ObjectContext.SaveChangesInternal(SaveOptions options, Boolean executeInExistingTransaction)\r\n   at System.Data.Entity.Core.Objects.ObjectContext.SaveChanges(SaveOptions options)\r\n   at System.Data.Entity.Internal.InternalContext.SaveChanges()\r\n   --- End of inner exception stack trace ---\r\n   at System.Data.Entity.Internal.InternalContext.SaveChanges()\r\n   at System.Data.Entity.Internal.LazyInternalContext.SaveChanges()\r\n   at System.Data.Entity.DbContext.SaveChanges()\r\n   at ProjectMakeMoney.v1.Controllers.SubcriberController.RefershToken(String ChangeID, String AccessToken, String RefreshToken) in E:\\git\\MonneyUp-Push\\moneyup\\ProjectMakeMoney.v1\\ProjectMakeMoney.v1\\Controllers\\SubcriberController.cs:line 211', '2020-09-08 16:19:31');
INSERT INTO `ErrorForm` VALUES ('19', 'longchin', 'SubcriberController->RefershToken->updateChannelsToUser', 'System.Data.Entity.Infrastructure.DbUpdateException: An error occurred while updating the entries. See the inner exception for details. ---> System.Data.Entity.Core.UpdateException: An error occurred while updating the entries. See the inner exception for details. ---> MySql.Data.MySqlClient.MySqlException: Timeout expired.  The timeout period elapsed prior to completion of the operation or the server is not responding. ---> System.TimeoutException: Timeout in IO operation\r\n   at MySql.Data.MySqlClient.TimedStream.StopTimer()\r\n   at MySql.Data.MySqlClient.TimedStream.Read(Byte[] buffer, Int32 offset, Int32 count)\r\n   at System.IO.BufferedStream.Read(Byte[] array, Int32 offset, Int32 count)\r\n   at MySql.Data.MySqlClient.MySqlStream.ReadFully(Stream stream, Byte[] buffer, Int32 offset, Int32 count)\r\n   at MySql.Data.MySqlClient.MySqlStream.LoadPacket()\r\n   at MySql.Data.MySqlClient.MySqlStream.ReadPacket()\r\n   at MySql.Data.MySqlClient.NativeDriver.GetResult(Int32& affectedRow, Int64& insertedId)\r\n   at MySql.Data.MySqlClient.Driver.GetResult(Int32 statementId, Int32& affectedRows, Int64& insertedId)\r\n   at MySql.Data.MySqlClient.Driver.NextResult(Int32 statementId, Boolean force)\r\n   at MySql.Data.MySqlClient.MySqlDataReader.NextResult()\r\n   at MySql.Data.MySqlClient.MySqlCommand.ExecuteReader(CommandBehavior behavior)\r\n   --- End of inner exception stack trace ---\r\n   at MySql.Data.MySqlClient.ExceptionInterceptor.Throw(Exception exception)\r\n   at MySql.Data.MySqlClient.MySqlConnection.Throw(Exception ex)\r\n   at MySql.Data.MySqlClient.MySqlConnection.HandleTimeoutOrThreadAbort(Exception ex)\r\n   at MySql.Data.MySqlClient.MySqlCommand.ExecuteReader(CommandBehavior behavior)\r\n   at MySql.Data.MySqlClient.MySqlCommand.ExecuteNonQuery()\r\n   at MySql.Data.Entity.EFMySqlCommand.ExecuteNonQuery()\r\n   at System.Data.Entity.Infrastructure.Interception.DbCommandDispatcher.<NonQuery>b__0(DbCommand t, DbCommandInterceptionContext`1 c)\r\n   at System.Data.Entity.Infrastructure.Interception.InternalDispatcher`1.Dispatch[TTarget,TInterceptionContext,TResult](TTarget target, Func`3 operation, TInterceptionContext interceptionContext, Action`3 executing, Action`3 executed)\r\n   at System.Data.Entity.Infrastructure.Interception.DbCommandDispatcher.NonQuery(DbCommand command, DbCommandInterceptionContext interceptionContext)\r\n   at System.Data.Entity.Internal.InterceptableDbCommand.ExecuteNonQuery()\r\n   at System.Data.Entity.Core.Mapping.Update.Internal.DynamicUpdateCommand.Execute(Dictionary`2 identifierValues, List`1 generatedValues)\r\n   at System.Data.Entity.Core.Mapping.Update.Internal.UpdateTranslator.Update()\r\n   --- End of inner exception stack trace ---\r\n   at System.Data.Entity.Core.Mapping.Update.Internal.UpdateTranslator.Update()\r\n   at System.Data.Entity.Core.EntityClient.Internal.EntityAdapter.<Update>b__2(UpdateTranslator ut)\r\n   at System.Data.Entity.Core.EntityClient.Internal.EntityAdapter.Update[T](T noChangesResult, Func`2 updateFunction)\r\n   at System.Data.Entity.Core.EntityClient.Internal.EntityAdapter.Update()\r\n   at System.Data.Entity.Core.Objects.ObjectContext.<SaveChangesToStore>b__35()\r\n   at System.Data.Entity.Core.Objects.ObjectContext.ExecuteInTransaction[T](Func`1 func, IDbExecutionStrategy executionStrategy, Boolean startLocalTransaction, Boolean releaseConnectionOnSuccess)\r\n   at System.Data.Entity.Core.Objects.ObjectContext.SaveChangesToStore(SaveOptions options, IDbExecutionStrategy executionStrategy, Boolean startLocalTransaction)\r\n   at System.Data.Entity.Core.Objects.ObjectContext.<>c__DisplayClass2a.<SaveChangesInternal>b__27()\r\n   at System.Data.Entity.Infrastructure.DefaultExecutionStrategy.Execute[TResult](Func`1 operation)\r\n   at System.Data.Entity.Core.Objects.ObjectContext.SaveChangesInternal(SaveOptions options, Boolean executeInExistingTransaction)\r\n   at System.Data.Entity.Core.Objects.ObjectContext.SaveChanges(SaveOptions options)\r\n   at System.Data.Entity.Internal.InternalContext.SaveChanges()\r\n   --- End of inner exception stack trace ---\r\n   at System.Data.Entity.Internal.InternalContext.SaveChanges()\r\n   at System.Data.Entity.Internal.LazyInternalContext.SaveChanges()\r\n   at System.Data.Entity.DbContext.SaveChanges()\r\n   at ProjectMakeMoney.v1.Controllers.SubcriberController.RefershToken(String ChangeID, String AccessToken, String RefreshToken) in E:\\git\\MonneyUp-Push\\moneyup\\ProjectMakeMoney.v1\\ProjectMakeMoney.v1\\Controllers\\SubcriberController.cs:line 211', '2020-09-08 16:35:53');
INSERT INTO `ErrorForm` VALUES ('20', 'longchin', 'SubcriberController->RefershToken->updateChannelsToUser', 'System.Data.Entity.Infrastructure.DbUpdateException: An error occurred while updating the entries. See the inner exception for details. ---> System.Data.Entity.Core.UpdateException: An error occurred while updating the entries. See the inner exception for details. ---> MySql.Data.MySqlClient.MySqlException: Timeout expired.  The timeout period elapsed prior to completion of the operation or the server is not responding. ---> System.TimeoutException: Timeout in IO operation\r\n   at MySql.Data.MySqlClient.TimedStream.StopTimer()\r\n   at MySql.Data.MySqlClient.TimedStream.Read(Byte[] buffer, Int32 offset, Int32 count)\r\n   at System.IO.BufferedStream.Read(Byte[] array, Int32 offset, Int32 count)\r\n   at MySql.Data.MySqlClient.MySqlStream.ReadFully(Stream stream, Byte[] buffer, Int32 offset, Int32 count)\r\n   at MySql.Data.MySqlClient.MySqlStream.LoadPacket()\r\n   at MySql.Data.MySqlClient.MySqlStream.ReadPacket()\r\n   at MySql.Data.MySqlClient.NativeDriver.GetResult(Int32& affectedRow, Int64& insertedId)\r\n   at MySql.Data.MySqlClient.Driver.GetResult(Int32 statementId, Int32& affectedRows, Int64& insertedId)\r\n   at MySql.Data.MySqlClient.Driver.NextResult(Int32 statementId, Boolean force)\r\n   at MySql.Data.MySqlClient.MySqlDataReader.NextResult()\r\n   at MySql.Data.MySqlClient.MySqlCommand.ExecuteReader(CommandBehavior behavior)\r\n   --- End of inner exception stack trace ---\r\n   at MySql.Data.MySqlClient.ExceptionInterceptor.Throw(Exception exception)\r\n   at MySql.Data.MySqlClient.MySqlConnection.Throw(Exception ex)\r\n   at MySql.Data.MySqlClient.MySqlConnection.HandleTimeoutOrThreadAbort(Exception ex)\r\n   at MySql.Data.MySqlClient.MySqlCommand.ExecuteReader(CommandBehavior behavior)\r\n   at MySql.Data.MySqlClient.MySqlCommand.ExecuteNonQuery()\r\n   at MySql.Data.Entity.EFMySqlCommand.ExecuteNonQuery()\r\n   at System.Data.Entity.Infrastructure.Interception.DbCommandDispatcher.<NonQuery>b__0(DbCommand t, DbCommandInterceptionContext`1 c)\r\n   at System.Data.Entity.Infrastructure.Interception.InternalDispatcher`1.Dispatch[TTarget,TInterceptionContext,TResult](TTarget target, Func`3 operation, TInterceptionContext interceptionContext, Action`3 executing, Action`3 executed)\r\n   at System.Data.Entity.Infrastructure.Interception.DbCommandDispatcher.NonQuery(DbCommand command, DbCommandInterceptionContext interceptionContext)\r\n   at System.Data.Entity.Internal.InterceptableDbCommand.ExecuteNonQuery()\r\n   at System.Data.Entity.Core.Mapping.Update.Internal.DynamicUpdateCommand.Execute(Dictionary`2 identifierValues, List`1 generatedValues)\r\n   at System.Data.Entity.Core.Mapping.Update.Internal.UpdateTranslator.Update()\r\n   --- End of inner exception stack trace ---\r\n   at System.Data.Entity.Core.Mapping.Update.Internal.UpdateTranslator.Update()\r\n   at System.Data.Entity.Core.EntityClient.Internal.EntityAdapter.<Update>b__2(UpdateTranslator ut)\r\n   at System.Data.Entity.Core.EntityClient.Internal.EntityAdapter.Update[T](T noChangesResult, Func`2 updateFunction)\r\n   at System.Data.Entity.Core.EntityClient.Internal.EntityAdapter.Update()\r\n   at System.Data.Entity.Core.Objects.ObjectContext.<SaveChangesToStore>b__35()\r\n   at System.Data.Entity.Core.Objects.ObjectContext.ExecuteInTransaction[T](Func`1 func, IDbExecutionStrategy executionStrategy, Boolean startLocalTransaction, Boolean releaseConnectionOnSuccess)\r\n   at System.Data.Entity.Core.Objects.ObjectContext.SaveChangesToStore(SaveOptions options, IDbExecutionStrategy executionStrategy, Boolean startLocalTransaction)\r\n   at System.Data.Entity.Core.Objects.ObjectContext.<>c__DisplayClass2a.<SaveChangesInternal>b__27()\r\n   at System.Data.Entity.Infrastructure.DefaultExecutionStrategy.Execute[TResult](Func`1 operation)\r\n   at System.Data.Entity.Core.Objects.ObjectContext.SaveChangesInternal(SaveOptions options, Boolean executeInExistingTransaction)\r\n   at System.Data.Entity.Core.Objects.ObjectContext.SaveChanges(SaveOptions options)\r\n   at System.Data.Entity.Internal.InternalContext.SaveChanges()\r\n   --- End of inner exception stack trace ---\r\n   at System.Data.Entity.Internal.InternalContext.SaveChanges()\r\n   at System.Data.Entity.Internal.LazyInternalContext.SaveChanges()\r\n   at System.Data.Entity.DbContext.SaveChanges()\r\n   at ProjectMakeMoney.v1.Controllers.SubcriberController.RefershToken(String ChangeID, String AccessToken, String RefreshToken) in E:\\git\\MonneyUp-Push\\moneyup\\ProjectMakeMoney.v1\\ProjectMakeMoney.v1\\Controllers\\SubcriberController.cs:line 211', '2020-09-08 17:33:28');
INSERT INTO `ErrorForm` VALUES ('21', 'longchin', 'SubcriberController->RefershToken->updateChannelsToUser', 'System.Data.Entity.Infrastructure.DbUpdateException: An error occurred while updating the entries. See the inner exception for details. ---> System.Data.Entity.Core.UpdateException: An error occurred while updating the entries. See the inner exception for details. ---> MySql.Data.MySqlClient.MySqlException: Timeout expired.  The timeout period elapsed prior to completion of the operation or the server is not responding. ---> System.TimeoutException: Timeout in IO operation\r\n   at MySql.Data.MySqlClient.TimedStream.StopTimer()\r\n   at MySql.Data.MySqlClient.TimedStream.Read(Byte[] buffer, Int32 offset, Int32 count)\r\n   at System.IO.BufferedStream.Read(Byte[] array, Int32 offset, Int32 count)\r\n   at MySql.Data.MySqlClient.MySqlStream.ReadFully(Stream stream, Byte[] buffer, Int32 offset, Int32 count)\r\n   at MySql.Data.MySqlClient.MySqlStream.LoadPacket()\r\n   at MySql.Data.MySqlClient.MySqlStream.ReadPacket()\r\n   at MySql.Data.MySqlClient.NativeDriver.GetResult(Int32& affectedRow, Int64& insertedId)\r\n   at MySql.Data.MySqlClient.Driver.GetResult(Int32 statementId, Int32& affectedRows, Int64& insertedId)\r\n   at MySql.Data.MySqlClient.Driver.NextResult(Int32 statementId, Boolean force)\r\n   at MySql.Data.MySqlClient.MySqlDataReader.NextResult()\r\n   at MySql.Data.MySqlClient.MySqlCommand.ExecuteReader(CommandBehavior behavior)\r\n   --- End of inner exception stack trace ---\r\n   at MySql.Data.MySqlClient.ExceptionInterceptor.Throw(Exception exception)\r\n   at MySql.Data.MySqlClient.MySqlConnection.Throw(Exception ex)\r\n   at MySql.Data.MySqlClient.MySqlConnection.HandleTimeoutOrThreadAbort(Exception ex)\r\n   at MySql.Data.MySqlClient.MySqlCommand.ExecuteReader(CommandBehavior behavior)\r\n   at MySql.Data.MySqlClient.MySqlCommand.ExecuteNonQuery()\r\n   at MySql.Data.Entity.EFMySqlCommand.ExecuteNonQuery()\r\n   at System.Data.Entity.Infrastructure.Interception.DbCommandDispatcher.<NonQuery>b__0(DbCommand t, DbCommandInterceptionContext`1 c)\r\n   at System.Data.Entity.Infrastructure.Interception.InternalDispatcher`1.Dispatch[TTarget,TInterceptionContext,TResult](TTarget target, Func`3 operation, TInterceptionContext interceptionContext, Action`3 executing, Action`3 executed)\r\n   at System.Data.Entity.Infrastructure.Interception.DbCommandDispatcher.NonQuery(DbCommand command, DbCommandInterceptionContext interceptionContext)\r\n   at System.Data.Entity.Internal.InterceptableDbCommand.ExecuteNonQuery()\r\n   at System.Data.Entity.Core.Mapping.Update.Internal.DynamicUpdateCommand.Execute(Dictionary`2 identifierValues, List`1 generatedValues)\r\n   at System.Data.Entity.Core.Mapping.Update.Internal.UpdateTranslator.Update()\r\n   --- End of inner exception stack trace ---\r\n   at System.Data.Entity.Core.Mapping.Update.Internal.UpdateTranslator.Update()\r\n   at System.Data.Entity.Core.EntityClient.Internal.EntityAdapter.<Update>b__2(UpdateTranslator ut)\r\n   at System.Data.Entity.Core.EntityClient.Internal.EntityAdapter.Update[T](T noChangesResult, Func`2 updateFunction)\r\n   at System.Data.Entity.Core.EntityClient.Internal.EntityAdapter.Update()\r\n   at System.Data.Entity.Core.Objects.ObjectContext.<SaveChangesToStore>b__35()\r\n   at System.Data.Entity.Core.Objects.ObjectContext.ExecuteInTransaction[T](Func`1 func, IDbExecutionStrategy executionStrategy, Boolean startLocalTransaction, Boolean releaseConnectionOnSuccess)\r\n   at System.Data.Entity.Core.Objects.ObjectContext.SaveChangesToStore(SaveOptions options, IDbExecutionStrategy executionStrategy, Boolean startLocalTransaction)\r\n   at System.Data.Entity.Core.Objects.ObjectContext.<>c__DisplayClass2a.<SaveChangesInternal>b__27()\r\n   at System.Data.Entity.Infrastructure.DefaultExecutionStrategy.Execute[TResult](Func`1 operation)\r\n   at System.Data.Entity.Core.Objects.ObjectContext.SaveChangesInternal(SaveOptions options, Boolean executeInExistingTransaction)\r\n   at System.Data.Entity.Core.Objects.ObjectContext.SaveChanges(SaveOptions options)\r\n   at System.Data.Entity.Internal.InternalContext.SaveChanges()\r\n   --- End of inner exception stack trace ---\r\n   at System.Data.Entity.Internal.InternalContext.SaveChanges()\r\n   at System.Data.Entity.Internal.LazyInternalContext.SaveChanges()\r\n   at System.Data.Entity.DbContext.SaveChanges()\r\n   at ProjectMakeMoney.v1.Controllers.SubcriberController.RefershToken(String ChangeID, String AccessToken, String RefreshToken) in E:\\git\\MonneyUp-Push\\moneyup\\ProjectMakeMoney.v1\\ProjectMakeMoney.v1\\Controllers\\SubcriberController.cs:line 211', '2020-09-08 17:34:30');
INSERT INTO `ErrorForm` VALUES ('22', 'longchin', 'SubcriberController->RefershToken->updateChannelsToUser', 'System.Data.Entity.Infrastructure.DbUpdateException: An error occurred while updating the entries. See the inner exception for details. ---> System.Data.Entity.Core.UpdateException: An error occurred while updating the entries. See the inner exception for details. ---> MySql.Data.MySqlClient.MySqlException: Timeout expired.  The timeout period elapsed prior to completion of the operation or the server is not responding. ---> System.TimeoutException: Timeout in IO operation\r\n   at MySql.Data.MySqlClient.TimedStream.StopTimer()\r\n   at MySql.Data.MySqlClient.TimedStream.Read(Byte[] buffer, Int32 offset, Int32 count)\r\n   at System.IO.BufferedStream.Read(Byte[] array, Int32 offset, Int32 count)\r\n   at MySql.Data.MySqlClient.MySqlStream.ReadFully(Stream stream, Byte[] buffer, Int32 offset, Int32 count)\r\n   at MySql.Data.MySqlClient.MySqlStream.LoadPacket()\r\n   at MySql.Data.MySqlClient.MySqlStream.ReadPacket()\r\n   at MySql.Data.MySqlClient.NativeDriver.GetResult(Int32& affectedRow, Int64& insertedId)\r\n   at MySql.Data.MySqlClient.Driver.GetResult(Int32 statementId, Int32& affectedRows, Int64& insertedId)\r\n   at MySql.Data.MySqlClient.Driver.NextResult(Int32 statementId, Boolean force)\r\n   at MySql.Data.MySqlClient.MySqlDataReader.NextResult()\r\n   at MySql.Data.MySqlClient.MySqlCommand.ExecuteReader(CommandBehavior behavior)\r\n   --- End of inner exception stack trace ---\r\n   at MySql.Data.MySqlClient.ExceptionInterceptor.Throw(Exception exception)\r\n   at MySql.Data.MySqlClient.MySqlConnection.Throw(Exception ex)\r\n   at MySql.Data.MySqlClient.MySqlConnection.HandleTimeoutOrThreadAbort(Exception ex)\r\n   at MySql.Data.MySqlClient.MySqlCommand.ExecuteReader(CommandBehavior behavior)\r\n   at MySql.Data.MySqlClient.MySqlCommand.ExecuteNonQuery()\r\n   at MySql.Data.Entity.EFMySqlCommand.ExecuteNonQuery()\r\n   at System.Data.Entity.Infrastructure.Interception.DbCommandDispatcher.<NonQuery>b__0(DbCommand t, DbCommandInterceptionContext`1 c)\r\n   at System.Data.Entity.Infrastructure.Interception.InternalDispatcher`1.Dispatch[TTarget,TInterceptionContext,TResult](TTarget target, Func`3 operation, TInterceptionContext interceptionContext, Action`3 executing, Action`3 executed)\r\n   at System.Data.Entity.Infrastructure.Interception.DbCommandDispatcher.NonQuery(DbCommand command, DbCommandInterceptionContext interceptionContext)\r\n   at System.Data.Entity.Internal.InterceptableDbCommand.ExecuteNonQuery()\r\n   at System.Data.Entity.Core.Mapping.Update.Internal.DynamicUpdateCommand.Execute(Dictionary`2 identifierValues, List`1 generatedValues)\r\n   at System.Data.Entity.Core.Mapping.Update.Internal.UpdateTranslator.Update()\r\n   --- End of inner exception stack trace ---\r\n   at System.Data.Entity.Core.Mapping.Update.Internal.UpdateTranslator.Update()\r\n   at System.Data.Entity.Core.EntityClient.Internal.EntityAdapter.<Update>b__2(UpdateTranslator ut)\r\n   at System.Data.Entity.Core.EntityClient.Internal.EntityAdapter.Update[T](T noChangesResult, Func`2 updateFunction)\r\n   at System.Data.Entity.Core.EntityClient.Internal.EntityAdapter.Update()\r\n   at System.Data.Entity.Core.Objects.ObjectContext.<SaveChangesToStore>b__35()\r\n   at System.Data.Entity.Core.Objects.ObjectContext.ExecuteInTransaction[T](Func`1 func, IDbExecutionStrategy executionStrategy, Boolean startLocalTransaction, Boolean releaseConnectionOnSuccess)\r\n   at System.Data.Entity.Core.Objects.ObjectContext.SaveChangesToStore(SaveOptions options, IDbExecutionStrategy executionStrategy, Boolean startLocalTransaction)\r\n   at System.Data.Entity.Core.Objects.ObjectContext.<>c__DisplayClass2a.<SaveChangesInternal>b__27()\r\n   at System.Data.Entity.Infrastructure.DefaultExecutionStrategy.Execute[TResult](Func`1 operation)\r\n   at System.Data.Entity.Core.Objects.ObjectContext.SaveChangesInternal(SaveOptions options, Boolean executeInExistingTransaction)\r\n   at System.Data.Entity.Core.Objects.ObjectContext.SaveChanges(SaveOptions options)\r\n   at System.Data.Entity.Internal.InternalContext.SaveChanges()\r\n   --- End of inner exception stack trace ---\r\n   at System.Data.Entity.Internal.InternalContext.SaveChanges()\r\n   at System.Data.Entity.Internal.LazyInternalContext.SaveChanges()\r\n   at System.Data.Entity.DbContext.SaveChanges()\r\n   at ProjectMakeMoney.v1.Controllers.SubcriberController.RefershToken(String ChangeID, String AccessToken, String RefreshToken) in E:\\git\\MonneyUp-Push\\moneyup\\ProjectMakeMoney.v1\\ProjectMakeMoney.v1\\Controllers\\SubcriberController.cs:line 211', '2020-09-08 17:35:33');
INSERT INTO `ErrorForm` VALUES ('23', 'huyennguyen', 'SubcriberController->InsertPrice->PostSubcriber', 'System.Data.Entity.Infrastructure.DbUpdateException: An error occurred while updating the entries. See the inner exception for details. ---> System.Data.Entity.Core.UpdateException: An error occurred while updating the entries. See the inner exception for details. ---> MySql.Data.MySqlClient.MySqlException: Timeout expired.  The timeout period elapsed prior to completion of the operation or the server is not responding. ---> System.TimeoutException: Timeout in IO operation\r\n   at MySql.Data.MySqlClient.TimedStream.StopTimer()\r\n   at MySql.Data.MySqlClient.TimedStream.Read(Byte[] buffer, Int32 offset, Int32 count)\r\n   at System.IO.BufferedStream.Read(Byte[] array, Int32 offset, Int32 count)\r\n   at MySql.Data.MySqlClient.MySqlStream.ReadFully(Stream stream, Byte[] buffer, Int32 offset, Int32 count)\r\n   at MySql.Data.MySqlClient.MySqlStream.LoadPacket()\r\n   at MySql.Data.MySqlClient.MySqlStream.ReadPacket()\r\n   at MySql.Data.MySqlClient.NativeDriver.GetResult(Int32& affectedRow, Int64& insertedId)\r\n   at MySql.Data.MySqlClient.Driver.GetResult(Int32 statementId, Int32& affectedRows, Int64& insertedId)\r\n   at MySql.Data.MySqlClient.Driver.NextResult(Int32 statementId, Boolean force)\r\n   at MySql.Data.MySqlClient.MySqlDataReader.NextResult()\r\n   at MySql.Data.MySqlClient.MySqlCommand.ExecuteReader(CommandBehavior behavior)\r\n   --- End of inner exception stack trace ---\r\n   at MySql.Data.MySqlClient.ExceptionInterceptor.Throw(Exception exception)\r\n   at MySql.Data.MySqlClient.MySqlConnection.Throw(Exception ex)\r\n   at MySql.Data.MySqlClient.MySqlConnection.HandleTimeoutOrThreadAbort(Exception ex)\r\n   at MySql.Data.MySqlClient.MySqlCommand.ExecuteReader(CommandBehavior behavior)\r\n   at MySql.Data.Entity.EFMySqlCommand.ExecuteDbDataReader(CommandBehavior behavior)\r\n   at System.Data.Common.DbCommand.ExecuteReader(CommandBehavior behavior)\r\n   at System.Data.Entity.Infrastructure.Interception.DbCommandDispatcher.<Reader>b__c(DbCommand t, DbCommandInterceptionContext`1 c)\r\n   at System.Data.Entity.Infrastructure.Interception.InternalDispatcher`1.Dispatch[TTarget,TInterceptionContext,TResult](TTarget target, Func`3 operation, TInterceptionContext interceptionContext, Action`3 executing, Action`3 executed)\r\n   at System.Data.Entity.Infrastructure.Interception.DbCommandDispatcher.Reader(DbCommand command, DbCommandInterceptionContext interceptionContext)\r\n   at System.Data.Entity.Internal.InterceptableDbCommand.ExecuteDbDataReader(CommandBehavior behavior)\r\n   at System.Data.Common.DbCommand.ExecuteReader(CommandBehavior behavior)\r\n   at System.Data.Entity.Core.Mapping.Update.Internal.DynamicUpdateCommand.Execute(Dictionary`2 identifierValues, List`1 generatedValues)\r\n   at System.Data.Entity.Core.Mapping.Update.Internal.UpdateTranslator.Update()\r\n   --- End of inner exception stack trace ---\r\n   at System.Data.Entity.Core.Mapping.Update.Internal.UpdateTranslator.Update()\r\n   at System.Data.Entity.Core.EntityClient.Internal.EntityAdapter.<Update>b__2(UpdateTranslator ut)\r\n   at System.Data.Entity.Core.EntityClient.Internal.EntityAdapter.Update[T](T noChangesResult, Func`2 updateFunction)\r\n   at System.Data.Entity.Core.EntityClient.Internal.EntityAdapter.Update()\r\n   at System.Data.Entity.Core.Objects.ObjectContext.<SaveChangesToStore>b__35()\r\n   at System.Data.Entity.Core.Objects.ObjectContext.ExecuteInTransaction[T](Func`1 func, IDbExecutionStrategy executionStrategy, Boolean startLocalTransaction, Boolean releaseConnectionOnSuccess)\r\n   at System.Data.Entity.Core.Objects.ObjectContext.SaveChangesToStore(SaveOptions options, IDbExecutionStrategy executionStrategy, Boolean startLocalTransaction)\r\n   at System.Data.Entity.Core.Objects.ObjectContext.<>c__DisplayClass2a.<SaveChangesInternal>b__27()\r\n   at System.Data.Entity.Infrastructure.DefaultExecutionStrategy.Execute[TResult](Func`1 operation)\r\n   at System.Data.Entity.Core.Objects.ObjectContext.SaveChangesInternal(SaveOptions options, Boolean executeInExistingTransaction)\r\n   at System.Data.Entity.Core.Objects.ObjectContext.SaveChanges(SaveOptions options)\r\n   at System.Data.Entity.Internal.InternalContext.SaveChanges()\r\n   --- End of inner exception stack trace ---\r\n   at System.Data.Entity.Internal.InternalContext.SaveChanges()\r\n   at System.Data.Entity.Internal.LazyInternalContext.SaveChanges()\r\n   at System.Data.Entity.DbContext.SaveChanges()\r\n   at ProjectMakeMoney.v1.Controllers.SubcriberController.PostSubcriber(Int64 idKenh) in E:\\git\\MonneyUp-Push\\moneyup\\ProjectMakeMoney.v1\\ProjectMakeMoney.v1\\Controllers\\SubcriberController.cs:line 179', '2020-09-13 11:01:41');
INSERT INTO `ErrorForm` VALUES ('24', 'huyennguyen', 'SubcriberController->InsertPrice->PostSubcriber', 'System.Data.Entity.Infrastructure.DbUpdateException: An error occurred while updating the entries. See the inner exception for details. ---> System.Data.Entity.Core.UpdateException: An error occurred while updating the entries. See the inner exception for details. ---> MySql.Data.MySqlClient.MySqlException: Timeout expired.  The timeout period elapsed prior to completion of the operation or the server is not responding. ---> System.TimeoutException: Timeout in IO operation\r\n   at MySql.Data.MySqlClient.TimedStream.StopTimer()\r\n   at MySql.Data.MySqlClient.TimedStream.Read(Byte[] buffer, Int32 offset, Int32 count)\r\n   at System.IO.BufferedStream.Read(Byte[] array, Int32 offset, Int32 count)\r\n   at MySql.Data.MySqlClient.MySqlStream.ReadFully(Stream stream, Byte[] buffer, Int32 offset, Int32 count)\r\n   at MySql.Data.MySqlClient.MySqlStream.LoadPacket()\r\n   at MySql.Data.MySqlClient.MySqlStream.ReadPacket()\r\n   at MySql.Data.MySqlClient.NativeDriver.GetResult(Int32& affectedRow, Int64& insertedId)\r\n   at MySql.Data.MySqlClient.Driver.GetResult(Int32 statementId, Int32& affectedRows, Int64& insertedId)\r\n   at MySql.Data.MySqlClient.Driver.NextResult(Int32 statementId, Boolean force)\r\n   at MySql.Data.MySqlClient.MySqlDataReader.NextResult()\r\n   at MySql.Data.MySqlClient.MySqlCommand.ExecuteReader(CommandBehavior behavior)\r\n   --- End of inner exception stack trace ---\r\n   at MySql.Data.MySqlClient.ExceptionInterceptor.Throw(Exception exception)\r\n   at MySql.Data.MySqlClient.MySqlConnection.Throw(Exception ex)\r\n   at MySql.Data.MySqlClient.MySqlConnection.HandleTimeoutOrThreadAbort(Exception ex)\r\n   at MySql.Data.MySqlClient.MySqlCommand.ExecuteReader(CommandBehavior behavior)\r\n   at MySql.Data.Entity.EFMySqlCommand.ExecuteDbDataReader(CommandBehavior behavior)\r\n   at System.Data.Common.DbCommand.ExecuteReader(CommandBehavior behavior)\r\n   at System.Data.Entity.Infrastructure.Interception.DbCommandDispatcher.<Reader>b__c(DbCommand t, DbCommandInterceptionContext`1 c)\r\n   at System.Data.Entity.Infrastructure.Interception.InternalDispatcher`1.Dispatch[TTarget,TInterceptionContext,TResult](TTarget target, Func`3 operation, TInterceptionContext interceptionContext, Action`3 executing, Action`3 executed)\r\n   at System.Data.Entity.Infrastructure.Interception.DbCommandDispatcher.Reader(DbCommand command, DbCommandInterceptionContext interceptionContext)\r\n   at System.Data.Entity.Internal.InterceptableDbCommand.ExecuteDbDataReader(CommandBehavior behavior)\r\n   at System.Data.Common.DbCommand.ExecuteReader(CommandBehavior behavior)\r\n   at System.Data.Entity.Core.Mapping.Update.Internal.DynamicUpdateCommand.Execute(Dictionary`2 identifierValues, List`1 generatedValues)\r\n   at System.Data.Entity.Core.Mapping.Update.Internal.UpdateTranslator.Update()\r\n   --- End of inner exception stack trace ---\r\n   at System.Data.Entity.Core.Mapping.Update.Internal.UpdateTranslator.Update()\r\n   at System.Data.Entity.Core.EntityClient.Internal.EntityAdapter.<Update>b__2(UpdateTranslator ut)\r\n   at System.Data.Entity.Core.EntityClient.Internal.EntityAdapter.Update[T](T noChangesResult, Func`2 updateFunction)\r\n   at System.Data.Entity.Core.EntityClient.Internal.EntityAdapter.Update()\r\n   at System.Data.Entity.Core.Objects.ObjectContext.<SaveChangesToStore>b__35()\r\n   at System.Data.Entity.Core.Objects.ObjectContext.ExecuteInTransaction[T](Func`1 func, IDbExecutionStrategy executionStrategy, Boolean startLocalTransaction, Boolean releaseConnectionOnSuccess)\r\n   at System.Data.Entity.Core.Objects.ObjectContext.SaveChangesToStore(SaveOptions options, IDbExecutionStrategy executionStrategy, Boolean startLocalTransaction)\r\n   at System.Data.Entity.Core.Objects.ObjectContext.<>c__DisplayClass2a.<SaveChangesInternal>b__27()\r\n   at System.Data.Entity.Infrastructure.DefaultExecutionStrategy.Execute[TResult](Func`1 operation)\r\n   at System.Data.Entity.Core.Objects.ObjectContext.SaveChangesInternal(SaveOptions options, Boolean executeInExistingTransaction)\r\n   at System.Data.Entity.Core.Objects.ObjectContext.SaveChanges(SaveOptions options)\r\n   at System.Data.Entity.Internal.InternalContext.SaveChanges()\r\n   --- End of inner exception stack trace ---\r\n   at System.Data.Entity.Internal.InternalContext.SaveChanges()\r\n   at System.Data.Entity.Internal.LazyInternalContext.SaveChanges()\r\n   at System.Data.Entity.DbContext.SaveChanges()\r\n   at ProjectMakeMoney.v1.Controllers.SubcriberController.PostSubcriber(Int64 idKenh) in E:\\git\\MonneyUp-Push\\moneyup\\ProjectMakeMoney.v1\\ProjectMakeMoney.v1\\Controllers\\SubcriberController.cs:line 179', '2020-09-13 11:02:43');
INSERT INTO `ErrorForm` VALUES ('25', 'longchin', 'SubcriberController->InsertPrice->PostSubcriber', 'System.Data.Entity.Infrastructure.DbUpdateException: An error occurred while updating the entries. See the inner exception for details. ---> System.Data.Entity.Core.UpdateException: An error occurred while updating the entries. See the inner exception for details. ---> MySql.Data.MySqlClient.MySqlException: Timeout expired.  The timeout period elapsed prior to completion of the operation or the server is not responding. ---> System.TimeoutException: Timeout in IO operation\r\n   at MySql.Data.MySqlClient.TimedStream.StopTimer()\r\n   at MySql.Data.MySqlClient.TimedStream.Read(Byte[] buffer, Int32 offset, Int32 count)\r\n   at System.IO.BufferedStream.Read(Byte[] array, Int32 offset, Int32 count)\r\n   at MySql.Data.MySqlClient.MySqlStream.ReadFully(Stream stream, Byte[] buffer, Int32 offset, Int32 count)\r\n   at MySql.Data.MySqlClient.MySqlStream.LoadPacket()\r\n   at MySql.Data.MySqlClient.MySqlStream.ReadPacket()\r\n   at MySql.Data.MySqlClient.NativeDriver.GetResult(Int32& affectedRow, Int64& insertedId)\r\n   at MySql.Data.MySqlClient.Driver.GetResult(Int32 statementId, Int32& affectedRows, Int64& insertedId)\r\n   at MySql.Data.MySqlClient.Driver.NextResult(Int32 statementId, Boolean force)\r\n   at MySql.Data.MySqlClient.MySqlDataReader.NextResult()\r\n   at MySql.Data.MySqlClient.MySqlCommand.ExecuteReader(CommandBehavior behavior)\r\n   --- End of inner exception stack trace ---\r\n   at MySql.Data.MySqlClient.ExceptionInterceptor.Throw(Exception exception)\r\n   at MySql.Data.MySqlClient.MySqlConnection.Throw(Exception ex)\r\n   at MySql.Data.MySqlClient.MySqlConnection.HandleTimeoutOrThreadAbort(Exception ex)\r\n   at MySql.Data.MySqlClient.MySqlCommand.ExecuteReader(CommandBehavior behavior)\r\n   at MySql.Data.Entity.EFMySqlCommand.ExecuteDbDataReader(CommandBehavior behavior)\r\n   at System.Data.Common.DbCommand.ExecuteReader(CommandBehavior behavior)\r\n   at System.Data.Entity.Infrastructure.Interception.DbCommandDispatcher.<Reader>b__c(DbCommand t, DbCommandInterceptionContext`1 c)\r\n   at System.Data.Entity.Infrastructure.Interception.InternalDispatcher`1.Dispatch[TTarget,TInterceptionContext,TResult](TTarget target, Func`3 operation, TInterceptionContext interceptionContext, Action`3 executing, Action`3 executed)\r\n   at System.Data.Entity.Infrastructure.Interception.DbCommandDispatcher.Reader(DbCommand command, DbCommandInterceptionContext interceptionContext)\r\n   at System.Data.Entity.Internal.InterceptableDbCommand.ExecuteDbDataReader(CommandBehavior behavior)\r\n   at System.Data.Common.DbCommand.ExecuteReader(CommandBehavior behavior)\r\n   at System.Data.Entity.Core.Mapping.Update.Internal.DynamicUpdateCommand.Execute(Dictionary`2 identifierValues, List`1 generatedValues)\r\n   at System.Data.Entity.Core.Mapping.Update.Internal.UpdateTranslator.Update()\r\n   --- End of inner exception stack trace ---\r\n   at System.Data.Entity.Core.Mapping.Update.Internal.UpdateTranslator.Update()\r\n   at System.Data.Entity.Core.EntityClient.Internal.EntityAdapter.<Update>b__2(UpdateTranslator ut)\r\n   at System.Data.Entity.Core.EntityClient.Internal.EntityAdapter.Update[T](T noChangesResult, Func`2 updateFunction)\r\n   at System.Data.Entity.Core.EntityClient.Internal.EntityAdapter.Update()\r\n   at System.Data.Entity.Core.Objects.ObjectContext.<SaveChangesToStore>b__35()\r\n   at System.Data.Entity.Core.Objects.ObjectContext.ExecuteInTransaction[T](Func`1 func, IDbExecutionStrategy executionStrategy, Boolean startLocalTransaction, Boolean releaseConnectionOnSuccess)\r\n   at System.Data.Entity.Core.Objects.ObjectContext.SaveChangesToStore(SaveOptions options, IDbExecutionStrategy executionStrategy, Boolean startLocalTransaction)\r\n   at System.Data.Entity.Core.Objects.ObjectContext.<>c__DisplayClass2a.<SaveChangesInternal>b__27()\r\n   at System.Data.Entity.Infrastructure.DefaultExecutionStrategy.Execute[TResult](Func`1 operation)\r\n   at System.Data.Entity.Core.Objects.ObjectContext.SaveChangesInternal(SaveOptions options, Boolean executeInExistingTransaction)\r\n   at System.Data.Entity.Core.Objects.ObjectContext.SaveChanges(SaveOptions options)\r\n   at System.Data.Entity.Internal.InternalContext.SaveChanges()\r\n   --- End of inner exception stack trace ---\r\n   at System.Data.Entity.Internal.InternalContext.SaveChanges()\r\n   at System.Data.Entity.Internal.LazyInternalContext.SaveChanges()\r\n   at System.Data.Entity.DbContext.SaveChanges()\r\n   at ProjectMakeMoney.v1.Controllers.SubcriberController.PostSubcriber(Int64 idKenh) in E:\\git\\MonneyUp-Push\\moneyup\\ProjectMakeMoney.v1\\ProjectMakeMoney.v1\\Controllers\\SubcriberController.cs:line 179', '2020-09-13 11:04:59');
INSERT INTO `ErrorForm` VALUES ('26', 'longchin', 'SubcriberController->RefershToken->updateChannelsToUser', 'System.Data.Entity.Infrastructure.DbUpdateException: An error occurred while updating the entries. See the inner exception for details. ---> System.Data.Entity.Core.UpdateException: An error occurred while updating the entries. See the inner exception for details. ---> MySql.Data.MySqlClient.MySqlException: Timeout expired.  The timeout period elapsed prior to completion of the operation or the server is not responding. ---> System.TimeoutException: Timeout in IO operation\r\n   at MySql.Data.MySqlClient.TimedStream.StopTimer()\r\n   at MySql.Data.MySqlClient.TimedStream.Read(Byte[] buffer, Int32 offset, Int32 count)\r\n   at System.IO.BufferedStream.Read(Byte[] array, Int32 offset, Int32 count)\r\n   at MySql.Data.MySqlClient.MySqlStream.ReadFully(Stream stream, Byte[] buffer, Int32 offset, Int32 count)\r\n   at MySql.Data.MySqlClient.MySqlStream.LoadPacket()\r\n   at MySql.Data.MySqlClient.MySqlStream.ReadPacket()\r\n   at MySql.Data.MySqlClient.NativeDriver.GetResult(Int32& affectedRow, Int64& insertedId)\r\n   at MySql.Data.MySqlClient.Driver.GetResult(Int32 statementId, Int32& affectedRows, Int64& insertedId)\r\n   at MySql.Data.MySqlClient.Driver.NextResult(Int32 statementId, Boolean force)\r\n   at MySql.Data.MySqlClient.MySqlDataReader.NextResult()\r\n   at MySql.Data.MySqlClient.MySqlCommand.ExecuteReader(CommandBehavior behavior)\r\n   --- End of inner exception stack trace ---\r\n   at MySql.Data.MySqlClient.ExceptionInterceptor.Throw(Exception exception)\r\n   at MySql.Data.MySqlClient.MySqlConnection.Throw(Exception ex)\r\n   at MySql.Data.MySqlClient.MySqlConnection.HandleTimeoutOrThreadAbort(Exception ex)\r\n   at MySql.Data.MySqlClient.MySqlCommand.ExecuteReader(CommandBehavior behavior)\r\n   at MySql.Data.MySqlClient.MySqlCommand.ExecuteNonQuery()\r\n   at MySql.Data.Entity.EFMySqlCommand.ExecuteNonQuery()\r\n   at System.Data.Entity.Infrastructure.Interception.DbCommandDispatcher.<NonQuery>b__0(DbCommand t, DbCommandInterceptionContext`1 c)\r\n   at System.Data.Entity.Infrastructure.Interception.InternalDispatcher`1.Dispatch[TTarget,TInterceptionContext,TResult](TTarget target, Func`3 operation, TInterceptionContext interceptionContext, Action`3 executing, Action`3 executed)\r\n   at System.Data.Entity.Infrastructure.Interception.DbCommandDispatcher.NonQuery(DbCommand command, DbCommandInterceptionContext interceptionContext)\r\n   at System.Data.Entity.Internal.InterceptableDbCommand.ExecuteNonQuery()\r\n   at System.Data.Entity.Core.Mapping.Update.Internal.DynamicUpdateCommand.Execute(Dictionary`2 identifierValues, List`1 generatedValues)\r\n   at System.Data.Entity.Core.Mapping.Update.Internal.UpdateTranslator.Update()\r\n   --- End of inner exception stack trace ---\r\n   at System.Data.Entity.Core.Mapping.Update.Internal.UpdateTranslator.Update()\r\n   at System.Data.Entity.Core.EntityClient.Internal.EntityAdapter.<Update>b__2(UpdateTranslator ut)\r\n   at System.Data.Entity.Core.EntityClient.Internal.EntityAdapter.Update[T](T noChangesResult, Func`2 updateFunction)\r\n   at System.Data.Entity.Core.EntityClient.Internal.EntityAdapter.Update()\r\n   at System.Data.Entity.Core.Objects.ObjectContext.<SaveChangesToStore>b__35()\r\n   at System.Data.Entity.Core.Objects.ObjectContext.ExecuteInTransaction[T](Func`1 func, IDbExecutionStrategy executionStrategy, Boolean startLocalTransaction, Boolean releaseConnectionOnSuccess)\r\n   at System.Data.Entity.Core.Objects.ObjectContext.SaveChangesToStore(SaveOptions options, IDbExecutionStrategy executionStrategy, Boolean startLocalTransaction)\r\n   at System.Data.Entity.Core.Objects.ObjectContext.<>c__DisplayClass2a.<SaveChangesInternal>b__27()\r\n   at System.Data.Entity.Infrastructure.DefaultExecutionStrategy.Execute[TResult](Func`1 operation)\r\n   at System.Data.Entity.Core.Objects.ObjectContext.SaveChangesInternal(SaveOptions options, Boolean executeInExistingTransaction)\r\n   at System.Data.Entity.Core.Objects.ObjectContext.SaveChanges(SaveOptions options)\r\n   at System.Data.Entity.Internal.InternalContext.SaveChanges()\r\n   --- End of inner exception stack trace ---\r\n   at System.Data.Entity.Internal.InternalContext.SaveChanges()\r\n   at System.Data.Entity.Internal.LazyInternalContext.SaveChanges()\r\n   at System.Data.Entity.DbContext.SaveChanges()\r\n   at ProjectMakeMoney.v1.Controllers.SubcriberController.RefershToken(String ChangeID, String AccessToken, String RefreshToken) in E:\\git\\MonneyUp-Push\\moneyup\\ProjectMakeMoney.v1\\ProjectMakeMoney.v1\\Controllers\\SubcriberController.cs:line 218', '2020-09-28 10:41:07');
INSERT INTO `ErrorForm` VALUES ('27', 'longchin', 'Accuracy->isErrorSecurityCode', 'System.Data.Entity.Infrastructure.DbUpdateException: An error occurred while updating the entries. See the inner exception for details. ---> System.Data.Entity.Core.UpdateException: An error occurred while updating the entries. See the inner exception for details. ---> MySql.Data.MySqlClient.MySqlException: Cannot add or update a child row: a foreign key constraint fails (\"MoneyUp\".\"YeuCauKhoa\", CONSTRAINT \"Fk_YCK_NNK\" FOREIGN KEY (\"LidoGui\") REFERENCES \"NguyenNhanKhoa\" (\"ID\") ON DELETE CASCADE ON UPDATE CASCADE)\r\n   at MySql.Data.MySqlClient.MySqlStream.ReadPacket()\r\n   at MySql.Data.MySqlClient.NativeDriver.GetResult(Int32& affectedRow, Int64& insertedId)\r\n   at MySql.Data.MySqlClient.Driver.GetResult(Int32 statementId, Int32& affectedRows, Int64& insertedId)\r\n   at MySql.Data.MySqlClient.Driver.NextResult(Int32 statementId, Boolean force)\r\n   at MySql.Data.MySqlClient.MySqlDataReader.NextResult()\r\n   at MySql.Data.MySqlClient.MySqlCommand.ExecuteReader(CommandBehavior behavior)\r\n   at MySql.Data.Entity.EFMySqlCommand.ExecuteDbDataReader(CommandBehavior behavior)\r\n   at System.Data.Common.DbCommand.ExecuteReader(CommandBehavior behavior)\r\n   at System.Data.Entity.Infrastructure.Interception.DbCommandDispatcher.<Reader>b__c(DbCommand t, DbCommandInterceptionContext`1 c)\r\n   at System.Data.Entity.Infrastructure.Interception.InternalDispatcher`1.Dispatch[TTarget,TInterceptionContext,TResult](TTarget target, Func`3 operation, TInterceptionContext interceptionContext, Action`3 executing, Action`3 executed)\r\n   at System.Data.Entity.Infrastructure.Interception.DbCommandDispatcher.Reader(DbCommand command, DbCommandInterceptionContext interceptionContext)\r\n   at System.Data.Entity.Internal.InterceptableDbCommand.ExecuteDbDataReader(CommandBehavior behavior)\r\n   at System.Data.Common.DbCommand.ExecuteReader(CommandBehavior behavior)\r\n   at System.Data.Entity.Core.Mapping.Update.Internal.DynamicUpdateCommand.Execute(Dictionary`2 identifierValues, List`1 generatedValues)\r\n   at System.Data.Entity.Core.Mapping.Update.Internal.UpdateTranslator.Update()\r\n   --- End of inner exception stack trace ---\r\n   at System.Data.Entity.Core.Mapping.Update.Internal.UpdateTranslator.Update()\r\n   at System.Data.Entity.Core.EntityClient.Internal.EntityAdapter.<Update>b__2(UpdateTranslator ut)\r\n   at System.Data.Entity.Core.EntityClient.Internal.EntityAdapter.Update[T](T noChangesResult, Func`2 updateFunction)\r\n   at System.Data.Entity.Core.EntityClient.Internal.EntityAdapter.Update()\r\n   at System.Data.Entity.Core.Objects.ObjectContext.<SaveChangesToStore>b__35()\r\n   at System.Data.Entity.Core.Objects.ObjectContext.ExecuteInTransaction[T](Func`1 func, IDbExecutionStrategy executionStrategy, Boolean startLocalTransaction, Boolean releaseConnectionOnSuccess)\r\n   at System.Data.Entity.Core.Objects.ObjectContext.SaveChangesToStore(SaveOptions options, IDbExecutionStrategy executionStrategy, Boolean startLocalTransaction)\r\n   at System.Data.Entity.Core.Objects.ObjectContext.<>c__DisplayClass2a.<SaveChangesInternal>b__27()\r\n   at System.Data.Entity.Infrastructure.DefaultExecutionStrategy.Execute[TResult](Func`1 operation)\r\n   at System.Data.Entity.Core.Objects.ObjectContext.SaveChangesInternal(SaveOptions options, Boolean executeInExistingTransaction)\r\n   at System.Data.Entity.Core.Objects.ObjectContext.SaveChanges(SaveOptions options)\r\n   at System.Data.Entity.Internal.InternalContext.SaveChanges()\r\n   --- End of inner exception stack trace ---\r\n   at System.Data.Entity.Internal.InternalContext.SaveChanges()\r\n   at System.Data.Entity.Internal.LazyInternalContext.SaveChanges()\r\n   at System.Data.Entity.DbContext.SaveChanges()\r\n   at Library.YTB.DA.Profiles.Profiles.isErrorSecurityCode(Int32 lido) in D:\\MoneyUp\\git\\library.ytb\\Library.YTB.DA\\Profiles\\Profiles.cs:line 289', '2020-10-29 16:55:38');
INSERT INTO `ErrorForm` VALUES ('28', 'longchin', 'Accuracy->isErrorSecurityCode', 'System.Data.Entity.Infrastructure.DbUpdateException: An error occurred while updating the entries. See the inner exception for details. ---> System.Data.Entity.Core.UpdateException: An error occurred while updating the entries. See the inner exception for details. ---> MySql.Data.MySqlClient.MySqlException: Cannot add or update a child row: a foreign key constraint fails (\"MoneyUp\".\"YeuCauKhoa\", CONSTRAINT \"Fk_YCK_NNK\" FOREIGN KEY (\"LidoGui\") REFERENCES \"NguyenNhanKhoa\" (\"ID\") ON DELETE CASCADE ON UPDATE CASCADE)\r\n   at MySql.Data.MySqlClient.MySqlStream.ReadPacket()\r\n   at MySql.Data.MySqlClient.NativeDriver.GetResult(Int32& affectedRow, Int64& insertedId)\r\n   at MySql.Data.MySqlClient.Driver.GetResult(Int32 statementId, Int32& affectedRows, Int64& insertedId)\r\n   at MySql.Data.MySqlClient.Driver.NextResult(Int32 statementId, Boolean force)\r\n   at MySql.Data.MySqlClient.MySqlDataReader.NextResult()\r\n   at MySql.Data.MySqlClient.MySqlCommand.ExecuteReader(CommandBehavior behavior)\r\n   at MySql.Data.Entity.EFMySqlCommand.ExecuteDbDataReader(CommandBehavior behavior)\r\n   at System.Data.Common.DbCommand.ExecuteReader(CommandBehavior behavior)\r\n   at System.Data.Entity.Infrastructure.Interception.DbCommandDispatcher.<Reader>b__c(DbCommand t, DbCommandInterceptionContext`1 c)\r\n   at System.Data.Entity.Infrastructure.Interception.InternalDispatcher`1.Dispatch[TTarget,TInterceptionContext,TResult](TTarget target, Func`3 operation, TInterceptionContext interceptionContext, Action`3 executing, Action`3 executed)\r\n   at System.Data.Entity.Infrastructure.Interception.DbCommandDispatcher.Reader(DbCommand command, DbCommandInterceptionContext interceptionContext)\r\n   at System.Data.Entity.Internal.InterceptableDbCommand.ExecuteDbDataReader(CommandBehavior behavior)\r\n   at System.Data.Common.DbCommand.ExecuteReader(CommandBehavior behavior)\r\n   at System.Data.Entity.Core.Mapping.Update.Internal.DynamicUpdateCommand.Execute(Dictionary`2 identifierValues, List`1 generatedValues)\r\n   at System.Data.Entity.Core.Mapping.Update.Internal.UpdateTranslator.Update()\r\n   --- End of inner exception stack trace ---\r\n   at System.Data.Entity.Core.Mapping.Update.Internal.UpdateTranslator.Update()\r\n   at System.Data.Entity.Core.EntityClient.Internal.EntityAdapter.<Update>b__2(UpdateTranslator ut)\r\n   at System.Data.Entity.Core.EntityClient.Internal.EntityAdapter.Update[T](T noChangesResult, Func`2 updateFunction)\r\n   at System.Data.Entity.Core.EntityClient.Internal.EntityAdapter.Update()\r\n   at System.Data.Entity.Core.Objects.ObjectContext.<SaveChangesToStore>b__35()\r\n   at System.Data.Entity.Core.Objects.ObjectContext.ExecuteInTransaction[T](Func`1 func, IDbExecutionStrategy executionStrategy, Boolean startLocalTransaction, Boolean releaseConnectionOnSuccess)\r\n   at System.Data.Entity.Core.Objects.ObjectContext.SaveChangesToStore(SaveOptions options, IDbExecutionStrategy executionStrategy, Boolean startLocalTransaction)\r\n   at System.Data.Entity.Core.Objects.ObjectContext.<>c__DisplayClass2a.<SaveChangesInternal>b__27()\r\n   at System.Data.Entity.Infrastructure.DefaultExecutionStrategy.Execute[TResult](Func`1 operation)\r\n   at System.Data.Entity.Core.Objects.ObjectContext.SaveChangesInternal(SaveOptions options, Boolean executeInExistingTransaction)\r\n   at System.Data.Entity.Core.Objects.ObjectContext.SaveChanges(SaveOptions options)\r\n   at System.Data.Entity.Internal.InternalContext.SaveChanges()\r\n   --- End of inner exception stack trace ---\r\n   at System.Data.Entity.Internal.InternalContext.SaveChanges()\r\n   at System.Data.Entity.Internal.LazyInternalContext.SaveChanges()\r\n   at System.Data.Entity.DbContext.SaveChanges()\r\n   at Library.YTB.DA.Profiles.Profiles.isErrorSecurityCode(Int32 lido) in D:\\MoneyUp\\git\\library.ytb\\Library.YTB.DA\\Profiles\\Profiles.cs:line 289', '2020-10-29 16:58:25');
INSERT INTO `ErrorForm` VALUES ('29', 'longchin', 'Accuracy->isErrorSecurityCode', 'System.Data.Entity.Infrastructure.DbUpdateException: An error occurred while updating the entries. See the inner exception for details. ---> System.Data.Entity.Core.UpdateException: An error occurred while updating the entries. See the inner exception for details. ---> MySql.Data.MySqlClient.MySqlException: Cannot add or update a child row: a foreign key constraint fails (\"MoneyUp\".\"YeuCauKhoa\", CONSTRAINT \"Fk_YCK_NNK\" FOREIGN KEY (\"LidoGui\") REFERENCES \"NguyenNhanKhoa\" (\"ID\") ON DELETE CASCADE ON UPDATE CASCADE)\r\n   at MySql.Data.MySqlClient.MySqlStream.ReadPacket()\r\n   at MySql.Data.MySqlClient.NativeDriver.GetResult(Int32& affectedRow, Int64& insertedId)\r\n   at MySql.Data.MySqlClient.Driver.GetResult(Int32 statementId, Int32& affectedRows, Int64& insertedId)\r\n   at MySql.Data.MySqlClient.Driver.NextResult(Int32 statementId, Boolean force)\r\n   at MySql.Data.MySqlClient.MySqlDataReader.NextResult()\r\n   at MySql.Data.MySqlClient.MySqlCommand.ExecuteReader(CommandBehavior behavior)\r\n   at MySql.Data.Entity.EFMySqlCommand.ExecuteDbDataReader(CommandBehavior behavior)\r\n   at System.Data.Common.DbCommand.ExecuteReader(CommandBehavior behavior)\r\n   at System.Data.Entity.Infrastructure.Interception.DbCommandDispatcher.<Reader>b__c(DbCommand t, DbCommandInterceptionContext`1 c)\r\n   at System.Data.Entity.Infrastructure.Interception.InternalDispatcher`1.Dispatch[TTarget,TInterceptionContext,TResult](TTarget target, Func`3 operation, TInterceptionContext interceptionContext, Action`3 executing, Action`3 executed)\r\n   at System.Data.Entity.Infrastructure.Interception.DbCommandDispatcher.Reader(DbCommand command, DbCommandInterceptionContext interceptionContext)\r\n   at System.Data.Entity.Internal.InterceptableDbCommand.ExecuteDbDataReader(CommandBehavior behavior)\r\n   at System.Data.Common.DbCommand.ExecuteReader(CommandBehavior behavior)\r\n   at System.Data.Entity.Core.Mapping.Update.Internal.DynamicUpdateCommand.Execute(Dictionary`2 identifierValues, List`1 generatedValues)\r\n   at System.Data.Entity.Core.Mapping.Update.Internal.UpdateTranslator.Update()\r\n   --- End of inner exception stack trace ---\r\n   at System.Data.Entity.Core.Mapping.Update.Internal.UpdateTranslator.Update()\r\n   at System.Data.Entity.Core.EntityClient.Internal.EntityAdapter.<Update>b__2(UpdateTranslator ut)\r\n   at System.Data.Entity.Core.EntityClient.Internal.EntityAdapter.Update[T](T noChangesResult, Func`2 updateFunction)\r\n   at System.Data.Entity.Core.EntityClient.Internal.EntityAdapter.Update()\r\n   at System.Data.Entity.Core.Objects.ObjectContext.<SaveChangesToStore>b__35()\r\n   at System.Data.Entity.Core.Objects.ObjectContext.ExecuteInTransaction[T](Func`1 func, IDbExecutionStrategy executionStrategy, Boolean startLocalTransaction, Boolean releaseConnectionOnSuccess)\r\n   at System.Data.Entity.Core.Objects.ObjectContext.SaveChangesToStore(SaveOptions options, IDbExecutionStrategy executionStrategy, Boolean startLocalTransaction)\r\n   at System.Data.Entity.Core.Objects.ObjectContext.<>c__DisplayClass2a.<SaveChangesInternal>b__27()\r\n   at System.Data.Entity.Infrastructure.DefaultExecutionStrategy.Execute[TResult](Func`1 operation)\r\n   at System.Data.Entity.Core.Objects.ObjectContext.SaveChangesInternal(SaveOptions options, Boolean executeInExistingTransaction)\r\n   at System.Data.Entity.Core.Objects.ObjectContext.SaveChanges(SaveOptions options)\r\n   at System.Data.Entity.Internal.InternalContext.SaveChanges()\r\n   --- End of inner exception stack trace ---\r\n   at System.Data.Entity.Internal.InternalContext.SaveChanges()\r\n   at System.Data.Entity.Internal.LazyInternalContext.SaveChanges()\r\n   at System.Data.Entity.DbContext.SaveChanges()\r\n   at Library.YTB.DA.Profiles.Profiles.isErrorSecurityCode(Int32 lido) in D:\\MoneyUp\\git\\library.ytb\\Library.YTB.DA\\Profiles\\Profiles.cs:line 289', '2020-10-29 16:58:30');

-- ----------------------------
-- Table structure for GiaoDich
-- ----------------------------
DROP TABLE IF EXISTS `GiaoDich`;
CREATE TABLE `GiaoDich` (
  `MaGiaoDich` bigint(20) NOT NULL AUTO_INCREMENT,
  `MaKhachHang` bigint(20) DEFAULT NULL,
  `NhanVienDuyet` varchar(255) COLLATE utf8mb4_vietnamese_ci DEFAULT '',
  `NgayTao` datetime DEFAULT current_timestamp(),
  `TrangThaiDuyet` int(1) DEFAULT NULL,
  `GhiChu` varchar(255) COLLATE utf8mb4_vietnamese_ci DEFAULT NULL,
  `NganHang` varchar(255) COLLATE utf8mb4_vietnamese_ci DEFAULT NULL,
  `Sotaikhoan` varchar(150) COLLATE utf8mb4_vietnamese_ci DEFAULT NULL,
  `Tentaikhoan` varchar(255) COLLATE utf8mb4_vietnamese_ci DEFAULT NULL,
  `SoTien` decimal(10,0) DEFAULT NULL,
  `SoDuSauRut` decimal(10,0) DEFAULT NULL,
  PRIMARY KEY (`MaGiaoDich`),
  KEY `fk_idkh_gg` (`MaKhachHang`),
  KEY `fk_iduser_gg` (`NhanVienDuyet`),
  KEY `fk_idnganhang_gg` (`NganHang`),
  CONSTRAINT `fk_idkh_gg` FOREIGN KEY (`MaKhachHang`) REFERENCES `KhachHang` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=36 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_vietnamese_ci;

-- ----------------------------
-- Records of GiaoDich
-- ----------------------------
INSERT INTO `GiaoDich` VALUES ('35', '24', 'longchin196', '2020-11-02 15:02:54', '2', 'long le', 'Vietcombank', '9776756545', 'lê ngọc', '150000', '318238');

-- ----------------------------
-- Table structure for Kenh
-- ----------------------------
DROP TABLE IF EXISTS `Kenh`;
CREATE TABLE `Kenh` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `ChannelsID` text COLLATE utf8mb4_vietnamese_ci DEFAULT NULL,
  `TenKenh` varchar(255) COLLATE utf8mb4_vietnamese_ci DEFAULT NULL,
  `EmbedDemo` varchar(255) COLLATE utf8mb4_vietnamese_ci DEFAULT NULL,
  `EmailKenh` varchar(100) COLLATE utf8mb4_vietnamese_ci DEFAULT NULL,
  `Image` text COLLATE utf8mb4_vietnamese_ci DEFAULT NULL,
  `PassKenh` varchar(100) COLLATE utf8mb4_vietnamese_ci DEFAULT NULL,
  `Sdt` varchar(50) COLLATE utf8mb4_vietnamese_ci DEFAULT NULL,
  `SoluotSub` int(20) NOT NULL,
  `GiatienSub` decimal(20,0) NOT NULL,
  `isPopular` bit(1) NOT NULL,
  `isPrioritize` bit(1) NOT NULL,
  `isActive` bit(1) NOT NULL,
  `Create_by` varchar(150) COLLATE utf8mb4_vietnamese_ci DEFAULT NULL,
  `Create_Date` datetime DEFAULT NULL,
  `isDelete` bit(1) NOT NULL,
  `Token` text COLLATE utf8mb4_vietnamese_ci DEFAULT NULL,
  `ReferToken` text COLLATE utf8mb4_vietnamese_ci DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_vietnamese_ci;

-- ----------------------------
-- Records of Kenh
-- ----------------------------
INSERT INTO `Kenh` VALUES ('1', 'UCFsKuWzh0TmAdfhQ7djM6xA', 't1', 'P3h_HY5QEas', 'e1', '/Lib/images/interFaceyt.jpg', 'p1', '0965', '5', '90', '', '', '', null, null, '\0', null, null);
INSERT INTO `Kenh` VALUES ('2', 'UCAwT6OP-zmruMb4EjSRFQ6w', 't2', 'P3h_HY5QEas', 'e2', '/Lib/images/interFaceyt.jpg', 'p2', '0966', '8', '490', '', '', '', null, '2020-06-01 16:39:33', '\0', null, null);
INSERT INTO `Kenh` VALUES ('3', 'UCb2iEG1CZVwGCC1VQaRo7ZQ', 't3', 'P3h_HY5QEas', 'e3', '/Lib/images/interFaceyt.jpg', 'p2345', '0966', '8', '490', '\0', '', '', null, '2020-06-01 16:40:12', '\0', null, null);
INSERT INTO `Kenh` VALUES ('4', 'UCJxDsxqe73u3icx-20RrLvA', 'Kênh VTV16', 'P3h_HY5QEas', 'VTV@125gmail.com', '/Lib/images/interFaceyt.jpg', '123456', '3123123123', '8', '10000', '\0', '', '', null, '2020-06-14 01:19:37', '\0', null, null);
INSERT INTO `Kenh` VALUES ('5', 'UC9UAICjSV95lFT5Dr86Nq3w', 'Kênh HCVB6', 'P3h_HY5QEas', 'HCVB@gmail.com', '/Lib/images/interFaceyt.jpg', '123456', '3123123123', '2', '20000', '\0', '\0', '', null, '2020-06-14 01:25:12', '\0', null, null);
INSERT INTO `Kenh` VALUES ('6', 'UCheTCcg8516oYzSCvktY-PA', 'longchin', 'P3h_HY5QEas', 'lvlong96123@gmail.com', '/Lib/images/interFaceyt.jpg', '12345678', '0965143609', '6', '230', '', '\0', '', 'longchin196', '2020-06-15 21:08:17', '\0', null, null);
INSERT INTO `Kenh` VALUES ('7', 'UCxzLBrxcIMadIBfguNGffpw', 'PSMH', 'P3h_HY5QEas', null, '/Lib/images/interFaceyt.jpg', null, null, '4', '350', '', '\0', '', 'longchin196', '2020-09-07 09:51:31', '\0', null, null);
INSERT INTO `Kenh` VALUES ('10', 'UCJxDsxqe73u3icx-20RrLvA', 'Nhạc Sàn Vn', 'P3h_HY5QEas', null, '/Lib/images/interFaceyt.jpg', null, null, '2', '900', '', '\0', '', 'longchin', '2020-09-07 15:58:22', '\0', null, null);
INSERT INTO `Kenh` VALUES ('11', 'UC3mPkQsnjNSr_StmZQkJDoA', 'VIỆT MIX PLUS', 'P3h_HY5QEas', null, '/Lib/images/interFaceyt.jpg', null, null, '2', '600', '\0', '\0', '', 'longchin', '2020-09-07 16:08:50', '\0', null, null);
INSERT INTO `Kenh` VALUES ('12', 'UC2OzDIKOESoKIdR1VtwCAAQ', 'G Entertainment ', 'P3h_HY5QEas', null, '/Lib/images/interFaceyt.jpg', null, null, '4', '400', '\0', '\0', '', 'longchin', '2020-09-07 16:09:22', '\0', null, null);
INSERT INTO `Kenh` VALUES ('13', 'UC4MauP5aEeRNSuwOM9a4Vvg', 'Jenny Remix', 'P3h_HY5QEas', null, '/Lib/images/interFaceyt.jpg', null, null, '4', '300', '\0', '\0', '', 'longchin', '2020-09-07 16:09:52', '\0', null, null);
INSERT INTO `Kenh` VALUES ('14', 'UCQ7d2lO-qGUy6wwM4n3KHfw', 'Phan Mạnh Quỳnh Official ', 'P3h_HY5QEas', null, '/Lib/images/interFaceyt.jpg', null, null, '9', '200', '', '', '', 'longchin', '2020-09-07 16:10:28', '\0', null, null);
INSERT INTO `Kenh` VALUES ('15', 'UClyA28-01x4z60eWQ2kiNbA', 'Sơn Tùng M-TP Official ', 'P3h_HY5QEas', null, '/Lib/images/interFaceyt.jpg', null, null, '6', '200', '', '', '', 'longchin', '2020-09-07 16:14:30', '\0', null, null);
INSERT INTO `Kenh` VALUES ('16', 'UCc2LH-7dNH3gZc6yFDFgHGg', 'TACA CHANNEL NEW', 'owXgaQnd8XM', null, '/Lib/images/interFaceyt.jpg', null, null, '0', '350', '', '', '\0', 'longchin196', '2020-11-02 19:45:25', '\0', null, null);

-- ----------------------------
-- Table structure for KhachHang
-- ----------------------------
DROP TABLE IF EXISTS `KhachHang`;
CREATE TABLE `KhachHang` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `code` varchar(20) COLLATE utf8mb4_vietnamese_ci DEFAULT '',
  `UserName` varchar(255) COLLATE utf8mb4_vietnamese_ci DEFAULT NULL,
  `Password` varchar(255) COLLATE utf8mb4_vietnamese_ci DEFAULT NULL,
  `Email` varchar(255) COLLATE utf8mb4_vietnamese_ci DEFAULT NULL,
  `DiaChi` varchar(255) COLLATE utf8mb4_vietnamese_ci DEFAULT NULL,
  `NgaySinh` date DEFAULT NULL,
  `SDT` varchar(255) COLLATE utf8mb4_vietnamese_ci DEFAULT NULL,
  `GioiTinh` int(255) DEFAULT NULL,
  `MaAnToan` int(255) DEFAULT NULL COMMENT 'Ma an toan (mat khau rut tien)',
  `MaGioiThieu` varchar(255) COLLATE utf8mb4_vietnamese_ci DEFAULT NULL,
  `Recommendedby` varchar(255) COLLATE utf8mb4_vietnamese_ci DEFAULT NULL,
  `SoDu` decimal(10,0) DEFAULT NULL,
  `SoduThucte` decimal(10,0) DEFAULT NULL,
  `Sodukichhoat` decimal(10,0) DEFAULT NULL,
  `TTKiemTien` bit(1) DEFAULT NULL,
  `TTXacThucTK` bit(1) DEFAULT NULL,
  `TTXacThucCmt` bit(1) DEFAULT b'0',
  `IsActive` bit(1) DEFAULT NULL,
  `IsDelete` bit(1) DEFAULT NULL,
  `Create_by` varchar(20) COLLATE utf8mb4_vietnamese_ci DEFAULT NULL,
  `Create_Date` datetime DEFAULT NULL,
  `first_name` varchar(50) COLLATE utf8mb4_vietnamese_ci DEFAULT NULL,
  `last_name` varchar(50) COLLATE utf8mb4_vietnamese_ci DEFAULT NULL,
  `full_name` varchar(150) COLLATE utf8mb4_vietnamese_ci DEFAULT NULL,
  `Role_id` int(11) NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `fk_khachhang_role` (`Role_id`),
  KEY `ID` (`ID`),
  KEY `ID_2` (`ID`),
  CONSTRAINT `fk_khachhang_role` FOREIGN KEY (`Role_id`) REFERENCES `Role` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=38 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_vietnamese_ci;

-- ----------------------------
-- Records of KhachHang
-- ----------------------------
INSERT INTO `KhachHang` VALUES ('24', 'IV0015', 'longchin', '74477211373d00c255b082dea4a966e1', 'lvlong96k@gmail.com', null, '1996-12-28', '0965143609', '2', '123456', 'IV0015', null, '493820', '293820', '200000', '', '', '\0', '', '\0', 'longchin', '2020-09-06 15:53:33', 'LÊ', 'LONG', 'LÊ LONG', '4');
INSERT INTO `KhachHang` VALUES ('25', 'IV0016', 'huyennguyen', '74477211373d00c255b082dea4a966e1', 'vanlongpu@gmail.com', null, null, null, null, '467612', 'IV0016', 'IV0015', '0', '0', '0', '', '', '\0', '', '\0', 'huyennguyen', '2020-09-06 16:11:12', 'NGUYỄN', 'HUYỀN', 'NGUYỄN THỊ HUYỀN', '4');
INSERT INTO `KhachHang` VALUES ('26', 'IV0017', 'tunganhdev', 'b2a1eaabb7c1d254c313c8f8dc8339ca', 'dovantunghy186@gmail.com', null, null, null, null, null, 'IV0017', null, '0', '0', '0', '\0', '\0', '\0', '', '\0', 'tunganhdev', '2020-09-08 22:13:21', 'Đỗ', 'Tùng', 'Đỗ Văn Tùng', '4');
INSERT INTO `KhachHang` VALUES ('27', 'IV0018', 'ngocle405', '74477211373d00c255b082dea4a966e1', 'ngocle405@gmail.com', null, null, null, null, null, 'IV0018', 'IV0015', '200000', '0', '200000', '', '\0', '\0', '', '\0', 'longchin1961', '2020-09-14 11:30:30', 'LÊ', 'NGỌC', 'LÊ THANH NGỌC', '4');
INSERT INTO `KhachHang` VALUES ('28', 'IV0019', 'test', 'c91b9f8c0be4831296c0cbbb7c511557', 'dolee864@gmail.com', null, null, null, null, null, 'IV0019', null, '0', '0', '0', '\0', '\0', '\0', '', '\0', 'test', '2020-09-21 18:32:55', 'Đỗ', 'Tùng', 'Đỗ Văn Tùng', '4');
INSERT INTO `KhachHang` VALUES ('30', 'IV0021', 'longchin123', '74477211373d00c255b082dea4a966e1', 'lvlong9666k@gmail.com', null, null, null, null, null, 'IV0021', null, '0', '0', '200000', '\0', '\0', '\0', '', '\0', 'longchin123', '2020-10-07 15:27:13', 'Lê', 'Ngọc', 'Lê Thanh Thị Ngọc', '4');
INSERT INTO `KhachHang` VALUES ('31', 'IV0022', 'longchin1234', '74477211373d00c255b082dea4a966e1', 'lvlong96gk@gmail.com', null, null, null, null, null, 'IV0022', null, '0', '0', '200000', '\0', '\0', '\0', '', '\0', 'longchin1234', '2020-10-07 15:27:58', 'Lê', 'Ngọc', 'Lê Thanh Thị Ngọc', '4');
INSERT INTO `KhachHang` VALUES ('32', 'IV0023', 'longchin89', '74477211373d00c255b082dea4a966e1', 'ngocle4056@gmail.com', null, null, null, null, null, 'IV0023', null, '0', '0', '200000', '\0', '\0', '\0', '', '\0', 'longchin89', '2020-10-07 15:29:08', 'Lê', 'Ngọc', 'Lê Thanh Thị Ngọc', '4');
INSERT INTO `KhachHang` VALUES ('33', 'IV0024', 'longchin9697', '74477211373d00c255b082dea4a966e1', 'lvlong9667k@gmail.com', null, null, null, null, null, 'IV0024', null, '0', '0', '200000', '\0', '\0', '\0', '', '\0', 'longchin9697', '2020-10-07 15:29:52', 'Lê', 'Ngọc', 'Lê Thanh Thị Ngọc', '4');
INSERT INTO `KhachHang` VALUES ('34', 'IV0025', 'longchin9684', '74477211373d00c255b082dea4a966e1', 'lvlong9684k@gmail.com', null, null, null, null, null, 'IV0025', null, '0', '0', '200000', '\0', '\0', '\0', '', '\0', 'longchin9684', '2020-10-07 15:42:30', 'Lê', 'Ngọc', 'Lê Thanh Thị Ngọc', '4');
INSERT INTO `KhachHang` VALUES ('35', 'IV0026', 'dovantunghy1863', '74477211373d00c255b082dea4a966e1', 'dovantunghy1863@gmail.com', null, null, null, null, null, 'IV0026', null, '0', '0', '200000', '\0', '\0', '\0', '', '\0', 'dovantunghy1863', '2020-10-07 15:44:13', 'Lê', 'Ngọc', 'Lê Thanh Thị Ngọc', '4');
INSERT INTO `KhachHang` VALUES ('36', 'IV0027', 'longchin113', '74477211373d00c255b082dea4a966e1', 'lvlong96kkkk@gmail.com', null, null, null, null, null, 'IV0027', null, '0', '0', '0', '\0', '\0', '\0', '', '\0', 'longchin113', '2020-10-11 12:41:36', 'Lê', 'Ngọc', 'Lê Thanh Thị Ngọc', '4');
INSERT INTO `KhachHang` VALUES ('37', 'IV0028', 'nhole', '74477211373d00c255b082dea4a966e1', 'nhole197@gmail.com', null, null, null, null, null, 'IV0028', 'IV0015', '200000', '0', null, '', '\0', null, '', '\0', 'nhole', '2020-11-02 15:38:34', 'lê', 'nho', 'lê nho', '4');

-- ----------------------------
-- Table structure for KhangNghi
-- ----------------------------
DROP TABLE IF EXISTS `KhangNghi`;
CREATE TABLE `KhangNghi` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `MaKhachHang` bigint(20) DEFAULT NULL,
  `Admin` varchar(255) DEFAULT NULL,
  `TieuDe` varchar(200) DEFAULT NULL,
  `NoiDung` varchar(2000) DEFAULT NULL,
  `Phanhoi` varchar(2000) DEFAULT NULL,
  `NgayTao` datetime DEFAULT NULL,
  `TrangThaiPhanHoi` int(1) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `fk_kh_kg` (`MaKhachHang`),
  CONSTRAINT `fk_kh_kg` FOREIGN KEY (`MaKhachHang`) REFERENCES `KhachHang` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of KhangNghi
-- ----------------------------

-- ----------------------------
-- Table structure for LyDoTichTien
-- ----------------------------
DROP TABLE IF EXISTS `LyDoTichTien`;
CREATE TABLE `LyDoTichTien` (
  `ID` bigint(11) NOT NULL AUTO_INCREMENT,
  `TenLyDo` varchar(255) DEFAULT NULL,
  `TrangThai` bit(1) DEFAULT NULL,
  `GhiChu` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of LyDoTichTien
-- ----------------------------
INSERT INTO `LyDoTichTien` VALUES ('1', 'Tich tien xem video', '', null);
INSERT INTO `LyDoTichTien` VALUES ('2', 'Tich tien sub kenh', '', null);
INSERT INTO `LyDoTichTien` VALUES ('3', 'Tich tien ma gioi thieu', '', null);

-- ----------------------------
-- Table structure for MD_NganHang
-- ----------------------------
DROP TABLE IF EXISTS `MD_NganHang`;
CREATE TABLE `MD_NganHang` (
  `MaNganHang` bigint(20) NOT NULL AUTO_INCREMENT,
  `TenNganHang` varchar(255) COLLATE utf8mb4_vietnamese_ci NOT NULL,
  `Code` varchar(20) COLLATE utf8mb4_vietnamese_ci DEFAULT NULL,
  `Create_By` varchar(255) COLLATE utf8mb4_vietnamese_ci DEFAULT NULL,
  `CreatedDate` datetime DEFAULT current_timestamp(),
  `isDelete` bit(1) NOT NULL,
  `isActive` bit(1) NOT NULL,
  PRIMARY KEY (`MaNganHang`)
) ENGINE=InnoDB AUTO_INCREMENT=795 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_vietnamese_ci;

-- ----------------------------
-- Records of MD_NganHang
-- ----------------------------
INSERT INTO `MD_NganHang` VALUES ('33', 'Test Ngân Hàng BIDV', null, 'longchin196', '2020-06-03 03:53:17', '', '');
INSERT INTO `MD_NganHang` VALUES ('260', 'Vietcombank', null, 'longchin196', '2020-09-13 10:12:16', '\0', '');
INSERT INTO `MD_NganHang` VALUES ('485', 'Test Vietcombank (VCB)', null, null, '2020-06-04 11:32:37', '', '');
INSERT INTO `MD_NganHang` VALUES ('609', 'Test Saocombank (SCB)', null, null, '2020-06-04 11:38:17', '', '');
INSERT INTO `MD_NganHang` VALUES ('660', 'BIDV', null, null, '2020-06-13 18:43:05', '', '');
INSERT INTO `MD_NganHang` VALUES ('717', 'Techcombank', null, 'longchin196', '2020-09-13 10:12:06', '\0', '');
INSERT INTO `MD_NganHang` VALUES ('794', 'BIDV', null, 'longchin196', '2020-09-13 10:12:27', '\0', '');

-- ----------------------------
-- Table structure for Menu
-- ----------------------------
DROP TABLE IF EXISTS `Menu`;
CREATE TABLE `Menu` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `TenMenu` varchar(255) DEFAULT NULL,
  `ThuTu` varchar(255) DEFAULT NULL,
  `MoTa` varchar(2000) DEFAULT NULL,
  `TrangThai` bit(1) NOT NULL,
  `isDelete` bit(1) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of Menu
-- ----------------------------

-- ----------------------------
-- Table structure for MyProject
-- ----------------------------
DROP TABLE IF EXISTS `MyProject`;
CREATE TABLE `MyProject` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ProjectName` varchar(255) DEFAULT NULL,
  `Email` varchar(255) DEFAULT NULL,
  `IDclient` text DEFAULT NULL,
  `CodeSecret` text DEFAULT NULL,
  `isActive` bit(1) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of MyProject
-- ----------------------------
INSERT INTO `MyProject` VALUES ('1', 'long', null, null, null, '');
INSERT INTO `MyProject` VALUES ('2', 'long1', null, null, null, '\0');

-- ----------------------------
-- Table structure for NguyenNhanKhoa
-- ----------------------------
DROP TABLE IF EXISTS `NguyenNhanKhoa`;
CREATE TABLE `NguyenNhanKhoa` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `Name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of NguyenNhanKhoa
-- ----------------------------
INSERT INTO `NguyenNhanKhoa` VALUES ('1', 'Khoas do sai mật khẩu');
INSERT INTO `NguyenNhanKhoa` VALUES ('2', 'khóa do nhập sai mã an toàn tại màn hình đổi mã an toàn');
INSERT INTO `NguyenNhanKhoa` VALUES ('3', 'khóa do nhập sai mật khẩu tại màn hình đổi mã an toàn');
INSERT INTO `NguyenNhanKhoa` VALUES ('4', 'khóa do nhập sai mật khẩu tại màn hình đăng nhập');
INSERT INTO `NguyenNhanKhoa` VALUES ('5', 'khóa do nhập sai mật khẩu tại màn hình đổi mật khẩu');

-- ----------------------------
-- Table structure for Role
-- ----------------------------
DROP TABLE IF EXISTS `Role`;
CREATE TABLE `Role` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(20) COLLATE utf8mb4_vietnamese_ci DEFAULT NULL,
  `name` varchar(100) CHARACTER SET utf8 NOT NULL,
  `isDelete` bit(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_vietnamese_ci;

-- ----------------------------
-- Records of Role
-- ----------------------------
INSERT INTO `Role` VALUES ('1', '111', 'admin', '\0');
INSERT INTO `Role` VALUES ('2', '222', 'Quan tri vien', '\0');
INSERT INTO `Role` VALUES ('3', '333', 'Nhan vien', '\0');
INSERT INTO `Role` VALUES ('4', '444', 'Nguoi dung', '\0');
INSERT INTO `Role` VALUES ('15', '00001', 'longchin196', '');
INSERT INTO `Role` VALUES ('16', '99999', 'test', '\0');

-- ----------------------------
-- Table structure for SanPham
-- ----------------------------
DROP TABLE IF EXISTS `SanPham`;
CREATE TABLE `SanPham` (
  `MaSanPham` bigint(255) NOT NULL AUTO_INCREMENT,
  `TenSanPham` varchar(250) COLLATE utf8mb4_vietnamese_ci NOT NULL,
  `AnhSanPham` text COLLATE utf8mb4_vietnamese_ci NOT NULL,
  `MoTa` text COLLATE utf8mb4_vietnamese_ci DEFAULT NULL,
  `ChiTiet` text COLLATE utf8mb4_vietnamese_ci DEFAULT NULL,
  `AnhMoTa1` text COLLATE utf8mb4_vietnamese_ci DEFAULT NULL,
  `AnhMoTa2` text COLLATE utf8mb4_vietnamese_ci DEFAULT NULL,
  `AnhMoTa3` text COLLATE utf8mb4_vietnamese_ci DEFAULT NULL,
  `AnhMoTa4` text COLLATE utf8mb4_vietnamese_ci DEFAULT NULL,
  `GiaBan` decimal(10,0) NOT NULL,
  `GiaKM` decimal(10,0) DEFAULT NULL,
  `SoLuong` int(11) DEFAULT NULL,
  `LuotXem` int(11) DEFAULT NULL,
  `ThuTuUuTien` int(11) DEFAULT NULL,
  `TrangThai` bit(1) NOT NULL,
  `CreateBy` varchar(150) COLLATE utf8mb4_vietnamese_ci DEFAULT NULL,
  `CreateDate` datetime DEFAULT NULL,
  `isDelete` bit(1) NOT NULL,
  PRIMARY KEY (`MaSanPham`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_vietnamese_ci;

-- ----------------------------
-- Records of SanPham
-- ----------------------------
INSERT INTO `SanPham` VALUES ('1', 'San pham tesst1', '1', '2', '3', '4', '5', '6', '7', '500000', '49900', '3', '4', '1', '', 'long', '2020-05-30 21:44:16', '\0');
INSERT INTO `SanPham` VALUES ('2', 'San pham tesst2', '1', null, '3', '4', '5', '6', '7', '700000', '20000', '8', '4', '1', '\0', null, '2020-05-30 22:04:26', '\0');
INSERT INTO `SanPham` VALUES ('3', 'Tên san ph?m 3', '1', null, '3', '4', '5', '6', '7', '700000', '20000', '8', '4', '1', '\0', null, '2020-05-30 22:06:36', '\0');
INSERT INTO `SanPham` VALUES ('4', 'Tên s?n ph?m 4', '1', null, '3', '4', '5', '6', '7', '0', '0', '0', '0', '1', '', null, '2020-05-30 22:32:44', '');
INSERT INTO `SanPham` VALUES ('5', 'Tên S?n Ph?m  4', '1', null, '3', '4', '5', '6', '7', '700000', '20000', '8', '0', null, '', null, '2020-05-30 23:08:03', '\0');
INSERT INTO `SanPham` VALUES ('6', 'Tên san pham 5', '1', null, '3', '4', '5', '6', '7', '700000', '20000', '8', '0', null, '', null, '2020-05-30 23:08:58', '\0');
INSERT INTO `SanPham` VALUES ('7', 'ten san pham 8', '1', null, '3', '4', '5', '6', '7', '700000', '20000', '8', '0', '1', '\0', null, '2020-05-30 23:09:23', '\0');
INSERT INTO `SanPham` VALUES ('8', 'Tên s?n pham 6', '1', null, '3', '4', '5', '6', '7', '700000', '20000', '8', '0', '1', '', null, '2020-05-30 23:11:37', '\0');
INSERT INTO `SanPham` VALUES ('9', 'Test Thêm Moi sua', '?nh 1', null, 'Chi ti?t 1', '1', '12', '32', '455', '50000', '1000', '2', null, '2', '', null, '2020-06-14 00:32:24', '\0');
INSERT INTO `SanPham` VALUES ('10', 'San pham BCSG 1', '1', 'S?n ph?m này là s?n ph?m mô t?', 'Chi ti?t 1', '1', '12', '32', '455', '50000', '1000', '5', null, '3', '', null, '2020-06-14 00:40:32', '\0');
INSERT INTO `SanPham` VALUES ('11', 'San pham BCSG 11111', '1', null, null, null, null, '32', '455', '77000', '9000', '5', null, null, '', 'longchin196', '2020-06-14 16:05:15', '');

-- ----------------------------
-- Table structure for Secon
-- ----------------------------
DROP TABLE IF EXISTS `Secon`;
CREATE TABLE `Secon` (
  `ID` bigint(11) NOT NULL AUTO_INCREMENT,
  `SeconID` int(11) NOT NULL COMMENT 'id 1: xem video , id 2:subcribe',
  `UserID` bigint(11) NOT NULL,
  `SeconName` varchar(255) DEFAULT NULL,
  `Secons` int(255) DEFAULT NULL COMMENT 'so giay',
  PRIMARY KEY (`ID`),
  KEY `Secon_user` (`UserID`),
  CONSTRAINT `Secon_user` FOREIGN KEY (`UserID`) REFERENCES `KhachHang` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of Secon
-- ----------------------------
INSERT INTO `Secon` VALUES ('1', '2', '24', 'xem video + subcribe', '10');
INSERT INTO `Secon` VALUES ('2', '1', '24', 'xem video', '30');

-- ----------------------------
-- Table structure for SubcriberMonney
-- ----------------------------
DROP TABLE IF EXISTS `SubcriberMonney`;
CREATE TABLE `SubcriberMonney` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `idKenh` bigint(20) DEFAULT NULL,
  `channelsID` text DEFAULT NULL COMMENT 'kênh của trang',
  `Price` decimal(10,2) DEFAULT NULL,
  `Username` varchar(255) DEFAULT NULL,
  `isDelete` bit(1) DEFAULT NULL,
  `channelsToUserID` varchar(200) DEFAULT '' COMMENT 'kênh của người dùng',
  `Create_date` datetime DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `fk_submonney_kenh` (`idKenh`),
  CONSTRAINT `fk_submonney_kenh` FOREIGN KEY (`idKenh`) REFERENCES `Kenh` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=138 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of SubcriberMonney
-- ----------------------------
INSERT INTO `SubcriberMonney` VALUES ('128', '14', 'UCQ7d2lO-qGUy6wwM4n3KHfw', '200.00', 'longchin', '\0', 'UCX-N1_F1soE1flx6XP3xQBA', '2020-10-31 22:00:00');
INSERT INTO `SubcriberMonney` VALUES ('129', '1', 'UCFsKuWzh0TmAdfhQ7djM6xA', '90.00', 'longchin', '\0', 'UCX-N1_F1soE1flx6XP3xQBA', '2020-10-31 22:20:05');
INSERT INTO `SubcriberMonney` VALUES ('130', '2', 'UCAwT6OP-zmruMb4EjSRFQ6w', '490.00', 'longchin', '\0', 'UCX-N1_F1soE1flx6XP3xQBA', '2020-10-31 22:48:49');
INSERT INTO `SubcriberMonney` VALUES ('131', '4', 'UCJxDsxqe73u3icx-20RrLvA', '10000.00', 'longchin', '\0', 'UCX-N1_F1soE1flx6XP3xQBA', '2020-10-31 23:20:26');
INSERT INTO `SubcriberMonney` VALUES ('132', '15', 'UClyA28-01x4z60eWQ2kiNbA', '200.00', 'longchin', '\0', 'UCX-N1_F1soE1flx6XP3xQBA', '2020-10-31 23:23:46');
INSERT INTO `SubcriberMonney` VALUES ('133', '3', 'UCb2iEG1CZVwGCC1VQaRo7ZQ', '490.00', 'longchin', '\0', 'UCX-N1_F1soE1flx6XP3xQBA', '2020-10-31 23:39:33');
INSERT INTO `SubcriberMonney` VALUES ('134', '2', 'UCAwT6OP-zmruMb4EjSRFQ6w', '490.00', 'longchin', '\0', 'UCFsKuWzh0TmAdfhQ7djM6xA', '2020-11-02 14:07:34');
INSERT INTO `SubcriberMonney` VALUES ('135', '14', 'UCQ7d2lO-qGUy6wwM4n3KHfw', '200.00', 'longchin', '\0', 'UCFsKuWzh0TmAdfhQ7djM6xA', '2020-11-02 14:54:18');
INSERT INTO `SubcriberMonney` VALUES ('136', '13', 'UC4MauP5aEeRNSuwOM9a4Vvg', '300.00', 'longchin', '\0', 'UCFsKuWzh0TmAdfhQ7djM6xA', '2020-11-02 15:00:32');
INSERT INTO `SubcriberMonney` VALUES ('137', '12', 'UC2OzDIKOESoKIdR1VtwCAAQ', '400.00', 'longchin', '\0', 'UCFsKuWzh0TmAdfhQ7djM6xA', '2020-11-02 15:10:38');

-- ----------------------------
-- Table structure for ThongBao
-- ----------------------------
DROP TABLE IF EXISTS `ThongBao`;
CREATE TABLE `ThongBao` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `TieuDe` varchar(1000) DEFAULT NULL,
  `MoTa` varchar(2000) DEFAULT NULL,
  `DaXem` bit(1) DEFAULT NULL,
  `NgayTao` datetime DEFAULT NULL,
  `Username` varchar(255) DEFAULT NULL,
  `ToanThanhvien` bit(1) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=35 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of ThongBao
-- ----------------------------
INSERT INTO `ThongBao` VALUES ('19', 'Cộng tiền từ giới thiệu', 'bạn vừa được cộng 25000 xu, từ việc kích hoạt tài khoản của thành viên NGUYỄN THỊ HUYỀN', '', '2020-09-14 11:21:44', 'longchin', '\0');
INSERT INTO `ThongBao` VALUES ('20', 'Cộng tiền từ giới thiệu', 'bạn vừa được cộng 25000 xu, từ việc kích hoạt tài khoản của thành viên LÊ THANH NGỌC', '', '2020-09-14 11:37:19', 'longchin', '\0');
INSERT INTO `ThongBao` VALUES ('21', 'Yêu cầu kích hoạt kiếm tiền', 'Xin chúc mừng, tài khoản của bạn đã được bật kiếm tiền thành công', '', '2020-09-14 11:37:20', 'ngocle405', '\0');
INSERT INTO `ThongBao` VALUES ('22', 'Bạn có một yêu cầu rút tiền bị hủy ', 'Yêu cầu chuyển tiền bị hủy bỏ do không đủ điều kiện, mã giao dịch ( 25 ), gửi kháng nghị kèm theo mã giao dịch để được xem xét lại', '', '2020-09-14 15:23:44', 'longchin', '\0');
INSERT INTO `ThongBao` VALUES ('23', 'Bạn có một yêu cầu rút tiền bị hủy ', 'Chúng tôi đã chuyển 50000 vào số tài khoản: 19032778590018 ,Ngân hàng: Techcombank ,Chủ tài khoản: LE VAN LONG', '', '2020-09-14 15:26:41', 'longchin', '\0');
INSERT INTO `ThongBao` VALUES ('24', 'Bạn có một yêu cầu rút tiền bị hủy ', 'Chúng tôi đã chuyển 150000 vào số tài khoản: 19032778590018 ,Ngân hàng: Vietcombank ,Chủ tài khoản: LE VAN LONG', '', '2020-09-15 14:36:38', 'longchin', '\0');
INSERT INTO `ThongBao` VALUES ('25', 'Yêu cầu chuyển tiền chấp thuận', 'Chúng tôi đã chuyển 200000 vào số tài khoản: 123214374443 ,Ngân hàng: Vietcombank ,Chủ tài khoản: LE VAN LONG', '', '2020-09-15 15:15:00', 'longchin', '\0');
INSERT INTO `ThongBao` VALUES ('26', 'Yêu cầu chuyển tiền hủy bỏ', 'Yêu cầu chuyển tiền bị hủy bỏ do không đủ điều kiện, mã giao dịch ( 27 ), gửi kháng nghị kèm theo mã giao dịch để được xem xét lại', '', '2020-09-15 15:16:34', 'longchin', '\0');
INSERT INTO `ThongBao` VALUES ('27', 'Yêu cầu chuyển tiền hủy bỏ', 'Yêu cầu chuyển tiền bị hủy bỏ do không đủ điều kiện , mã giao dịch ( 29 ), gửi kháng nghị kèm theo mã giao dịch để được xem xét lại', '', '2020-10-12 01:01:04', 'longchin', '\0');
INSERT INTO `ThongBao` VALUES ('28', 'Yêu cầu chuyển tiền chấp thuận', 'Chúng tôi đã chuyển 50000 vào số tài khoản: 19032778590018 ,Ngân hàng: Techcombank ,Chủ tài khoản: LE VAN LONG', '', '2020-10-12 10:14:10', 'longchin', '\0');
INSERT INTO `ThongBao` VALUES ('29', 'Yêu cầu chuyển tiền chấp thuận', 'Chúng tôi đã chuyển 150000 vào số tài khoản: 1233543534 ,Ngân hàng: Techcombank ,Chủ tài khoản: LE VAN LONG', '\0', '2020-10-12 10:32:37', 'longchin', '\0');
INSERT INTO `ThongBao` VALUES ('30', 'Yêu cầu chuyển tiền chấp thuận', 'Chúng tôi đã chuyển 150000 vào số tài khoản: 1001234321 ,Ngân hàng: Techcombank ,Chủ tài khoản: LE VAN LONG', '\0', '2020-10-12 10:35:37', 'longchin', '\0');
INSERT INTO `ThongBao` VALUES ('31', 'Yêu cầu chuyển tiền chấp thuận', 'Chúng tôi đã chuyển 200000 vào số tài khoản: 19032778590018 ,Ngân hàng: Techcombank ,Chủ tài khoản: LE VAN LONG', '', '2020-10-12 10:38:43', 'longchin', '\0');
INSERT INTO `ThongBao` VALUES ('32', 'Yêu cầu chuyển tiền chấp thuận', 'Chúng tôi đã chuyển 150000 vào số tài khoản: 19032778590018 ,Ngân hàng: Vietcombank ,Chủ tài khoản: LE VAN LONG', '', '2020-10-12 10:42:18', 'longchin', '\0');
INSERT INTO `ThongBao` VALUES ('33', 'Cộng tiền từ giới thiệu', 'bạn vừa được cộng 25000 xu, từ việc kích hoạt tài khoản của thành viên lê nho', '', '2020-11-02 15:55:17', 'longchin', '\0');
INSERT INTO `ThongBao` VALUES ('34', 'Yêu cầu kích hoạt kiếm tiền', 'Xin chúc mừng, tài khoản của bạn đã được bật kiếm tiền thành công', '', '2020-11-02 15:55:17', 'nhole', '\0');

-- ----------------------------
-- Table structure for TichTien
-- ----------------------------
DROP TABLE IF EXISTS `TichTien`;
CREATE TABLE `TichTien` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `MaKhachHang` bigint(20) DEFAULT NULL,
  `SoTien` decimal(10,0) DEFAULT NULL,
  `NgayTao` datetime DEFAULT NULL,
  `IDLyDoTichTien` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `FK_LyDo` (`IDLyDoTichTien`),
  KEY `FK_Tichtien_kh` (`MaKhachHang`),
  CONSTRAINT `FK_LyDo` FOREIGN KEY (`IDLyDoTichTien`) REFERENCES `LyDoTichTien` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_Tichtien_kh` FOREIGN KEY (`MaKhachHang`) REFERENCES `KhachHang` (`ID`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=136 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of TichTien
-- ----------------------------
INSERT INTO `TichTien` VALUES ('108', '24', '200', '2020-10-30 00:04:00', '2');
INSERT INTO `TichTien` VALUES ('109', '24', '200', '2020-10-31 21:59:26', '2');
INSERT INTO `TichTien` VALUES ('110', '24', '200', '2020-10-31 22:00:00', '2');
INSERT INTO `TichTien` VALUES ('111', '24', '90', '2020-10-31 22:20:05', '2');
INSERT INTO `TichTien` VALUES ('112', '24', '490', '2020-10-31 22:48:49', '2');
INSERT INTO `TichTien` VALUES ('113', '24', '10000', '2020-10-31 23:20:26', '2');
INSERT INTO `TichTien` VALUES ('114', '24', '200', '2020-10-31 23:23:46', '2');
INSERT INTO `TichTien` VALUES ('115', '24', '490', '2020-10-31 23:39:33', '2');
INSERT INTO `TichTien` VALUES ('116', '24', '25', '2020-11-01 11:05:44', '1');
INSERT INTO `TichTien` VALUES ('117', '24', '14', '2020-11-01 11:10:34', '1');
INSERT INTO `TichTien` VALUES ('118', '24', '60', '2020-11-01 11:11:20', '1');
INSERT INTO `TichTien` VALUES ('119', '24', '90', '2020-11-01 13:38:20', '1');
INSERT INTO `TichTien` VALUES ('120', '24', '490', '2020-11-02 14:07:34', '2');
INSERT INTO `TichTien` VALUES ('121', '24', '25', '2020-11-02 14:31:01', '1');
INSERT INTO `TichTien` VALUES ('122', '24', '14', '2020-11-02 14:33:47', '1');
INSERT INTO `TichTien` VALUES ('123', '24', '60', '2020-11-02 14:37:59', '1');
INSERT INTO `TichTien` VALUES ('124', '24', '70', '2020-11-02 14:45:53', '1');
INSERT INTO `TichTien` VALUES ('125', '24', '91', '2020-11-02 14:47:10', '1');
INSERT INTO `TichTien` VALUES ('126', '24', '200', '2020-11-02 14:54:18', '2');
INSERT INTO `TichTien` VALUES ('127', '24', '0', '2020-11-02 14:55:25', '1');
INSERT INTO `TichTien` VALUES ('128', '24', '50000', '2020-11-02 14:57:01', '1');
INSERT INTO `TichTien` VALUES ('129', '24', '300', '2020-11-02 15:00:33', '2');
INSERT INTO `TichTien` VALUES ('130', '24', '900', '2020-11-02 15:00:48', '1');
INSERT INTO `TichTien` VALUES ('131', '24', '2', '2020-11-02 15:10:27', '1');
INSERT INTO `TichTien` VALUES ('132', '24', '400', '2020-11-02 15:10:38', '2');
INSERT INTO `TichTien` VALUES ('133', '24', '90', '2020-11-02 15:12:42', '1');
INSERT INTO `TichTien` VALUES ('134', '24', '90', '2020-11-02 15:14:14', '1');
INSERT INTO `TichTien` VALUES ('135', '24', '25000', '2020-11-02 15:55:17', '3');

-- ----------------------------
-- Table structure for TinNhan
-- ----------------------------
DROP TABLE IF EXISTS `TinNhan`;
CREATE TABLE `TinNhan` (
  `ID` bigint(20) NOT NULL,
  `TieuDe` varchar(1000) DEFAULT NULL,
  `MoTa` varchar(2000) DEFAULT NULL,
  `NoiDung` varchar(2000) DEFAULT NULL,
  `TrangThai` bit(1) DEFAULT NULL,
  `DaXem` bit(1) DEFAULT NULL,
  `NgayTao` datetime DEFAULT NULL,
  `NguoiTao` varchar(255) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of TinNhan
-- ----------------------------

-- ----------------------------
-- Table structure for Trangthaiyeucau
-- ----------------------------
DROP TABLE IF EXISTS `Trangthaiyeucau`;
CREATE TABLE `Trangthaiyeucau` (
  `id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of Trangthaiyeucau
-- ----------------------------
INSERT INTO `Trangthaiyeucau` VALUES ('1', 'xac nhan email');
INSERT INTO `Trangthaiyeucau` VALUES ('2', 'xac nhan bat kiem tien');
INSERT INTO `Trangthaiyeucau` VALUES ('3', 'xac nhan chung minh thu');

-- ----------------------------
-- Table structure for User
-- ----------------------------
DROP TABLE IF EXISTS `User`;
CREATE TABLE `User` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(150) COLLATE utf8mb4_vietnamese_ci DEFAULT NULL,
  `username` varchar(150) COLLATE utf8mb4_vietnamese_ci DEFAULT NULL,
  `salt` text COLLATE utf8mb4_vietnamese_ci DEFAULT NULL,
  `password` varchar(150) COLLATE utf8mb4_vietnamese_ci DEFAULT NULL,
  `email` text COLLATE utf8mb4_vietnamese_ci DEFAULT NULL,
  `first_name` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `mid_name` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `last_name` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `full_name` varchar(150) CHARACTER SET utf8 DEFAULT NULL,
  `birth_date` datetime DEFAULT NULL,
  `passw_change_required` bit(1) DEFAULT NULL,
  `login_count` int(11) DEFAULT NULL,
  `captcha_code` varchar(20) COLLATE utf8mb4_vietnamese_ci DEFAULT NULL,
  `role_id` int(11) DEFAULT NULL,
  `active` bit(1) DEFAULT NULL,
  `created_by` varchar(150) COLLATE utf8mb4_vietnamese_ci DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `updated_by` varchar(150) COLLATE utf8mb4_vietnamese_ci DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_user_role_id` (`role_id`),
  CONSTRAINT `fk_user_role_id` FOREIGN KEY (`role_id`) REFERENCES `Role` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_vietnamese_ci;

-- ----------------------------
-- Records of User
-- ----------------------------
INSERT INTO `User` VALUES ('2', 'IO0004', 'ngocle', '', '74477211373d00c255b082dea4a966e1', 'lvlong96k@gmail.com', 'lê', 'van', 'long', 'lê van long', '2020-09-06 13:05:03', '\0', '0', null, '3', '', 'Le Van Long', '2020-01-07 11:54:00', 'longchin196', '2020-10-29 16:36:55');
INSERT INTO `User` VALUES ('5', 'IO0001', 'longchin196', null, '74477211373d00c255b082dea4a966e1', 'vanlongpu@gmail.com', 'Lê', 'Van', 'Long', 'Lê Van Long', '1996-12-28 00:00:00', '\0', '0', null, '2', '', 'Le Van Long', '2020-01-07 11:54:00', 'longchin196', '2020-06-08 00:08:36');
INSERT INTO `User` VALUES ('7', 'IO0002', 'admin', null, '74477211373d00c255b082dea4a966e1', 'tungdohy186@gmail.com', 'Do', 'Van', 'tùng', '  tùng', '1997-07-16 11:53:18', '\0', '0', '', '1', '', 'longchin196', '2020-05-07 16:39:36', 'longchin196', '2020-06-01 07:16:24');
INSERT INTO `User` VALUES ('17', 'IO0005', 'huyenthu', null, '74477211373d00c255b082dea4a966e1', 'huyenthu@gmail.com', 'nguyễn', 'thị', 'huyền', 'nguyễn thị huyền', '1997-11-12 00:00:00', '\0', '0', null, '3', '', 'longchin196', '2020-11-02 19:31:56', 'longchin196', '2020-11-02 19:41:04');

-- ----------------------------
-- Table structure for Video
-- ----------------------------
DROP TABLE IF EXISTS `Video`;
CREATE TABLE `Video` (
  `IDVideo` bigint(20) NOT NULL AUTO_INCREMENT,
  `TieuDe` varchar(255) COLLATE utf8mb4_vietnamese_ci DEFAULT NULL,
  `TieuDe_KhongDau` varchar(255) COLLATE utf8mb4_vietnamese_ci DEFAULT NULL,
  `Anhdaidien` text COLLATE utf8mb4_vietnamese_ci DEFAULT NULL,
  `Embed` text COLLATE utf8mb4_vietnamese_ci DEFAULT NULL,
  `Mota` text COLLATE utf8mb4_vietnamese_ci DEFAULT NULL,
  `SoTien` decimal(11,0) DEFAULT NULL,
  `LuotXem` int(255) DEFAULT NULL,
  `ThinhHanh` bit(1) DEFAULT NULL,
  `DeXuat` bit(1) DEFAULT NULL,
  `TrangThai` bit(1) DEFAULT NULL,
  `isDelete` bit(1) DEFAULT NULL,
  `CreateBy` varchar(150) COLLATE utf8mb4_vietnamese_ci DEFAULT NULL,
  `CreateDate` datetime DEFAULT NULL,
  PRIMARY KEY (`IDVideo`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_vietnamese_ci;

-- ----------------------------
-- Records of Video
-- ----------------------------
INSERT INTO `Video` VALUES ('1', 't1', 'td1', '/Lib/images/interFaceyt.jpg', 'x0W5Y4dX85c', 'm1', '90', '45', '\0', '', '', '', null, '2020-06-01 20:26:03');
INSERT INTO `Video` VALUES ('2', 't2', 't2', '/Lib/images/interFaceyt.jpg', '4VdDspATq44', 'm2', '90', '60', '\0', '', '', '\0', '', '2020-06-01 20:26:03');
INSERT INTO `Video` VALUES ('3', 't2', 't2', '/Lib/images/interFaceyt.jpg', 'Lm3UG2GXLHk', 'm2', '90', '45', '\0', '', '', '', null, '2020-06-02 00:18:45');
INSERT INTO `Video` VALUES ('4', 'long t2', 'long t2', '/Lib/images/interFaceyt.jpg', 'j8U06veqxdU', 'm2', '91', '60', '', '', '', '\0', null, '2020-06-02 00:21:00');
INSERT INTO `Video` VALUES ('5', 'long t3', 'long t3', '/Lib/images/interFaceyt.jpg', '8x2NjwwHUbQ', 'm2', '0', '90', '', '', '', '\0', null, '2020-06-02 00:21:29');
INSERT INTO `Video` VALUES ('6', 't5', 't5', '/Lib/images/interFaceyt.jpg', '886d9rm_AFE', 'm5', '900', '30', '\0', '', '', '\0', null, '2020-06-02 00:24:06');
INSERT INTO `Video` VALUES ('7', 't6', 't6', '/Lib/images/interFaceyt.jpg', 'FCXpxB0UtPg', 'm5', '900', '0', '\0', '', '', '\0', null, '2020-06-02 00:28:07');
INSERT INTO `Video` VALUES ('8', 'tieudetest', 'tieudetest', '/Lib/images/interFaceyt.jpg', 'Wddg6_fK3pY', 'm5', '900', '30', '\0', '', '', '\0', null, '2020-06-02 00:28:50');
INSERT INTO `Video` VALUES ('9', 'Video Test Create', 'video test create', '/Lib/images/interFaceyt.jpg', 'idLTfjxe3fk', 'Video Test Create', '600000', '0', '\0', '', '', '\0', null, '2020-06-14 02:35:22');
INSERT INTO `Video` VALUES ('10', 'Video Test Create 1', 'video test create 1', '/Lib/images/interFaceyt.jpg', 'ckBc_1XnXhA', 'Video Test Create 1', '54535', '0', '\0', '', '', '', null, '2020-06-14 02:42:28');
INSERT INTO `Video` VALUES ('11', 'Video Test Create 2', 'video test create 2', '/Lib/images/interFaceyt.jpg', 'ckBc_1XnXhA', 'Video Test Create 2', '3453453', '0', '\0', '', '', '\0', null, '2020-06-14 02:44:45');
INSERT INTO `Video` VALUES ('12', 'Video Test Create 3444', 'vvv', '/Lib/images/interFaceyt.jpg', 'ckBc_1XnXhA', '?trtghsfhshr5yh', '50000', '30', '', '', '', '\0', null, '2020-06-14 02:48:38');
INSERT INTO `Video` VALUES ('13', 'Anh Thanh Niên Remix', 'anh thanh nien remix', '/Lib/images/interFaceyt.jpg', 'ctjR0i88VU4', null, '50', '0', '\0', '', '', '\0', 'longchin196', '2020-09-07 11:18:54');
INSERT INTO `Video` VALUES ('14', 'Tình Sầu Thiên Thu Muôn Lối Remix ', null, '/Lib/images/interFaceyt.jpg', 'uyqymX3s5MI', null, '25', '240', '', '\0', '', '\0', 'longchin196', '2020-09-07 11:43:37');
INSERT INTO `Video` VALUES ('15', 'CHẠY NGAY ĐI ', 'chay ngay di ', '/Lib/images/interFaceyt.jpg', '32sYGCOYJUM', null, '60', '150', '\0', '\0', '', '\0', 'longchin196', '2020-09-07 11:56:24');
INSERT INTO `Video` VALUES ('16', 'gửi yêu cầu test', null, '/Lib/images/interFaceyt.jpg', 'P3h_HY5QEas', '....', '14', '210', '', '\0', '', '\0', 'longchin', '2020-09-07 15:47:22');
INSERT INTO `Video` VALUES ('17', 'gửi yêu cầu test1', null, '/Lib/images/interFaceyt.jpg', 'Jfhsbo1-Ids', '90', '2', '30', '', '', '', '\0', 'longchin', '2020-09-07 15:54:13');
INSERT INTO `Video` VALUES ('18', 'gửi yêu cầu test 3', null, '/Lib/images/interFaceyt.jpg', 'vzZIPWpQc2E', 'nnn', '70', '120', '\0', '\0', '', '\0', 'longchin', '2020-09-07 15:55:54');
INSERT INTO `Video` VALUES ('19', 'NẮNG ẤM XA DẦN (ONIONN REMIX) | SƠN TÙNG M-TP | Official Music', 'nang am xa dan  onionn remix    son tung m tp   official music', 'https://i.ytimg.com/vi/ErhGuwNgrmw/maxresdefault.jpg', 'ErhGuwNgrmw', 'NẮNG ẤM XA DẦN (ONIONN REMIX) | SƠN TÙNG M-TP | Official Music', '90', '120', '', '', '', '\0', 'longchin196', '2020-09-12 09:32:54');
INSERT INTO `Video` VALUES ('20', 'ĐIỀU GÌ SẼ XẢY RA NẾU LỰC HẤP DẪN BIẾN MẤ', null, '/Lib/images/interFaceyt.jpg', 'ummWMvaJVcI', null, '25', '0', '', null, '', '', 'longchin196', '2020-11-02 20:07:05');

-- ----------------------------
-- Table structure for VideoMoney
-- ----------------------------
DROP TABLE IF EXISTS `VideoMoney`;
CREATE TABLE `VideoMoney` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `IDVideo` bigint(20) DEFAULT NULL,
  `Username` varchar(255) DEFAULT NULL,
  `Seconds` int(11) DEFAULT NULL,
  `Price` decimal(10,2) DEFAULT NULL,
  `isDelete` bit(1) DEFAULT NULL,
  `Create_date` datetime DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `FK_VideoMonney_video` (`IDVideo`),
  CONSTRAINT `FK_VideoMonney_video` FOREIGN KEY (`IDVideo`) REFERENCES `Video` (`IDVideo`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=73 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of VideoMoney
-- ----------------------------
INSERT INTO `VideoMoney` VALUES ('58', '14', 'longchin', '30', '25.00', '\0', '2020-11-01 11:05:44');
INSERT INTO `VideoMoney` VALUES ('59', '16', 'longchin', '30', '14.00', '\0', '2020-11-01 11:10:33');
INSERT INTO `VideoMoney` VALUES ('60', '15', 'longchin', '30', '60.00', '\0', '2020-11-01 11:11:20');
INSERT INTO `VideoMoney` VALUES ('61', '19', 'longchin', '30', '90.00', '\0', '2020-11-01 13:38:20');
INSERT INTO `VideoMoney` VALUES ('62', '14', 'longchin', '30', '25.00', '\0', '2020-11-02 14:31:01');
INSERT INTO `VideoMoney` VALUES ('63', '16', 'longchin', '30', '14.00', '\0', '2020-11-02 14:33:47');
INSERT INTO `VideoMoney` VALUES ('64', '15', 'longchin', '30', '60.00', '\0', '2020-11-02 14:37:59');
INSERT INTO `VideoMoney` VALUES ('65', '18', 'longchin', '30', '70.00', '\0', '2020-11-02 14:45:53');
INSERT INTO `VideoMoney` VALUES ('66', '4', 'longchin', '30', '91.00', '\0', '2020-11-02 14:47:10');
INSERT INTO `VideoMoney` VALUES ('67', '5', 'longchin', '30', '0.00', '\0', '2020-11-02 14:55:25');
INSERT INTO `VideoMoney` VALUES ('68', '12', 'longchin', '30', '50000.00', '\0', '2020-11-02 14:57:01');
INSERT INTO `VideoMoney` VALUES ('69', '8', 'longchin', '30', '900.00', '\0', '2020-11-02 15:00:48');
INSERT INTO `VideoMoney` VALUES ('70', '17', 'longchin', '30', '2.00', '\0', '2020-11-02 15:10:27');
INSERT INTO `VideoMoney` VALUES ('71', '19', 'longchin', '30', '90.00', '\0', '2020-11-02 15:12:42');
INSERT INTO `VideoMoney` VALUES ('72', '2', 'longchin', '30', '90.00', '\0', '2020-11-02 15:14:14');

-- ----------------------------
-- Table structure for Yeucaudoimatkhau
-- ----------------------------
DROP TABLE IF EXISTS `Yeucaudoimatkhau`;
CREATE TABLE `Yeucaudoimatkhau` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `OTP` varchar(255) DEFAULT NULL,
  `Username` varchar(255) DEFAULT NULL,
  `CreateDate` datetime DEFAULT NULL,
  `isActive` bit(1) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of Yeucaudoimatkhau
-- ----------------------------
INSERT INTO `Yeucaudoimatkhau` VALUES ('1', '862D16F1173E4D09A1D7B06561D6BCBD', 'longchin', '2020-09-12 16:03:15', '');
INSERT INTO `Yeucaudoimatkhau` VALUES ('2', '207D959A189A462AADF40B3FFE297896', 'longchin', '2020-09-12 16:03:38', '');
INSERT INTO `Yeucaudoimatkhau` VALUES ('3', '1FF4E63C8BDC4B7CB6CCCFA9D874587B', 'longchin', '2020-09-12 16:05:47', '');
INSERT INTO `Yeucaudoimatkhau` VALUES ('4', '9A0ECEC04D114F17822194A0AE82D47B', 'longchin', '2020-09-12 16:11:02', '');
INSERT INTO `Yeucaudoimatkhau` VALUES ('5', 'B78E1DC39EC041F7B67F7C86A92B2231', 'longchin', '2020-09-12 21:30:11', '\0');
INSERT INTO `Yeucaudoimatkhau` VALUES ('6', 'E7D30276B8C8461F96992D6FE05DB263', 'longchin', '2020-09-12 21:56:45', '');
INSERT INTO `Yeucaudoimatkhau` VALUES ('7', 'B76D7037CCDF4737AB9551D2728C937D', 'longchin', '2020-09-12 21:58:40', '');
INSERT INTO `Yeucaudoimatkhau` VALUES ('8', '19BB0BEA6AFE457F8A2C895CFAEB41FD', 'longchin', '2020-09-12 22:04:09', '');
INSERT INTO `Yeucaudoimatkhau` VALUES ('9', '6AF5B504D9C94652ABA48EA823CB876C', 'longchin', '2020-09-12 22:04:53', '\0');
INSERT INTO `Yeucaudoimatkhau` VALUES ('10', '68EEC59D35AE4348AF53620F664EE4C0', 'longchin', '2020-09-13 09:51:26', '\0');
INSERT INTO `Yeucaudoimatkhau` VALUES ('11', 'BE98AE315B7849BE8011ABA79E62150B', 'longchin', '2020-09-21 23:28:56', '\0');
INSERT INTO `Yeucaudoimatkhau` VALUES ('12', '827916215EE84281A88AFF433E92EB0F', 'longchin', '2020-10-08 22:32:02', '');
INSERT INTO `Yeucaudoimatkhau` VALUES ('13', '2A47FC8A09B24B4F8E1456C410FAB1BE', 'longchin', '2020-10-08 22:35:45', '\0');
INSERT INTO `Yeucaudoimatkhau` VALUES ('14', '3BAC81C13F84429A939A3CDDBED33CE2', 'longchin', '2020-10-11 11:26:52', '\0');
INSERT INTO `Yeucaudoimatkhau` VALUES ('15', '4D8AF4EB19B9424B958D8F0AC93941C9', 'longchin', '2020-10-11 11:29:37', '\0');
INSERT INTO `Yeucaudoimatkhau` VALUES ('16', '6640957A2C1546A4BD80231CD9D28DAF', 'longchin', '2020-10-11 11:48:45', '\0');
INSERT INTO `Yeucaudoimatkhau` VALUES ('17', 'AA030A9523FE4ED98F22F3E15F42441D', 'longchin', '2020-10-11 11:56:54', '');

-- ----------------------------
-- Table structure for YeuCauGiaoDich
-- ----------------------------
DROP TABLE IF EXISTS `YeuCauGiaoDich`;
CREATE TABLE `YeuCauGiaoDich` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `TenNganHang` varchar(255) DEFAULT NULL,
  `SoTaiKhoan` varchar(255) DEFAULT NULL,
  `TenChuThe` varchar(255) DEFAULT NULL,
  `SoTien` decimal(20,0) DEFAULT NULL,
  `idKhachhang` bigint(20) DEFAULT NULL,
  `otp` int(10) DEFAULT NULL,
  `Solangui` int(10) DEFAULT NULL,
  `CreateDate` datetime DEFAULT NULL,
  `isStatus` bit(1) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `FK_YCGD_KH` (`idKhachhang`),
  CONSTRAINT `FK_YCGD_KH` FOREIGN KEY (`idKhachhang`) REFERENCES `KhachHang` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of YeuCauGiaoDich
-- ----------------------------

-- ----------------------------
-- Table structure for YeuCauKhoa
-- ----------------------------
DROP TABLE IF EXISTS `YeuCauKhoa`;
CREATE TABLE `YeuCauKhoa` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `Username` varchar(255) DEFAULT '',
  `LidoGui` int(11) DEFAULT NULL COMMENT '1. mat khau, 2. ma an toan',
  `Solangui` int(11) DEFAULT NULL,
  `Ngaygui` datetime DEFAULT NULL,
  `Trang thai` bit(1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `Fk_YCK_NNK` (`LidoGui`),
  CONSTRAINT `Fk_YCK_NNK` FOREIGN KEY (`LidoGui`) REFERENCES `NguyenNhanKhoa` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=33 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of YeuCauKhoa
-- ----------------------------
INSERT INTO `YeuCauKhoa` VALUES ('31', 'longchin', '5', '5', '2020-10-29 17:08:43', '');
INSERT INTO `YeuCauKhoa` VALUES ('32', 'longchin', '1', '1', '2020-10-31 10:31:48', '\0');

-- ----------------------------
-- Table structure for Yeucaukichhoat
-- ----------------------------
DROP TABLE IF EXISTS `Yeucaukichhoat`;
CREATE TABLE `Yeucaukichhoat` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `idKichhoat` text DEFAULT NULL,
  `idUser` bigint(20) DEFAULT NULL,
  `idLido` int(11) DEFAULT NULL,
  `CreateDate` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_yckh_kh` (`idUser`),
  KEY `Fk_tckh_ttyc` (`idLido`),
  CONSTRAINT `Fk_tckh_ttyc` FOREIGN KEY (`idLido`) REFERENCES `Trangthaiyeucau` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_yckh_kh` FOREIGN KEY (`idUser`) REFERENCES `KhachHang` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of Yeucaukichhoat
-- ----------------------------
INSERT INTO `Yeucaukichhoat` VALUES ('7', '6C6225350756467B820F426DEE12162E', '24', '1', '2020-09-06 15:54:31');
INSERT INTO `Yeucaukichhoat` VALUES ('8', '0C756186CEF04C989A693548570908D7', '25', '1', '2020-09-06 16:12:39');
INSERT INTO `Yeucaukichhoat` VALUES ('9', '972AEFEB8AF1426EA0B462DA1DDAF730', '27', '1', '2020-09-14 11:34:07');
INSERT INTO `Yeucaukichhoat` VALUES ('10', '4380F605D30343D580DBC86A40CEEEB2', '28', '1', '2020-09-21 18:58:31');
INSERT INTO `Yeucaukichhoat` VALUES ('11', '4EA0019F99014A0AA25D8FD383F8F6EE', '28', '1', '2020-09-21 19:02:51');
INSERT INTO `Yeucaukichhoat` VALUES ('12', 'B117919CFB4744C29DB2FB65809F2DB8', '24', '1', '2020-10-10 16:40:47');
INSERT INTO `Yeucaukichhoat` VALUES ('13', 'D8854DAD41284ACF84DA57F13F62ACC1', '24', '1', '2020-10-31 10:29:39');

-- ----------------------------
-- Procedure structure for SP_KHACHHANG_GET
-- ----------------------------
DROP PROCEDURE IF EXISTS `SP_KHACHHANG_GET`;
DELIMITER ;;
CREATE DEFINER=`admin`@`%` PROCEDURE `SP_KHACHHANG_GET`(IN `p_perPage` int,IN `p_startRow` int,IN `p_UserName` varchar(150),IN `p_Role_id` int,IN `p_Email` varchar(150),IN `p_DiaChi` varchar(255),IN `p_SDT` varchar(255),IN `p_GioiTinh` int,IN `p_SoTaiKhoan` varchar(255),IN `p_IDNganHang` bigint,IN `p_SoCMT` varchar(255),IN `p_MaTinhThanh` bigint,IN `p_MaQuanHuyen` bigint,IN `p_MaXaPhuong` bigint,IN `p_IsActive` bit)
BEGIN
	#Routine body goes here...
DECLARE total INT;
		SELECT t1.* FROM `KhachHang` t1 LEFT JOIN Role t2 ON t1.Role_id=t2.id
						WHERE  (p_UserName IS NULL OR t1.UserName LIKE CONCAT('%', COALESCE(p_UserName, ''), '%'))
						AND (p_Role_id IS NULL OR t1.Role_id = p_Role_id)
						AND (p_Email IS NULL OR t1.Email LIKE CONCAT('%', COALESCE(p_Email, ''), '%'))
						AND (p_DiaChi IS NULL OR t1.DiaChi LIKE CONCAT('%', COALESCE(p_DiaChi, ''), '%'))
						AND (p_SDT IS NULL OR t1.SDT LIKE CONCAT('%', COALESCE(p_SDT, ''), '%'))
						AND (p_GioiTinh IS NULL OR t1.GioiTinh = p_GioiTinh)
						AND (p_SoTaiKhoan IS NULL OR t1.SoTaiKhoan LIKE CONCAT('%', COALESCE(p_SoTaiKhoan, ''), '%'))
						AND (p_IDNganHang IS NULL OR t1.IDNganHang = p_IDNganHang)
						AND (p_SoCMT IS NULL OR t1.SoCMT LIKE CONCAT('%', COALESCE(p_SoCMT, ''), '%'))
						AND (p_MaTinhThanh IS NULL OR t1.MaTinhThanh = p_MaTinhThanh)
						AND (p_MaQuanHuyen IS NULL OR t1.MaQuanHuyen = p_MaQuanHuyen)
						AND (p_MaXaPhuong IS NULL OR t1.MaXaPhuong = p_MaXaPhuong)
						AND (p_IsActive IS NULL OR t1.IsActive = p_IsActive)
ORDER BY t1.Create_Date DESC
				 LIMIT p_startRow, p_perPage;
		SET total = (
									SELECT COUNT(t1.id)
									FROM `KhachHang` t1 LEFT JOIN Role t2 ON t1.Role_id=t2.id
										WHERE  (p_UserName IS NULL OR t1.UserName LIKE CONCAT('%', COALESCE(p_UserName, ''), '%'))
											AND (p_Role_id IS NULL OR t1.Role_id = p_Role_id)
											AND (p_Email IS NULL OR t1.Email LIKE CONCAT('%', COALESCE(p_Email, ''), '%'))
											AND (p_DiaChi IS NULL OR t1.DiaChi LIKE CONCAT('%', COALESCE(p_DiaChi, ''), '%'))
											AND (p_SDT IS NULL OR t1.SDT LIKE CONCAT('%', COALESCE(p_SDT, ''), '%'))
											AND (p_GioiTinh IS NULL OR t1.GioiTinh = p_GioiTinh)
											AND (p_SoTaiKhoan IS NULL OR t1.SoTaiKhoan LIKE CONCAT('%', COALESCE(p_SoTaiKhoan, ''), '%'))
											AND (p_IDNganHang IS NULL OR t1.IDNganHang = p_IDNganHang)
											AND (p_SoCMT IS NULL OR t1.SoCMT LIKE CONCAT('%', COALESCE(p_SoCMT, ''), '%'))
											AND (p_MaTinhThanh IS NULL OR t1.MaTinhThanh = p_MaTinhThanh)
											AND (p_MaQuanHuyen IS NULL OR t1.MaQuanHuyen = p_MaQuanHuyen)
											AND (p_MaXaPhuong IS NULL OR t1.MaXaPhuong = p_MaXaPhuong)
											AND (p_IsActive IS NULL OR t1.IsActive = p_IsActive)
									);
						
		SELECT total AS total, ceil(total/p_perPage) AS totalPage;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for SP_NGANHANG_GET
-- ----------------------------
DROP PROCEDURE IF EXISTS `SP_NGANHANG_GET`;
DELIMITER ;;
CREATE DEFINER=`admin`@`%` PROCEDURE `SP_NGANHANG_GET`(IN `p_perPage` int,IN `p_startRow` int,IN `p_TenNganHang` varchar(255),IN `p_Code` varchar(255),IN `p_isActive` bit,IN `p_isDelete` bit)
BEGIN
	#Routine body goes here...
	DECLARE total INT;
	SELECT * from MD_NganHang t1 WHERE 
			 (p_TenNganHang IS NULL OR t1.TenNganHang LIKE CONCAT('%', COALESCE(p_TenNganHang, ''), '%'))
			and (p_Code is null or t1.Code LIKE CONCAT('%',COALESCE(p_Code,''),'%'))
			and (p_isDelete is NULL or t1.isDelete=p_isDelete)
			and (p_isActive is null or t1.isActive=p_isActive)
			ORDER BY t1.CreatedDate DESC 
			LIMIT p_startRow, p_perPage;
	SET total = (
			SELECT COUNT(t1.MaNganHang) from MD_NganHang t1 WHERE (p_TenNganHang IS NULL OR t1.TenNganHang LIKE CONCAT('%', COALESCE(p_TenNganHang, ''), '%'))
						and (p_Code is null or t1.Code LIKE CONCAT('%',COALESCE(p_Code,''),'%'))
						and  (p_isDelete is NULL or t1.isDelete=p_isDelete)
						and (p_isActive is null or t1.isActive=p_isActive)
				);

	SELECT total AS total, ceil(total/p_perPage) AS totalPage;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for SP_SUBSCRIBER_GET_PROPOSER
-- ----------------------------
DROP PROCEDURE IF EXISTS `SP_SUBSCRIBER_GET_PROPOSER`;
DELIMITER ;;
CREATE DEFINER=`boc_maria`@`%` PROCEDURE `SP_SUBSCRIBER_GET_PROPOSER`(IN `p_perPage` int,IN `p_startRow` int,IN `p_channelsToUserID` text)
BEGIN
	#Routine body goes here...
		DECLARE total INT;
			SELECT t1.* from Kenh t1 where t1.ID not in (
			SELECT t4.idKenh from ChannelsToUser t2
			JOIN ChannelsConnect t3 on t2.ChannelsConnectID=t3.ID
			JOIN SubcriberMonney t4 on t4.channelsToUserID =t3.ChannelsID  WHERE t2.id=p_channelsToUserID
			)
		ORDER BY t1.isPrioritize DESC,t1.isPopular desc
				 LIMIT p_startRow, p_perPage;
		SET total = (
									SELECT COUNT(t1.ID)
									FROM Kenh t1 where t1.ID not in (
									SELECT t4.idKenh from ChannelsToUser t2
									JOIN ChannelsConnect t3 on t2.ChannelsConnectID=t3.ID
									JOIN SubcriberMonney t4 on t4.channelsToUserID =t3.ChannelsID  WHERE t2.id=p_channelsToUserID)
									);
						
		SELECT total AS total, ceil(total/p_perPage) AS totalPage;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for SP_SUBSCRIBER_GETID_PROPOSER
-- ----------------------------
DROP PROCEDURE IF EXISTS `SP_SUBSCRIBER_GETID_PROPOSER`;
DELIMITER ;;
CREATE DEFINER=`boc_maria`@`%` PROCEDURE `SP_SUBSCRIBER_GETID_PROPOSER`(IN `p_channelsID` text, IN `p_idkenh`  bigint)
BEGIN
	#Routine body goes here...
			SELECT t1.* from Kenh t1 where t1.ID=p_idkenh AND t1.ID not in (
			SELECT t4.idKenh from ChannelsToUser t2
			JOIN ChannelsConnect t3 on t2.ChannelsConnectID=t3.ID
			JOIN SubcriberMonney t4 on t4.channelsToUserID =t3.ChannelsID  WHERE t2.id=p_channelsID);
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for SP_SUBSCRIBER_GETONE_PROPOSER
-- ----------------------------
DROP PROCEDURE IF EXISTS `SP_SUBSCRIBER_GETONE_PROPOSER`;
DELIMITER ;;
CREATE DEFINER=`boc_maria`@`%` PROCEDURE `SP_SUBSCRIBER_GETONE_PROPOSER`(IN `p_perPage` int,IN `p_startRow` int,IN `p_channelsToUserID` text)
BEGIN
	#Routine body goes here...
		SELECT t1.* from Kenh t1 where t1.ID not in (
			SELECT t4.idKenh from ChannelsToUser t2
			JOIN ChannelsConnect t3 on t2.ChannelsConnectID=t3.ID
			JOIN SubcriberMonney t4 on t4.channelsToUserID =t3.ChannelsID  WHERE t2.id=p_channelsToUserID
			)
		ORDER BY t1.isPrioritize DESC,t1.isPopular desc
				 LIMIT p_startRow, p_perPage;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for SP_USERS_GET
-- ----------------------------
DROP PROCEDURE IF EXISTS `SP_USERS_GET`;
DELIMITER ;;
CREATE DEFINER=`admin`@`%` PROCEDURE `SP_USERS_GET`(IN `p_perPage` int,IN `p_startRow` int,IN `p_active` tinyint,IN `p_full_name` varchar(150),IN `p_email` varchar(150),IN `p_role_id` int)
BEGIN
	#Routine body goes here...
		DECLARE total INT;
		SELECT * FROM `User` t1 LEFT JOIN Role t2 ON t1.role_id=t2.id
		WHERE  (p_full_name IS NULL OR t1.full_name LIKE CONCAT('%', COALESCE(p_full_name, ''), '%'))
					AND (p_email IS NULL OR t1.email LIKE CONCAT('%', COALESCE(p_email, ''), '%'))
					AND (p_role_id IS NULL OR t1.role_id = p_role_id)
					AND (p_active IS NULL OR t1.active = p_active)
		ORDER BY t1.created_date DESC
				 LIMIT p_startRow, p_perPage;
		SET total = (
									SELECT COUNT(t1.id)
									FROM `User` t1 LEFT JOIN Role t2 ON t1.role_id=t2.id
									WHERE  (p_full_name IS NULL OR t1.full_name LIKE CONCAT('%', COALESCE(p_full_name, ''), '%'))
												AND (p_email IS NULL OR t1.email LIKE CONCAT('%', COALESCE(p_email, ''), '%'))
												AND (p_role_id IS NULL OR t1.role_id = p_role_id)
												AND (p_active IS NULL OR t1.active = p_active)
									);
						
		SELECT total AS total, ceil(total/p_perPage) AS totalPage;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for SP_VIDEO_GETIDMONEY
-- ----------------------------
DROP PROCEDURE IF EXISTS `SP_VIDEO_GETIDMONEY`;
DELIMITER ;;
CREATE DEFINER=`boc_maria`@`%` PROCEDURE `SP_VIDEO_GETIDMONEY`(IN `p_idvideo` bigint,IN `p_Username` varchar(255))
BEGIN
	#Routine body goes here...
	select * from Video t1 WHERE t1.IDVideo=p_idvideo  AND t1.IDVideo not in (SELECT t2.IDVideo from VideoMoney t2 where DATE_FORMAT(t2.Create_date, "%M %d %Y")=DATE_FORMAT(CURRENT_DATE, "%M %d %Y") and t2.Username=p_Username)
						AND t1.isDelete=FALSE and t1.TrangThai=TRUE
		ORDER BY t1.DeXuat,t1.ThinhHanh desc
				 LIMIT 0, 1;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for SP_VIDEO_GETNOT_VIDEOMONEY
-- ----------------------------
DROP PROCEDURE IF EXISTS `SP_VIDEO_GETNOT_VIDEOMONEY`;
DELIMITER ;;
CREATE DEFINER=`boc_maria`@`%` PROCEDURE `SP_VIDEO_GETNOT_VIDEOMONEY`(IN `p_perPage` int,IN `p_startRow` int,IN `p_username` varchar(255))
BEGIN
	#Routine body goes here...
		DECLARE total INT;
			select * from Video t1 WHERE t1.IDVideo not in (SELECT t2.IDVideo from VideoMoney t2 where DATE_FORMAT(t2.Create_date, "%M %d %Y")=DATE_FORMAT(CURRENT_DATE, "%M %d %Y") and t2.Username=p_username)
						AND t1.isDelete=FALSE and t1.TrangThai=TRUE
		ORDER BY t1.DeXuat,t1.ThinhHanh desc
				 LIMIT p_startRow, p_perPage;
		SET total = (
									select COUNT(t1.IDVideo) from Video t1 WHERE t1.IDVideo not in (SELECT t2.IDVideo from VideoMoney t2 where DATE_FORMAT(t2.Create_date, "%M %d %Y")=DATE_FORMAT(CURRENT_DATE, "%M %d %Y") and t2.Username=p_username)
										AND t1.isDelete=FALSE and t1.TrangThai=TRUE
									);
						
		SELECT total AS total, ceil(total/p_perPage) AS totalPage;
END
;;
DELIMITER ;
