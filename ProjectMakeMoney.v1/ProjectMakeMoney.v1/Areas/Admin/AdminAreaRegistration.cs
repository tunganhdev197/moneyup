﻿using System.Web.Mvc;

namespace ProjectMakeMoney.v1.Areas.Admin
{
    public class AdminAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "Admin";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
             "logout",
             "logout-user",
             defaults: new { controller = "Login", action = "Logout", id = UrlParameter.Optional },
              namespaces: new[] { "ProjectMakeMoney.v1.Areas.Admin.Controllers" }
            );

            context.MapRoute(
                "Khachhang_TurnOnMakeMoney",
                "Admin/Khachhang/TurnOnMakeMoney/{idKhachhang}",
                defaults: new { controller = "Khachhang", action = "TurnOnMakeMoney", id = UrlParameter.Optional },
                namespaces: new[] { "ProjectMakeMoney.v1.Areas.Admin.Controllers" }
                );

            context.MapRoute(
                "Khachhang_ChkTTKiemtien",
                "Admin/Khachhang/ChkTTKiemtien/{idKhachhang}",
                defaults: new { controller = "Khachhang", action = "ChkTTKiemtien", id = UrlParameter.Optional },
                namespaces: new[] { "ProjectMakeMoney.v1.Areas.Admin.Controllers" }
                );
            context.MapRoute(
              "Khachhang_Edit_PUT",
              "Admin/Khachhang/EditPUT/{idKhachhang}",
              defaults: new { controller = "Khachhang", action = "EditPUT", id = UrlParameter.Optional },
               namespaces: new[] { "ProjectMakeMoney.v1.Areas.Admin.Controllers" }
            );
            context.MapRoute(
               "Khachhang_Edit_API",
               "Admin/Khachhang/{action}/{idKhachhang}",
               defaults: new { controller = "Khachhang", action = "GETID", id = UrlParameter.Optional },
                namespaces: new[] { "ProjectMakeMoney.v1.Areas.Admin.Controllers" }
            );
            context.MapRoute(
               "Kenh_Edit_API",
               "Admin/Kenh/{action}/{idKenh}",
               defaults: new { controller = "Kenh", action = "GETID", id = UrlParameter.Optional },
                namespaces: new[] { "ProjectMakeMoney.v1.Areas.Admin.Controllers" }
            );
            context.MapRoute(
               "Menu_Edit_API",
               "Admin/Menu/{action}/{idMenu}",
               defaults: new { controller = "Menu", action = "GETID", id = UrlParameter.Optional },
                namespaces: new[] { "ProjectMakeMoney.v1.Areas.Admin.Controllers" }
           );

            context.MapRoute(
               "Admin-Home-Index",
               "home-admin",
               defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional },
               namespaces: new[] { "ProjectMakeMoney.v1.Areas.Admin.Controllers" }
           );

            context.MapRoute(
                "Admin_login",
                "login-admin",
                defaults: new { controller = "Login", action = "Index", id = UrlParameter.Optional },
                namespaces: new[] { "ProjectMakeMoney.v1.Areas.Admin.Controllers" }
            );

            context.MapRoute(
                "Admin_default",
                "Admin/{controller}/{action}/{id}",
                defaults: new { action = "Index", id = UrlParameter.Optional },
                 namespaces: new[] { "ProjectMakeMoney.v1.Areas.Admin.Controllers" }
            );
        }
    }
}