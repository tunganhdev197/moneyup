﻿
var frmUser = $("#frmUser");
frmUser.submit(function (e) {
    showPage(true);
    e.preventDefault();
    $.ajax({
        type: 'POST',
        url: frmUser.attr('action'),
        data: frmUser.serialize(),
        success: function (data) {
            if (data.success) {
                notification(data.message);
                window.location = data.action;
            } else { notification(data.message); }
            showPage(false);
        },
        error: function (data) {
            notification("lỗi hệ thống");
        },
    });
});

var frmProduc = $("#frmProduc");
frmProduc.submit(function (e) {
    showPage(true);
    e.preventDefault();
    $.ajax({
        type: 'POST',
        url: frmProduc.attr('action'),
        data: frmProduc.serialize(),
        success: function (data) {
            if (data.success) {
                notification(data.message);            
                window.location = data.action;
            } else { notification(data.message); }
            showPage(false);
        },
        error: function (data) {
            notification("lỗi hệ thống");
        },
    });
});

function Delete(id) {
    if (confirm("bạn có chắc thực hiện yêu cầu này!")) {
        showPage(true);
        $.ajax({
            url: "/Admin/User/Delete/" + id,
            type: "POST",
            success: function (data) {
                notification(data.message);
                if (data.success) {
                    LoadData();
                }
                showPage(false);
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log(jqXHR);
                console.log(textStatus);
                console.log(errorThrown);
                notification("lỗi hệ thống");
            }
        });
    }
}

function LoadRoleSelect() {
    $.ajax({
        url: "/Admin/User/GETRoleID",
        type: "GET",
        success: function (data) {
            if (data.success) {
                var html = '';
                html += '<option value="">--Tất cả--</option>';
                $.each(data.data, function (key, item) {
                    html += '<option value="' + item.id + '">' + item.name + '</option>';
                });
                $("#roleid-search").html(html);

            } else {
                notification(data.message);
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            console.log(jqXHR);
            console.log(textStatus);
            console.log(errorThrown);
            notification("lỗi hệ thống");
        }
    });
}


function LoadProfile() {
    showPage(true);
    $.ajax({
        url: "/Admin/User/GETProfile",
        type: "GET",
        success: function (data) {
            if (data.success) {              
                $('input[type="text"][name=code]').val(data.data.code);
                $('input[type="text"][name=email]').val(data.data.email);
                $('input[type="text"][name=username]').val(data.data.username);
                $('input[type="text"][name=name]').val(data.data.name);
                $('input[type="text"][name=first_name]').val(data.data.first_name);
                $('input[type="text"][name=mid_name]').val(data.data.mid_name);
                $('input[type="text"][name=last_name]').val(data.data.last_name);
                $('input[type="checkbox"][name=active]').prop('checked', data.data.active);
                //$('input[type="date"][name=birth_date]').val(Fomatdate(data.data.birth_date));
                
            } else {
                notification(data.message);
                if(data.action!=null){
                    window.location = data.action;
                }
            }
            showPage(false);
        },
        error: function (jqXHR, textStatus, errorThrown) {
            console.log(jqXHR);
            console.log(textStatus);
            console.log(errorThrown);
            notification("lỗi hệ thống");
        }
    });
}

function LoadInfoUser(id) {
    showPage(true);
    $.ajax({
        url: "/Admin/User/GETID/" + id,
        type: "GET",
        success: function (data) {
            if (data.success) {
                var htm = '';
                htm += '  <div class="widget-user-header bg-yellow">';
                htm += ' <div class="widget-user-image">';
                htm += '      <img class="img-circle" src="/Lib/dist/img/user7-128x128.jpg" alt="User Avatar">';
                htm += '  </div>';
                htm += '    <h3 class="widget-user-username">' + data.data.username + '</h3>';
                htm += '    <h5 class="widget-user-desc">' + data.data.name + '</h5>';
                htm += ' </div>';
                htm += '  <div class="box-footer no-padding">';
                htm += '      <ul class="nav nav-stacked">';
                htm += '          <li><a href="#">Username <span class="pull-right">' + data.data.username + '</span></a></li>';
                htm += '          <li><a href="#">Email <span class="pull-right">' + data.data.email + '</span></a></li>';
                htm += '          <li><a href="#">Họ tên <span class="pull-right">' + data.data.full_name + '</span></a></li>';
                if (data.data.birth_date!=null) {
                    htm += '        <li><a href="#">Ngày sinh <span class="pull-right">' + Fomatdate(data.data.birth_date) + '</span></a></li>';
                }
                htm += '        <li><a href="#">Chức vụ <span class="pull-right">' + data.data.name + '</span></a></li>';
                if (data.data.active) {
                                    htm += '        <li><a href="#">Trạng thái <span class="pull-right">đang hoạt động</span></a></li>';
                } else {
                    htm += '        <li><a href="#">Trạng thái <span class="pull-right">ngừng hoạt động</span></a></li>';
                }
                if (data.data.created_date!=null) {
                    htm += '        <li><a href="#">Ngày tạo <span class="pull-right">' + Fomatdate(data.data.created_date) + '</span></a></li>';
                }
                htm += '        <li><a href="#">Người tạo <span class="pull-right">' + data.data.created_by + '</span></a></li>';
                if (data.data.updated_date) {
                     htm += '        <li><a href="#">Ngày cập nhập <span class="pull-right">' + Fomatdate(data.data.updated_date) + '</span></a></li>';
                }
                htm += '        <li><a href="#">Người cập nhập <span class="pull-right">' + data.data.updated_by + '</span></a></li>';
                htm += '   </ul>';
                htm += ' </div>';
                $("#model-load-user").html(htm);

                $("#modal-user").modal('show');
                showPage(false);
            } else {
                notification(data.message);
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            console.log(jqXHR);
            console.log(textStatus);
            console.log(errorThrown);
            notification("lỗi hệ thống");
        }
    });
}