﻿
function itemVideo(id) {
    showPage(true);
    $.ajax({
        type: 'GET',
        url: '/Admin/Video/GETID/' + id,
        success: function (data) {
            if (data.success) {
                console.log(data.data);
                $('input[type="text"][name=IDVideo]').val(data.data.IDVideo);
                $('input[type="text"][name=TieuDe]').val(data.data.TieuDe);
                $('input[type="text"][name=Anhdaidien]').val(data.data.Anhdaidien);
                $('#Mota').val(data.data.Mota);
                $('input[type="number"][name=SoTien]').val(data.data.SoTien);
                $('input[type="text"][name=DiaChi]').val(data.data.DiaChi);
                $('input[type="text"][name=Embed]').val(data.data.Embed);
                
                $('#ThinhHanh').prop('checked', data.data.ThinhHanh);
                $('#DeXuat').prop('checked', data.data.DeXuat);
                $('#TrangThai').prop('checked', data.data.TrangThai);

                GetListKenh(data.data.ID_kenh);
            } else { notification(data.message); }
            showPage(false);
        },
        error: function (data) {
            notification("lỗi hệ thống");
        },
    });
}

var frmVideo = $("#frmVideo");
frmVideo.submit(function (e) {
    showPage(true);
    e.preventDefault();
    $.ajax({
        type: 'POST',
        url: frmVideo.attr('action'),
        data: frmVideo.serialize(),
        success: function (data) {
            if (data.success) {
                notification(data.message);
                frmVideo[0].reset();
            } else { notification(data.message); }

            showPage(false);
        },
        error: function (data) {
            $.notify("Tạo thông tin thất bại", { position: "top right", className: "error" });
        },
    });
});


var frmVideoUpdate = $("#frmVideoUpdate");
frmVideoUpdate.submit(function (e) {
    showPage(true);
    e.preventDefault();
    $.ajax({
        type: 'PUT',
        url: frmVideoUpdate.attr('action'),
        data: frmVideoUpdate.serialize(),
        success: function (data) {
            if (data.success) {
                notification(data.message);
            } else { notification(data.message); }

            showPage(false);
        },
        error: function (data) {
            $.notify("Tạo thông tin thất bại", { position: "top right", className: "error" });
        },
    });
});


function Delete(id) {
    if (confirm("bạn có chắc xóa bản ghi này!")) {
        showPage(true);
        $.ajax({
            url: "/Admin/Video/DELETE/" + id,
            type: "DELETE",
            success: function (data) {
                if (data.success) {
                    LoadData();
                    notification(data.message);
                } else {
                    notification(data.message);
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log(jqXHR);
                console.log(textStatus);
                console.log(errorThrown);

                notification("Xóa thông tin thất bại");
            }
        });
    }
    showPage(false);
}


function getDetailItem(id) {
    showPage(true);
    $.ajax({
        type: 'GET',
        url: '/Admin/Video/GETID/' + id,
        success: function (data) {
            if (data.success) {
                console.log(data.data);
                $('#TenKenh').text(data.data.TenKenh);
                $('#Mota').text(data.data.Mota);
                $('#TieuDe').text(data.data.TieuDe);
                $('#DiaChi').text(data.data.DiaChi);
                $('#TieuDe_KhongDau').text(data.data.TieuDe_KhongDau);
                $('#CreateBy').text(data.data.CreateBy);
                $('#CreateDate').text(Fomatdate(data.data.CreateDate))
                $('#SoTien').text(formatMoney(data.data.SoTien));
                $('#LuotXem').text(data.data.LuotXem);
                $('#Anhdaidien').attr("src", data.data.Anhdaidien);
                $('#Embed').attr("src", data.data.Embed);

                if (data.data.TrangThai) {
                    $('#TrangThai').html('<td><label class="label label-success">Đang hoạt động</label></td>');
                }
                else {
                    $('#TrangThai').html('<td><label class="label label-danger">Ngừng hoạt động</label></td>');
                }

                if (data.data.ThinhHanh) {
                    $('#ThinhHanh').html('<td><label class="label label-success">Thịnh hành</label></td>');
                }
                else {
                    $('#ThinhHanh').html('<td><label class="label label-danger">Mặc định</label></td>');
                }

                if (data.data.DeXuat) {
                    $('#DeXuat').html('<td><label class="label label-success">Đề xuất</label></td>');
                }
                else {
                    $('#DeXuat').html('<td><label class="label label-danger">Mặc định</label></td>');
                }

            } else { notification(data.message); }
            showPage(false);
        },
        error: function (data) {
            notification("lỗi hệ thống");
        },
    });
}

function GetListKenh(id) {
    $.ajax({
        type: 'GET',
        url: '/Admin/Video/GetListKenh',
        success: function (data) {
            html = '    <option selected="selected" value="">-- chọn kênh --</option>';
            $.each(data.data, function (key, item) {
                html += '<option value="' + item.ID + '">' + item.TenKenh + ' 	&rarr; ' + item.ChannelsID + '</option>';
            });
            $("#ID_kenh-cbo").html(html);
            $('#ID_kenh-cbo').val(id);
            $('#ID_kenh-cbo').select2();

        },
        error: function (data) {
            notification("lỗi hệ thống");
        },
    });
}