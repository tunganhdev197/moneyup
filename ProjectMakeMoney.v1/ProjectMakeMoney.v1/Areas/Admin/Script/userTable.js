﻿var currentPage = 1, lastPage = 1, perPage = 10, total = 100;
var code = '', username = '', email = '', fullname = '', roleid = '', active = '';

/////////////////////////////////////////////////////////////////////////////////
$(function () {
    $("#code-search").on("change", function () {
        code = this.value;
        currentPage = 1;
        LoadData();
    });
    $("#username-search").on("change", function () {
        username = this.value;
        currentPage = 1;
        LoadData();
    });
    $("#email-search").on("change", function () {
        email = this.value;
        currentPage = 1;
        LoadData();
    });
    $("#fullname-search").on("change", function () {
        fullname = this.value;
        currentPage = 1;
        LoadData();
    });
    $("#roleid-search").on("change", function () {
        roleid = this.value;
        currentPage = 1;
        LoadData();
    });
    $("#active-search").on("change", function () {
        active = this.value;
        currentPage = 1;
        LoadData();
    });
});

/////////////////////////////////////////////////////////////////////////////////
$(function () {  
    $('#example-length').on('change', function () {
        perPage = this.value;
        currentPage = 1
        LoadData();
    });
    $("#example_previous").on("click", function () {
        if (total > 0) {
            if (currentPage > 1) {
                currentPage--;

            } else {
                currentPage = lastPage;
            }
        }
        LoadData();
    });
    $("#example_next").on("click", function () {
        if (currentPage * perPage < total) {
            currentPage++;

        } else {
            currentPage = 1
        }
        LoadData();
    });
});
/////////////////////////////////////////////////////////////////////////////////

function LoadData() {
    showPage(true);
    $.ajax({
        url: "/Admin/User/GET?perPage=" + perPage + "&currentPage=" + currentPage + "&code=" + code + "&username=" + username + "&email=" + email + "&fullname=" + fullname + "&roleid=" + roleid + "&active=" + active,
        type: "GET",
        success: function (data) {
            total = data.data.total;
            lastPage = data.data.lastPage;
            currentPage = data.data.currentPage;
            $("#example-curent").html('<a href="#">' + data.data.currentPage + '/' + data.data.lastPage + '</a>');
            $("#example-info").html('Showing ' + data.data.currentPage + ' to ' + data.data.lastPage + ' of ' + data.data.total + ' entries')
            datatable(data.data.apiResult);
            showPage(false);
        },
        error: function (jqXHR, textStatus, errorThrown) {
            console.log(jqXHR);
            console.log(textStatus);
            console.log(errorThrown);
        }
    });
}

function datatable(data) {
    var htm = '';
    htm += '<thead>';
    htm += '    <tr role="row">';
    htm += '        <th class="sorting_asc"style="width: 3%;">ID</th>';
    htm += '        <th class="sorting"style="width: 10%;">Username</th>';
    htm += '        <th class="sorting"style="width: 10%;">Roles</th>';
    htm += '        <th class="sorting"style="width: 15%;">Email</th>';
    htm += '        <th class="sorting"style="width: 10%;">Họ tên</th>';
    htm += '        <th class="sorting"style="width: 7%;">Đăng nhập</th>';
    htm += '        <th class="sorting"style="width: 7%;">Trạng thái</th>';
    htm += '        <th class="sorting"style="width: 10%;">Người cập nhập</th>';
    htm += '        <th class="sorting"style="width: 10%;">Ngày cập nhập</th>';
    htm += '        <th class="sorting"style="width: 8%;"></th>';
    htm += '    </tr>';
    htm += '</thead>';
    htm += '<tbody>';
    $.each(data, function (key, item) {
        htm += '<tr role="row" class="odd">';
        htm += '    <td class="sorting_1">' + item.code + '</td>';
        htm += '    <td>' + item.username + '</td>';
        htm += '    <td>' + item.name + '</td>';
        htm += '    <td>' + item.email + '</td>';
        htm += '    <td>' + item.full_name + '</td>';
        htm += '    <td>' + item.login_count + '</td>';

        if (item.active) {
            htm += '    <td><label class="label label-success"onclick="return Delete(' + item.id + ');">đang hoạt động</label></td>';
        } else {
            htm += '    <td><label class="label label-danger"onclick="return Delete(' + item.id + ');">ngừng hoạt động</label></td>';
        }

        if (item.updated_by != null) {
            htm += '    <td>' + item.updated_by + '</td>';
        } else {
            htm += '    <td>' + item.created_by + '</td>';
        }

        if (item.updated_date != null) {
            htm += '    <td>' + Fomatdate(item.updated_date) + '</td>';
        } else {
            htm += '    <td>' + Fomatdate(item.created_date) + '</td>';
        }

        htm += '    <td>';
        htm += '        <button onclick="return LoadInfoUser(' + item.id + ')" title="info" class="btn-primary btn btn-sm fa fa-info-circle"style="margin-top:2px"></button>';
        htm += '        <a href="/Admin/User/Edit/' + item.id + '" title="info" class="btn-success btn btn-sm fa fa-pencil-square-o"style="margin-top:2px"></a>';
        htm += '    </td>';
        htm += '</tr>';
    });
    htm += '</tbody>';
    $("#example-dataTable").html(htm);
}