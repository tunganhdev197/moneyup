﻿var frmGiaodich = $("#frmGiaodich");
var NodeExchangeDetail = '';

function TrangthaiDuyet(status) {
    if (confirm("bạn có chắc thực hiện yêu cầu này!")) {
        
        $('input[type="hidden"][name=trangthai]').val(status);

        alertify.prompt('_____', 'Ghi chú', ''
                       , function (evt, value) {
                           showPage(true);
                           $('input[type="hidden"][name=Node]').val(value);
                           $.ajax({
                               type: 'POST',
                               url: frmGiaodich.attr('action'),
                               data: frmGiaodich.serialize(),
                               success: function (data) {
                                   if (data.success) {
                                       notification(data.message);
                                       LoadData();
                                   } else { notification(data.message); }
                               },
                               error: function (data) {
                                   notification(data.message);
                               },
                           });
                           showPage(false);
                       }
                       , function () { LoadData(); });
    }
    
}

function TrangthaiDuyet2(id,status) {
    if (confirm("bạn có chắc thực hiện yêu cầu này!")) {
        alertify.prompt('_____', 'Ghi chú', NodeExchangeDetail
               , function (evt, value) {
                   showPage(true);
                   $.ajax({
                       type: 'POST',
                       url: '/Admin/Giaodich/isActive2',
                       data: {
                           Magiaodich: id,
                           trangthai: status,
                           Node: value
                       },
                       success: function (data) {
                           if (data.success) {
                               notification(data.message);
                               getid(idGiaodich);
                           } else { notification(data.message); }
                           showPage(false);
                       },
                       error: function (data) {
                           notification(data.message);
                       },
                   });
               }
               , function () { showPage(false); });
        
        
    }
    
}

function TrangthaiDuyet3(id) {
    if (confirm("bạn có chắc thực hiện yêu cầu này!")) {
        showPage(true);
        $.ajax({
            type: 'POST',
            url: '/Admin/Giaodich/isActive3',
            data: {
                Magiaodich: id,
            },
            success: function (data) {
                if (data.success) {
                    notification(data.message);
                    getid(id);
                    console.log(1);
                } else { notification(data.message); console.log(0); }
            },
            error: function (data) {
                notification(data.message);
            },
        });
    }
    showPage(false);
}

function getid(id) {
    showPage(true);
    $.ajax({
        type: 'GET',
        url: '/Admin/Giaodich/GetID/' + id,
        success: function (data) {
            if (data.success) {
                NodeExchangeDetail = data.data.GhiChu;
                var htm = '';
                htm += '<div class="col-md-6">';
                if (data.data.TrangThaiDuyet == 3) {
                    htm += '     <p class="lead">Thông Tin Giao Dịch <label class="label label-success">đã duyệt</label></p>';
                }
                if (data.data.TrangThaiDuyet == 2) {
                    htm += '     <p class="lead">Thông Tin Giao Dịch <label class="label label-primary">chờ duyệt</label></p>';
                }
                if (data.data.TrangThaiDuyet == 1) {
                    htm += '     <p class="lead">Thông Tin Giao Dịch <label class="label label-warning">chờ xác nhận</label></p>';
                }
                if (data.data.TrangThaiDuyet == 0) {
                    htm += '     <p class="lead">Thông Tin Giao Dịch <label class="label label-danger">hủy duyệt</label></p>';
                }
                if (data.data.TrangThaiDuyet == 4) {
                    htm += '     <p class="lead">Thông Tin Giao Dịch <label class="label label-default">đã chuyển tiền</label></p>';
                }
                htm += '      <div class="table-responsive">';
                htm += '           <table class="table">';
                htm += '              <tr>';
                htm += '                  <th style="width:50%">Mã giao dịch:</th>';
                htm += '                  <td>' + data.data.MaGiaoDich + '</td>';
                htm += '              </tr>';
                if (data.data.NgayTao!=null) {
                    htm += '               <tr>';
                    htm += '                   <th>Ngày tạo:</th>';
                    htm += '                   <td>' + Fomatdate(data.data.NgayTao) + '</td>';
                    htm += '               </tr>';
                }

                htm += '              <tr>';
                htm += '                  <th>Ngân hàng:</th>';
                htm += '                 <td>' + data.data.NganHang + '</td>';
                htm += '               </tr>';
                htm += '              <tr>';
                htm += '                 <th>Số tài khoản:</th>';
                htm += '                 <td>' + data.data.Sotaikhoan + '</td>';
                htm += '         </tr>';
                htm += '         <tr>';
                htm += '             <th>Chủ tài khoản:</th>';
                htm += '            <td>' + data.data.Tentaikhoan + '</td>';
                htm += '         </tr>';
                htm += '         <tr>';
                htm += '             <th>Số tiền:</th>';
                htm += '            <td>' + formatMoney(data.data.SoTien) + '</td>';
                htm += '         </tr>';
                htm += '         <tr>';
                htm += '             <th>Số dư sau rút:</th>';
                htm += '             <td>' + formatMoney(data.data.SoDuSauRut) + '</td>';
                htm += '         </tr>';
                htm += '         <tr>';
                htm += '             <th>Ghi chú:</th>';
                htm += '             <td>' + data.data.GhiChu + '</td>';
                htm += '         </tr>';
                htm += '     </table>';
                htm += ' </div>';
                htm += '</div>';
                htm += '  <div class="col-md-6">';
              
                htm += '<div class="col-md-6">';
                htm += ' <p class="lead">Thông Tin Người Dùng</p>';
                htm += ' <div class="table-responsive">';
                htm += '     <table class="table">';
                htm += '         <tr>';
                htm += '             <th style="width:50%">Mã khách hàng:</th>';
                htm += '              <td>' + data.data.code + '</td>';
                htm += '         </tr>';
                htm += '         <tr>';
                htm += '             <th>Họ tên:</th>';
                htm += '              <td>' + data.data.full_name_kh + '</td>';
                htm += '         </tr>';
                htm += '         <tr>';
                htm += '             <th>Tên đăng nhập</th>';
                htm += '             <td>' + data.data.UserName + '</td>';
                htm += '         </tr>';
                htm += '         <tr>';
                htm += '             <th>Email đăng kí:</th>';
                htm += '             <td>' + data.data.Email + '</td>';
                htm += '         </tr>';
                htm += '         <tr>';
                htm += '             <th>Địa chỉ:</th>';
                htm += '              <td>' + data.data.DiaChi + '</td>';
                htm += '         </tr>';
                htm += '         <tr>';
                htm += '             <th>Số điện thoại:</th>';
                htm += '             <td>' + data.data.SDT + '</td>';
                htm += '         </tr>';
                htm += '         <tr>';
                htm += '             <th>Chứng minh thư:</th>';
                htm += '            <td>' + data.data.SoCMT + '</td>';
                htm += '         </tr>';
                if (data.data.Create_Date != null) {
                    htm += '         <tr>';
                    htm += '             <th>Ngày tạo:</th>';
                    htm += '             <td>' + Fomatdate(data.data.Create_Date) + '</td>';
                    htm += '         </tr>';
                }

                htm += '         <tr>';
                htm += '             <th>Số dư:</th>';
                htm += '             <td>' + formatMoney(data.data.SoDu) + '</td>';
                htm += '         <tr>';
                htm += '             <th>Trạng thái:</th>';
                if (data.data.IsActive) {
                    htm += '             <td><label class="label label-success">Hoạt động</label></td>';
                }
                else {
                    htm += '             <td><label class="label label-danger">Ngừng hoạt động</label></td>';
                }
                htm += '         </tr>';
                htm += '         <tr>';
                htm += '             <th>Kiếm tiền:</th>';
                if (data.data.TTKiemTien) {
                    htm += '             <td><label class="label label-success">Đã bật</label></td>';
                }
                else {
                    htm += '             <td><label class="label label-danger">Chưa bật</label></td>';
                }
                htm += '         </tr>';
                htm += '         <tr>';
                htm += '             <th>Xác thực:</th>';
                if (data.data.TTXacThucTK) {
                    htm += '             <td><label class="label label-success">Đã bật</label></td>';
                }
                else {
                    htm += '             <td><label class="label label-danger">Chưa bật</label></td>';
                }
                htm += '         </tr>';
                htm += '     </table>';
                htm += ' </div>';
                htm += '</div>';

                $('#infoGiaodich').html(htm);
            } else { notification(data.message); }

            showPage(false);
        },
        error: function (data) {
            notification("lỗi hệ thống");
        },
    });
}