﻿

var frmMenu = $("#frmMenu");
frmMenu.submit(function (e) {
    showPage(true);
    e.preventDefault();
    $.ajax({
        type: 'POST',
        url: frmMenu.attr('action'),
        data: frmMenu.serialize(),
        success: function (data) {
            notification(data.message);

            showPage(false);
        },
        error: function (data) {
            $.notify("Tạo thông tin thất bại", { position: "top right", className: "error" });
        },
    });
});

var frmMenuUpdate = $("#frmMenuUpdate");
frmMenuUpdate.submit(function (e) {
    showPage(true);
    e.preventDefault();
    $.ajax({
        type: 'PUT',
        url: frmMenuUpdate.attr('action'),
        data: frmMenuUpdate.serialize(),
        success: function (data) {
            notification(data.message);

            showPage(false);
        },
        error: function (data) {
            $.notify("Tạo thông tin thất bại", { position: "top right", className: "error" });
        },
    });
});

function getid(id) {
    showPage(true);
    $.ajax({
        type: 'GET',
        url: '/Admin/Menu/GETID/' + id,
        success: function (data) {
            if (data.success) {
                $('input[type="text"][name=TenMenu]').val(data.data.TenMenu);
                $('input[type="text"][name=ThuTu]').val(data.data.ThuTu);
                $('textarea[name=MoTa]').val(data.data.MoTa);
                $('input[type="checkbox"][name=TrangThai]').prop('checked', data.data.TrangThai);


            } else { notification(data.message); }

            showPage(false);
        },
        error: function (data) {
            notification("lỗi hệ thống");
        },
    });
}


function Delete(id) {
    if (confirm("bạn có chắc xóa bản ghi này!")) {
        showPage(true);
        $.ajax({
            url: "/Admin/Menu/DELETE/" + id,
            type: "DELETE",
            success: function (data) {
                notification(data.message);
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log(jqXHR);
                console.log(textStatus);
                console.log(errorThrown);

                notification("Xóa thông tin thất bại");
            }
        });
    }
    showPage(false);
}

function clearControl() {
    $('input[type="text"][name=TenMenu]').val('');
    $('input[type="text"][name=ThuTu]').val('');
    $('textarea[name=MoTa]').val('');
    $('input[type="checkbox"][name=TrangThai]').prop('checked', true);
}