﻿
var html = '';
var currentPage = 1, lastPage = 1, perPage = 10, total = 100, TenMenu = '', trangthai = true;

function formatDate(date) {
    var hours = date.getHours();
    var minutes = date.getMinutes();
    var ampm = hours >= 12 ? 'pm' : 'am';
    hours = hours % 12;
    hours = hours ? hours : 12; // the hour '0' should be '12'
    minutes = minutes < 10 ? '0' + minutes : minutes;
    var strTime = hours + ':' + minutes + ' ' + ampm;
    return (date.getMonth() + 1) + "/" + date.getDate() + "/" + date.getFullYear() + "  " + strTime;
}

function Fomatdate(createDate) {
    var resDate = createDate.substring(6, createDate.length - 2);
    var d = new Date(parseInt(resDate));
    var e = formatDate(d);
    return e;
}

$(function () {
    LoadData();
    $('#example-category-length').on('change', function () {
        perPage = this.value;
        currentPage = 1
        LoadData();
    });
    $("#TenMenu-search").on("keyup", function () {
        TenMenu = this.value;
        currentPage = 1;
        LoadData();
    });
    $("#trangthai-search").on("change", function () {
        trangthai = this.value;
        currentPage = 1;
        LoadData();
    });
    $("#example_previous").on("click", function () {
        if (total > 0) {
            if (currentPage > 1) {
                currentPage--;

            } else {
                currentPage = lastPage;
            }
        }
        LoadData();
    });
    $("#example_next").on("click", function () {
        if (currentPage * perPage < total) {
            currentPage++;

        } else {
            currentPage = 1
        }
        LoadData();
    });
});

function LoadData() {
    $.ajax({
        url: '/Admin/MD_NganHang/GET?perPage=' + perPage + '&currentPage=' + currentPage + "&TenNganHang=" + TenMenu + "&isActive=" + trangthai,
        type: 'GET',
        success: function (data) {
            if (data.success) {
                total = data.data.total;
                lastPage = data.data.lastPage;
                currentPage = data.data.currentPage;
                $("#example-curent").html('<a href="#">' + data.data.currentPage + '/' + data.data.lastPage + '</a>');
                $("#example-categry-info").html('Showing ' + data.data.currentPage + ' to ' + data.data.lastPage + ' of ' + data.data.total + ' entries')
                datatable(data.data.apiResult);
                $.notify(data.message, "success");
            } else {
                $.notify(data.message, { position: "top right", className: "error" });
            }
        },
        error: function (data) {
            $.notify("Tạo thông tin thất bại", { position: "top right", className: "error" });
        },
    });
}

function detailItem(id) {
    showPage(true);
    $.ajax({
        type: 'GET',
        url: '/Admin/MD_NganHang/GETID/' + id,
        success: function (data) {
            if (data.success) {
                $('input[type="text"][name=TenNganHang]').val(data.data.TenNganHang);
                $('input[type="checkbox"][name=isActive]').prop('checked', data.data.isActive);
            } else { notification(data.message); }
            showPage(false);
        },
        error: function (data) {
            notification("lỗi hệ thống");
        },
    });
}



function IsActive(id) {
    if (confirm("bạn có chắc thay đổi bản ghi này!")) {
        $.ajax({
            url: "/Admin/MD_NganHang/IsActive/" + id,
            type: "PUT",
            success: function (data) {
                if (data.success) {
                    LoadData();              
                }
                notification(data.message);
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log(jqXHR);
                console.log(textStatus);
                console.log(errorThrown);
                $.notify("Xóa thông tin thất bại", { position: "top right", className: "error" });
            }
        });
    }
}

function Delete(id) {
    if (confirm("bạn có chắc xóa bản ghi này!")) {
        $.ajax({
            url: "/Admin/MD_NganHang/DELETE/" + id,
            type: "DELETE",
            success: function (data) {
                if (data.success) {
                    LoadData();                
                }
                notification(data.message);
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log(jqXHR);
                console.log(textStatus);
                console.log(errorThrown);
                $.notify("Xóa thông tin thất bại", { position: "top right", className: "error" });
            }
        });
    }
}


function datatable(data) {
    var htm = '';
    htm += '<thead>';
    htm += '    <tr role="row">';
    htm += '        <th class="sorting_asc"style="width: 10%;">Mã ngân hàng</th>';
    htm += '        <th class="sorting"style="width: 30%;">Tên ngân hàng</th>';
    htm += '        <th class="sorting"style="width: 20%;">Người tạo</th>';
    htm += '        <th class="sorting"style="width: 15%;">Ngày tạo</th>';
    htm += '        <th class="sorting"style="width: 15%;">Trạng thái</th>';
    htm += '        <th class="sorting"style="width: 10%;"></th>';
    htm += '    </tr>';
    htm += '</thead>';
    htm += '<tbody>';
    $.each(data, function (key, item) {
        htm += '<tr role="row" class="odd">';
        htm += '    <td class="sorting_1">' + item.MaNganHang + '</td>';
        htm += '    <td>' + item.TenNganHang + '</td>';
        htm += '    <td>' + item.Create_By + '</td>';
        htm += '    <td>' + Fomatdate(item.CreatedDate) + '</td>';
        if (item.isActive == true) {
            htm += '<td><label class="label label-success" onclick="return IsActive('+item.MaNganHang+')">đang kích hoạt</label></td>'
        }
        else {
            htm += '<td><label class="label label-danger" onclick="return IsActive(' + item.MaNganHang + ')">chưa kích hoạt</label></td>'
        }
        htm += '    <td>';
        htm += '        <button onclick="return Delete(' + item.MaNganHang + ');" title="delete" class="btn-dange btn btn-sm fa fa-trash-o"style="margin-top:2px"></button>';
        htm += '    </td>';
        htm += '</tr>';
    });
    htm += '</tbody>';
    $("#example-category").html(htm);
}