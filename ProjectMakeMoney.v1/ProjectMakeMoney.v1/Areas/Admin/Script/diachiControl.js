﻿function Delete(id) {
    if (confirm("bạn có chắc xóa bản ghi này!")) {
        showPage(true);
        if (infoAction == 1) {
            $.ajax({
                url: "/Admin/MD_PhuongXa/Delete/" + id,
                type: "DELETE",
                success: function (data) {
                    if (data.success) {
                        LoadData();
                        notification(data.message);
                    } else {
                        notification(data.message);
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    console.log(jqXHR);
                    console.log(textStatus);
                    console.log(errorThrown);

                    notification("Xóa thông tin thất bại");
                }
            });
        }
        if (infoAction == 2) {
            $.ajax({
                url: "/Admin/MD_QuanHuyen/Delete/" + id,
                type: "DELETE",
                success: function (data) {
                    if (data.success) {
                        LoadData();
                        notification(data.message);
                    } else {
                        notification(data.message);
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    console.log(jqXHR);
                    console.log(textStatus);
                    console.log(errorThrown);

                    notification("Xóa thông tin thất bại");
                }
            });
        }
        if (infoAction == 3) {
            $.ajax({
                url: "/Admin/MD_TinhThanh/Delete/" + id,
                type: "DELETE",
                success: function (data) {
                    if (data.success) {
                        LoadData();
                        notification(data.message);
                    } else {
                        notification(data.message);
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    console.log(jqXHR);
                    console.log(textStatus);
                    console.log(errorThrown);

                    notification("Xóa thông tin thất bại");
                }
            });
        }
        showPage(true);
    }
}