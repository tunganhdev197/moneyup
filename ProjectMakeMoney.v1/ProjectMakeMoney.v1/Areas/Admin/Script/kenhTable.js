﻿var currentPage = 1, lastPage = 1, perPage = 10, total = 100, tenkenh = '', EmailKenh = '', sdt = '', isactive = '', isPopular = '', isPrioritize='';

/////////////////////////////////////////////////////////////////////////////////
$(function () {
    $("#tenkenh-search").on("change", function () {
        tenkenh = this.value;
        currentPage = 1;
        LoadData();
    });
    $("#EmailKenh-search").on("change", function () {
        EmailKenh = this.value;
        currentPage = 1;
        LoadData();
    });
    $("#sdt-search").on("change", function () {
        sdt = this.value;
        currentPage = 1;
        LoadData();
    });
    $("#isactive-search").on("change", function () {
        isactive = this.value;
        currentPage = 1;
        LoadData();
    });
    $("#isPopular-search").on("change", function () {
        isPopular = this.value;
        currentPage = 1;
        LoadData();
    });
    $("#isPrioritize-search").on("change", function () {
        isPrioritize = this.value;
        currentPage = 1;
        LoadData();
    });
});

/////////////////////////////////////////////////////////////////////////////////
$(function () {
    LoadData();
    $('#example-length').on('change', function () {
        perPage = this.value;
        currentPage = 1
        LoadData();
    });
    $("#example_previous").on("click", function () {
        if (total > 0) {
            if (currentPage > 1) {
                currentPage--;

            } else {
                currentPage = lastPage;
            }
        }
        LoadData();
    });
    $("#example_next").on("click", function () {
        if (currentPage * perPage < total) {
            currentPage++;

        } else {
            currentPage = 1
        }
        LoadData();
    });
});
/////////////////////////////////////////////////////////////////////////////////

function LoadData() {
    showPage(true);
    $.ajax({
        url: "/Admin/Kenh/GET?perPage=" + perPage + "&currentPage=" + currentPage + "&tenkenh=" + tenkenh + "&sdt=" + sdt + "&EmailKenh=" + EmailKenh + "&isPopular=" + isPopular + "&isPrioritize=" + isPrioritize + "&isactive=" + isactive,
        type: "GET",
        success: function (data) {
            total = data.data.total;
            lastPage = data.data.lastPage;
            currentPage = data.data.currentPage;
            $("#example-curent").html('<a href="#">' + data.data.currentPage + '/' + data.data.lastPage + '</a>');
            $("#example-info").html('Showing ' + data.data.currentPage + ' to ' + data.data.lastPage + ' of ' + data.data.total + ' entries')
            datatable(data.data.apiResult);
            showPage(false);
        },
        error: function (jqXHR, textStatus, errorThrown) {
            console.log(jqXHR);
            console.log(textStatus);
            console.log(errorThrown);
        }
    });
}

function datatable(data) {
    var htm = '';
    htm += '<thead>';
    htm += '    <tr role="row">';
    htm += '        <th class="sorting_asc"style="width: 3%;">ID</th>';
    htm += '        <th class="sorting"style="width: 15%;">Tên kênh</th>';
    htm += '        <th class="sorting"style="width: 15%;">ID kênh</th>';
    htm += '        <th class="sorting"style="width: 20%;">Email kênh</th>';
    htm += '        <th class="sorting"style="width: 15%;">Số lượt sub</th>';
    htm += '        <th class="sorting"style="width: 15%;">Giá tiền</th>';
    htm += '        <th class="sorting"style="width: 15%;">Phổ biến</th>';
    htm += '        <th class="sorting"style="width: 15%;">Ưu tiên</th>';
    htm += '        <th class="sorting"style="width: 30%;">TrangThai</th>';
    htm += '        <th class="sorting"style="width: 8%;"></th>';
    htm += '    </tr>';
    htm += '</thead>';
    htm += '<tbody>';
    $.each(data, function (key, item) {
        htm += '<tr role="row" class="odd">';
        htm += '    <td class="sorting_1">' + item.ID + '</td>';
        htm += '    <td>' + item.TenKenh + '</td>';
        htm += '    <td>' + item.ChannelsID + '</td>';
        htm += '    <td>' + item.EmailKenh + '</td>';
        htm += '    <td>' + formatMoney(item.SoluotSub) + '</td>';
        htm += '    <td>' + formatMoney(item.GiatienSub) + '</td>';
        if (item.isPopular) {
            htm += '    <td><label class="label label-success">Phổ biến</label></td>';
        } else {
            htm += '    <td><label class="label label-default">Mặc định</label></td>';
        }
        if (item.isPrioritize) {
            htm += '    <td><label class="label label-success">Ưu tiên</label></td>';
        } else {
            htm += '    <td><label class="label label-default">Mặc định</label></td>';
        }
        if (item.isActive) {
            htm += '    <td><label class="label label-success">đang hoạt động</label></td>';
        } else {
            htm += '    <td><label class="label label-danger">ngừng hoạt động</label></td>';
        }

        htm += '    <td>';
        htm += '        <a href="/Admin/Kenh/Detail/' + item.ID + '" title="info" class="btn-primary btn btn-sm fa fa-info-circle"style="margin-top:2px"></a>';
        htm += '        <a href="/Admin/Kenh/Edit/' + item.ID + '" title="edit" class="edit btn-warning btn btn-sm fa fa-pencil"style="margin-top:2px"></a>';
        htm += '        <button onclick="return Delete(' + item.ID + ');" title="delete" class="delete btn-dange btn btn-sm fa fa-trash-o"style="margin-top:2px"></button>';
        htm += '    </td>';
        htm += '</tr>';
    });
    htm += '</tbody>';
    $("#example-dataTable").html(htm);
}