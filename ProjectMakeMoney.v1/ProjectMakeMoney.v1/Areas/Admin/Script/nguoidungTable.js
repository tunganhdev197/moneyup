﻿var currentPage = 1, lastPage = 1, perPage = 10, total = 100;
var UserName = '', Email = '', IsActive = '', TTKiemTien = '', TTXacThucTK='';

/////////////////////////////////////////////////////////////////////////////////
$(function () {
    $("#UserName-search").on("change", function () {
        UserName = this.value;
        currentPage = 1;
        LoadData();
    });
    $("#Email-search").on("change", function () {
        Email = this.value;
        currentPage = 1;
        LoadData();
    });
    $("#IsActive-search").on("change", function () {
        IsActive = this.value;
        currentPage = 1;
        LoadData();
    });
    $("#TTKiemTien-search").on("change", function () {
        TTKiemTien = this.value;
        currentPage = 1;
        LoadData();
    });
    $("#TTXacThucTK-search").on("change", function () {
        TTXacThucTK = this.value;
        currentPage = 1;
        LoadData();
    });
});

/////////////////////////////////////////////////////////////////////////////////
$(function () {
    LoadData();
    $('#example-length').on('change', function () {
        perPage = this.value;
        currentPage = 1
        LoadData();
    });
    $("#example_previous").on("click", function () {
        if (total > 0) {
            if (currentPage > 1) {
                currentPage--;

            } else {
                currentPage = lastPage;
            }
        }
        LoadData();
    });
    $("#example_next").on("click", function () {
        if (currentPage * perPage < total) {
            currentPage++;

        } else {
            currentPage = 1
        }
        LoadData();
    });
});
/////////////////////////////////////////////////////////////////////////////////

function LoadData() {
    showPage(true);
    $.ajax({
        url: "/Admin/Khachhang/GET?perPage=" + perPage + "&currentPage=" + currentPage + "&UserName=" + UserName + "&Email=" + Email + "&IsActive=" + IsActive + "&TTKiemTien=" + TTKiemTien + "&TTXacThucTK=" + TTXacThucTK,
        type: "GET",
        success: function (data) {
            total = data.data.total;
            lastPage = data.data.lastPage;
            currentPage = data.data.currentPage;
            $("#example-curent").html('<a href="#">' + data.data.currentPage + '/' + data.data.lastPage + '</a>');
            $("#example-info").html('Showing ' + data.data.currentPage + ' to ' + data.data.lastPage + ' of ' + data.data.total + ' entries')
            datatable(data.data.apiResult);
            showPage(false);
        },
        error: function (jqXHR, textStatus, errorThrown) {
            console.log(jqXHR);
            console.log(textStatus);
            console.log(errorThrown);
        }
    });
}

function datatable(data) {
    var htm = '';
    htm += '<thead>';
    htm += '    <tr role="row">';
    htm += '        <th class="sorting_asc"style="width: 3%;">Code</th>';
    htm += '        <th class="sorting"style="width: 10%;">Username</th>';
    htm += '        <th class="sorting"style="width: 10%;">Email</th>';
    htm += '        <th class="sorting"style="width: 7%;">Bật Kiếm tiền</th>';
    htm += '        <th class="sorting"style="width: 7%;">Xác thực</th>';
    htm += '        <th class="sorting"style="width: 7%;">Hoạt động</th>';
    htm += '        <th class="sorting"style="width: 8%;"></th>';
    htm += '    </tr>';
    htm += '</thead>';
    htm += '<tbody>';
    $.each(data, function (key, item) {
        htm += '<tr role="row" class="odd">';
        htm += '    <td class="sorting_1">' + item.code + '</td>';
        htm += '    <td>' + item.UserName + '</td>';
        htm += '    <td>' + item.Email + '</td>';

        if (item.TTKiemTien) {
            htm += '    <td><label class="label label-success"onclick="return Delete(' + item.ID + ');">Đã bật kiếm tiền</label></td>';
        } else {
            htm += '    <td><label class="label label-danger"onclick="return Delete(' + item.ID + ');">Chưa bật kiếm tiền</label></td>';
        }

        if (item.TTXacThucTK) {
            htm += '    <td><label class="label label-success"onclick="return Delete(' + item.ID + ');">Đã xác thực</label></td>';
        } else {
            htm += '    <td><label class="label label-danger"onclick="return Delete(' + item.ID + ');">Chưa xác thực</label></td>';
        }
        if (item.IsActive) {
            htm += '    <td><label class="label label-success"onclick="return Delete(' + item.ID + ');">Hoạt động</label></td>';
        } else {
            htm += '    <td><label class="label label-danger"onclick="return Delete(' + item.ID + ');">Ngưng hoạt động</label></td>';
        }

        htm += '    <td>';
        htm += '        <button onclick="return LoadInfoUser(' + item.ID + ')" title="info" class="btn-primary btn btn-sm fa fa-info-circle"style="margin-top:2px"></button>';
        htm += '        <a href="/Admin/Khachhang/Edit/' + item.ID + '" title="info" class="btn-success btn btn-sm fa fa-pencil-square-o"style="margin-top:2px"></a>';
        htm += '    </td>';
        htm += '</tr>';
    });
    htm += '</tbody>';
    $("#example-dataTable").html(htm);
}