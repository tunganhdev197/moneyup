﻿var currentPage = 1, lastPage = 1, perPage = 10, total = 100, TenMenu = '', trangthai = true;

/////////////////////////////////////////////////////////////////////////////////
$(function () {
    $("#TenMenu-search").on("change", function () {
        TenMenu = this.value;
        currentPage = 1;
        LoadData();
    });
    $("#trangthai-search").on("change", function () {
        trangthai = this.value;
        currentPage = 1;
        LoadData();
    });
});

/////////////////////////////////////////////////////////////////////////////////
$(function () {
    LoadData();
    $('#example-length').on('change', function () {
        perPage = this.value;
        currentPage = 1
        LoadData();
    });
    $("#example_previous").on("click", function () {
        if (total > 0) {
            if (currentPage > 1) {
                currentPage--;

            } else {
                currentPage = lastPage;
            }
        }
        LoadData();
    });
    $("#example_next").on("click", function () {
        if (currentPage * perPage < total) {
            currentPage++;

        } else {
            currentPage = 1
        }
        LoadData();
    });
});
/////////////////////////////////////////////////////////////////////////////////

function LoadData() {
    showPage(true);
    $.ajax({
        url: "/Admin/Menu/GET?perPage=" + perPage + "&currentPage=" + currentPage + "&TenMenu=" + TenMenu + "&trangthai=" + trangthai,
        type: "GET",
        success: function (data) {
            total = data.data.total;
            lastPage = data.data.lastPage;
            currentPage = data.data.currentPage;
            $("#example-curent").html('<a href="#">' + data.data.currentPage + '/' + data.data.lastPage + '</a>');
            $("#example-info").html('Showing ' + data.data.currentPage + ' to ' + data.data.lastPage + ' of ' + data.data.total + ' entries')
            datatable(data.data.apiResult);
            showPage(false);
        },
        error: function (jqXHR, textStatus, errorThrown) {
            console.log(jqXHR);
            console.log(textStatus);
            console.log(errorThrown);
        }
    });
}

function datatable(data) {
    var htm = '';
    htm += '<thead>';
    htm += '    <tr role="row">';
    htm += '        <th class="sorting_asc"style="width: 3%;">ID</th>';
    htm += '        <th class="sorting"style="width: 15%;">Tên</th>';
    htm += '        <th class="sorting"style="width: 20%;">ThuTu</th>';
    htm += '        <th class="sorting"style="width: 15%;">MoTa</th>';
    htm += '        <th class="sorting"style="width: 30%;">TrangThai</th>';
    htm += '        <th class="sorting"style="width: 8%;"></th>';
    htm += '    </tr>';
    htm += '</thead>';
    htm += '<tbody>';
    $.each(data, function (key, item) {
        htm += '<tr role="row" class="odd">';
        htm += '    <td class="sorting_1">' + item.ID + '</td>';
        htm += '    <td>' + item.TenMenu + '</td>';
        htm += '    <td>' + item.ThuTu + '</td>';
        htm += '    <td>' + item.MoTa + '</td>';
        if (item.TrangThai) {
            htm += '    <td><label class="label label-success">đang hoạt động</label></td>';
        } else {
            htm += '    <td><label class="label label-danger">ngừng hoạt động</label></td>';
        }
        htm += '    <td>';
        htm += '        <a href="/Admin/Menu/Edit/' + item.ID + '" title="edit" class="edit btn-warning btn btn-sm fa fa-pencil"style="margin-top:2px"></a>';
        htm += '        <button onclick="return Delete(' + item.ID + ');" title="delete" class="delete btn-dange btn btn-sm fa fa-trash-o"style="margin-top:2px"></button>';
        htm += '    </td>';
        htm += '</tr>';
    });
    htm += '</tbody>';
    $("#example-dataTable").html(htm);
}