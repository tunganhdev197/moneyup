﻿
var frmKenh = $("#frmKenh");
frmKenh.submit(function (e) {
    showPage(true);
    e.preventDefault();
    $.ajax({
        type: 'POST',
        url: frmKenh.attr('action'),
        data: frmKenh.serialize(),
        success: function (data) {
            if (data.success) {
                notification(data.message);
                frmKenh[0].reset();
            } else { notification(data.message); }

            showPage(false);
        },
        error: function (data) {
            $.notify("Tạo thông tin thất bại", { position: "top right", className: "error" });
        },
    });
});

function detailItemKenh(id) {
    showPage(true);
    $.ajax({
        type: 'GET',
        url: '/Admin/Kenh/GETID/' + id,
        success: function (data) {
            if (data.success) {
                console.log(data.data);
                $('input[type="text"][name=ID]').val(data.data.ID);
                $('input[type="text"][name=ChannelsID]').val(data.data.ChannelsID);
                $('input[type="text"][name=EmbedDemo]').val(data.data.EmbedDemo);
                $('input[type="text"][name=TenKenh]').val(data.data.TenKenh);
                $('input[type="text"][name=EmailKenh]').val(data.data.EmailKenh);
                $('input[type="text"][name=PassKenh]').val(data.data.PassKenh);
                $('input[type="text"][name=Sdt]').val(data.data.Sdt);
                $('input[type="number"][name=GiatienSub]').val(data.data.GiatienSub);
                $('#isPopular').prop('checked', data.data.isPopular);
                $('#isPrioritize').prop('checked', data.data.isPrioritize);
                $('#isActive').prop('checked', data.data.isActive);
            } else { notification(data.message); }
            showPage(false);
        },
        error: function (data) {
            notification("lỗi hệ thống");
        },
    });
}


var frmKenhUpdate = $("#frmKenhUpdate");
frmKenhUpdate.submit(function (e) {
    showPage(true);
    e.preventDefault();
    $.ajax({
        type: 'PUT',
        url: frmKenhUpdate.attr('action'),
        data: frmKenhUpdate.serialize(),
        success: function (data) {
            notification(data.message);
            showPage(false);
        },
        error: function (data) {
            $.notify("Tạo thông tin thất bại", { position: "top right", className: "error" });
        },
    });
});


function Delete(id) {
    if (confirm("bạn có chắc xóa bản ghi này!")) {
        showPage(true);
        $.ajax({
            url: "/Admin/Kenh/DELETE/" + id,
            type: "DELETE",
            success: function (data) {
                if (data.success) {
                    LoadData();
                    notification(data.message);
                } else {
                    notification(data.message);
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log(jqXHR);
                console.log(textStatus);
                console.log(errorThrown);

                notification("Xóa thông tin thất bại");
            }
        });
    }
    showPage(false);
}

function getDetailItem(id) {
    showPage(true);
    $.ajax({
        type: 'GET',
        url: '/Admin/Kenh/GETID/' + id,
        success: function (data) {
            if (data.success) {
                $('#TenKenh').text(data.data.TenKenh);
                $('#ChannelsID').text(data.data.ChannelsID);
                $('#EmailKenh').text(data.data.EmailKenh);
                $('#PassKenh').text(data.data.PassKenh);
                $('#Create_by').text(data.data.Create_by);
                $('#Create_Date').text(Fomatdate(data.data.Create_Date))
                $('#GiatienSub').text(formatMoney(data.data.GiatienSub));
                $('#SoluotSub').text(data.data.SoluotSub);

                if (data.data.isActive) {
                    $('#isActive').html('<td><label class="label label-success">Hoạt động</label></td>');
                }
                else {
                    $('#isActive').html('<td><label class="label label-danger">Ngừng hoạt động</label></td>');
                }

                if (data.data.isPopular) {
                    $('#isPopular').html('<td><label class="label label-success">Phổ biến</label></td>');
                }
                else {
                    $('#isPopular').html('<td><label class="label label-danger">Mặc định</label></td>');
                }

                if (data.data.isPrioritize) {
                    $('#isPrioritize').html('<td><label class="label label-success">Ưu tiên</label></td>');
                }
                else {
                    $('#isPrioritize').html('<td><label class="label label-danger">Mặc định</label></td>');
                }

            } else { notification(data.message); }
            showPage(false);
        },
        error: function (data) {
            notification("lỗi hệ thống");
        },
    });
}