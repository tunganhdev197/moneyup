﻿var currentPage = 1, lastPage = 1, perPage = 10, total = 100, magiaodich = null, makhachhang = null, manhanvienduyet = null, formdate = null, todate = null, trangthaiduyet = null;

/////////////////////////////////////////////////////////////////////////////////
$(function () {
    $("#magiaodich-search").on("change", function () {
        magiaodich = this.value;
        currentPage = 1;
        LoadData();
    });

    $("#makhachhang-search").on("change", function () {
        makhachhang = this.value;
        currentPage = 1;
        LoadData();
    });


    $("#manhanvienduyet-search").on("change", function () {
        manhanvienduyet = this.value;
        currentPage = 1;
        LoadData();
    });

    $("#formdate-search").on("change", function () {
        formdate = this.value;
        currentPage = 1;
        LoadData();
    });

    $("#todate-search").on("change", function () {
        todate = this.value;
        currentPage = 1;
        LoadData();
    });

    $("#trangthaiduyet-search").on("change", function () {
        trangthaiduyet = this.value;
        currentPage = 1;
        LoadData();
    });
});

/////////////////////////////////////////////////////////////////////////////////
$(function () {
    LoadData();
    $('#example-length').on('change', function () {
        perPage = this.value;
        currentPage = 1
        LoadData();
    });
    $("#example_previous").on("click", function () {
        if (total > 0) {
            if (currentPage > 1) {
                currentPage--;

            } else {
                currentPage = lastPage;
            }
        }
        LoadData();
    });
    $("#example_next").on("click", function () {
        if (currentPage * perPage < total) {
            currentPage++;

        } else {
            currentPage = 1
        }
        LoadData();
    });
});
/////////////////////////////////////////////////////////////////////////////////

function LoadData() {
    showPage(true);
    $.ajax({
        url: "/Admin/Giaodich/Get?perPage=" + perPage + "&currentPage=" + currentPage + "&magiaodich=" + magiaodich + "&makhachhang=" + makhachhang + "&manhanvienduyet=" + manhanvienduyet + "&formdate=" + formdate + "&todate=" + todate + "&trangthaiduyet=" + trangthaiduyet,
        type: "GET",
        success: function (data) {
            total = data.data.total;
            lastPage = data.data.lastPage;
            currentPage = data.data.currentPage;
            $("#example-curent").html('<a href="#">' + data.data.currentPage + '/' + data.data.lastPage + '</a>');
            $("#example-info").html('Showing ' + data.data.currentPage + ' to ' + data.data.lastPage + ' of ' + data.data.total + ' entries')
            datatable(data.data.apiResult);
            showPage(false);
        },
        error: function (jqXHR, textStatus, errorThrown) {
            console.log(jqXHR);
            console.log(textStatus);
            console.log(errorThrown);
        }
    });
}

function datatable(data) {
    var htm = '';
    htm += '<thead>';
    htm += '    <tr role="row">';
    htm += '        <th style="width: 4%;">Chọn</th>';
    htm += '        <th class="sorting_asc"style="width: 8%;">Mã giao dịch</th>';
    htm += '        <th class="sorting"style="width: 10%;">Trạng thái</th>';
    htm += '        <th class="sorting"style="width: 10%;">Mã khách hàng</th>';
    htm += '        <th class="sorting"style="width: 10%;">Nhân viên duyệt</th>';
    htm += '        <th class="sorting"style="width: 15%;">Ngân hàng</th>';
    htm += '        <th class="sorting"style="width: 10%;">Số tiền</th>';
    htm += '        <th class="sorting"style="width: 10%;">Số dư sau rút</th>';
    htm += '        <th class="sorting"style="width: 10%;">Ngày tạo</th>';
    htm += '        <th class="sorting"style="width: 20%;">Ghi chú</th>';
    htm += '        <th class="sorting"style="width: 8%;"></th>';
    htm += '    </tr>';
    htm += '</thead>';
    htm += '<tbody>';
    $.each(data, function (key, item) {
        htm += '<tr role="row" class="odd">';
        htm += '    <td class="sorting_1"><input type="checkbox" name="listGiaodich" value="' + item.MaGiaoDich + '"> <input type="hidden" name="trangthai"><input type="hidden" name="Node"></td>';
        htm += '    <td class="sorting_1">' + item.MaGiaoDich + '</td>';
        if (item.TrangThaiDuyet == 3) {
            htm += '    <td><label class="label label-success">đã duyệt</label></td>';
        }
        if (item.TrangThaiDuyet == 2) {
            htm += '    <td><label class="label label-primary">chờ duyệt</label></td>';
        }
        if (item.TrangThaiDuyet == 1) {
            htm += '    <td><label class="label label-warning">chờ xác nhận</label></td>';
        }
        if (item.TrangThaiDuyet == 0) {
            htm += '    <td><label class="label label-danger">hủy duyệt</label></td>';
        }
        if (item.TrangThaiDuyet == 4) {
            htm += '    <td><label class="label label-default">đã chuyển tiền</label></td>';
        }
        htm += '    <td>' + item.MaKhachHang + '(' + item.full_name + ')</td>';
        htm += '    <td>' + item.NhanVienDuyet + '</td>';
        htm += '    <td>' + item.NganHang + '</td>';
        htm += '    <td>' + item.SoTien + '</td>';
        htm += '    <td>' + item.SoDuSauRut + '</td>';
        htm += '    <td>' + Fomatdate(item.NgayTao) + '</td>';
        htm += '    <td>' + item.GhiChu + '</td>';
        htm += '    <td>';
        htm += '        <a href="/Admin/Giaodich/Info/' + item.MaGiaoDich + '" title="info" class="btn-primary btn btn-sm fa fa-info-circle"style="margin-top:2px"></a>';
        htm += '    </td>';
        htm += '</tr>';
    });
    htm += '</tbody>';
    $("#example-dataTable").html(htm);
}