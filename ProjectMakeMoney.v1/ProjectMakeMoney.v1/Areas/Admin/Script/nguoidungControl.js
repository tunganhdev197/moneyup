﻿var frmkh = $("#frmkh");
frmkh.submit(function (e) {
    showPage(true);
    e.preventDefault();
    $.ajax({
        type: 'POST',
        url: frmkh.attr('action'),
        data: frmkh.serialize(),
        success: function (data) {
            if (data.success) {
                notification(data.message);
                window.location = data.action;
            } else { notification(data.message); }
            showPage(false);
        },
        error: function (data) {
            notification("lỗi hệ thống");
        },
    });
});

var frmkhEdit = $("#frmkhEdit");
frmkhEdit.submit(function (e) {
    showPage(true);
    e.preventDefault();
    $.ajax({
        type: 'POST',
        url: frmkhEdit.attr('action'),
        data: frmkhEdit.serialize(),
        success: function (data) {
            notification(data.message);
            showPage(false);
        },
        error: function (data) {
            notification("lỗi hệ thống");
        },
    });
});

var frmkhbkt = $("#frmkhbkt");
frmkhbkt.submit(function (e) {
    showPage(true);
    e.preventDefault();
    $.ajax({
        type: 'POST',
        url: frmkhbkt.attr('action'),
        data: frmkhbkt.serialize(),
        success: function (data) {
            if (data.success)
            {
                Getbtn(data.data);
                frmkhbkt[0].reset();
            }
            $('#modal-batkiemtien').modal('hide');
            notification(data.message);
            showPage(false);
            
        },
        error: function (data) {
            notification("lỗi hệ thống");
        },
    });
});



function Getbtn(idkh) {
    $.ajax({
        url: "/Admin/Khachhang/ChkTTKiemtien/" + idkh,
        type: "GET",
        success: function (data) {
            var htm = '';
            if (data.data) {
                htm += '<button type="button" class="btn btn-default" onclick="return clearControl()">Làm mới</button>';
                htm += '<button type="submit" class="btn btn-info pull-right" id="btnupdate">Cập nhập</button>';
            } else {
                htm += '<button type="button" class="btn btn-default" onclick="return clearControl()">Làm mới</button>';
                htm += '<button type="button" class="btn btn-info pull-right" style="margin-left: 10px;" data-toggle="modal" data-target="#modal-batkiemtien">Bật kiếm tiền</button>';
                htm += '<button type="submit" class="btn btn-info pull-right" id="btnupdate">Cập nhập</button>';
            }

            $('#footerID').html(htm);
        },
        error: function (jqXHR, textStatus, errorThrown) {
            console.log(jqXHR);
            console.log(textStatus);
            console.log(errorThrown);
        }
    });
}

function GetIDkhachhang(id) {
    showPage(true);
    $.ajax({
        type: 'GET',
        url: '/Admin/Khachhang/GETID?idKhachhang=' + id,
        success: function (data) {
            if (data.success) {
                $('input[type="text"][name=UserName]').val(data.data.UserName);
                $('input[type="text"][name=Email]').val(data.data.Email);
                if (data.data.NgaySinh != null)
                    $('input[type="date"][name=NgaySinh]').val(Fomatdateyymmdd(data.data.NgaySinh));
                $('input[type="text"][name=SDT]').val(data.data.SDT);
                $("input[name=GioiTinh][value=" + data.data.GioiTinh + "]").prop('checked', true);
                $('input[type="text"][name=first_name]').val(data.data.first_name);
                $('input[type="text"][name=mid_name]').val(data.data.mid_name);
                $('input[type="text"][name=last_name]').val(data.data.last_name);
                $('input[type="number"][name=MaAnToan]').val(data.data.MaAnToan);
                $('input[type="text"][name=MaGioiThieu]').val(data.data.MaGioiThieu);
                $('input[type="text"][name=SoCMT]').val(data.data.SoCMT);
                $('input[type="checkbox"][name=TTKiemTien]').prop('checked', data.data.TTKiemTien);
                $('input[type="checkbox"][name=TTXacThucTK]').prop('checked', data.data.TTXacThucTK);
                $('input[type="checkbox"][name=TTXacThucCmt]').prop('checked', data.data.TTXacThucCmt);
                $('input[type="checkbox"][name=IsActive]').prop('checked', data.data.IsActive);
            } else { notification(data.message); }

            showPage(false);
        },
        error: function (data) {
            notification("lỗi hệ thống");
        },
    });
}