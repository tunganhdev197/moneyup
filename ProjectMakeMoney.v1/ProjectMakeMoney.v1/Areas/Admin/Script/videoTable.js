﻿var currentPage = 1, lastPage = 1, perPage = 10, total = 100, tieude = '', thinhhanh = '', dexuat = '', trangthai = '';

/////////////////////////////////////////////////////////////////////////////////
$(function () {
    $("#tieude-search").on("change", function () {
        tieude = this.value;
        currentPage = 1;
        LoadData();
    });
    $("#thinhhanh-search").on("change", function () {
        thinhhanh = this.value;
        currentPage = 1;
        LoadData();
    });
    $("#dexuat-search").on("change", function () {
        dexuat = this.value;
        currentPage = 1;
        LoadData();
    });
    $("#trangthai-search").on("change", function () {
        trangthai = this.value;
        currentPage = 1;
        LoadData();
    });
});

/////////////////////////////////////////////////////////////////////////////////
$(function () {
    LoadData();
    $('#example-length').on('change', function () {
        perPage = this.value;
        currentPage = 1
        LoadData();
    });
    $("#example_previous").on("click", function () {
        if (total > 0) {
            if (currentPage > 1) {
                currentPage--;

            } else {
                currentPage = lastPage;
            }
        }
        LoadData();
    });
    $("#example_next").on("click", function () {
        if (currentPage * perPage < total) {
            currentPage++;

        } else {
            currentPage = 1
        }
        LoadData();
    });
});
/////////////////////////////////////////////////////////////////////////////////

function LoadData() {
    //showPage(true);
    $.ajax({
        url: "/Admin/Video/GET?perPage=" + perPage + "&currentPage=" + currentPage + "&tieude=" + tieude + "&idkenh=" + idkenh + "&thinhhanh=" + thinhhanh + "&dexuat=" + dexuat + "&trangthai=" + trangthai,
        type: "GET",
        success: function (data) {
            total = data.data.total;
            lastPage = data.data.lastPage;
            currentPage = data.data.currentPage;
            $("#example-curent").html('<a href="#">' + data.data.currentPage + '/' + data.data.lastPage + '</a>');
            $("#example-info").html('Showing ' + data.data.currentPage + ' to ' + data.data.lastPage + ' of ' + data.data.total + ' entries')
            datatable(data.data.apiResult);
            showPage(false);
        },
        error: function (jqXHR, textStatus, errorThrown) {
            console.log(jqXHR);
            console.log(textStatus);
            console.log(errorThrown);
        }
    });
}

function datatable(data) {
    var htm = '';
    htm += '<thead>';
    htm += '    <tr role="row">';
    htm += '        <th class="sorting_asc"style="width: 3%;">ID</th>';
    htm += '        <th class="sorting"style="width: 8%;">Ảnh</th>';
    htm += '        <th class="sorting"style="width: 15%;">Tiêu đề</th>';
    htm += '        <th class="sorting"style="width: 8%;">Tên kênh</th>';
    htm += '        <th class="sorting"style="width: 8%;">Lượt xem</th>';
    htm += '        <th class="sorting"style="width: 10%;">Số tiền</th>';
    htm += '        <th class="sorting"style="width: 8%;">Thịnh hành</th>';
    htm += '        <th class="sorting"style="width: 8%;">Đề xuất</th>';
    htm += '        <th class="sorting"style="width: 8%;">Trạng thái</th>';
    htm += '        <th class="sorting"style="width: 10%;"></th>';
    htm += '    </tr>';
    htm += '</thead>';
    htm += '<tbody>';
    $.each(data, function (key, item) {
        htm += '<tr role="row" class="odd">';
        htm += '    <td class="sorting_1">' + item.IDVideo + '</td>';
        htm += '    <td><img src="' + item.Anhdaidien + '" class="img-thumbnail"></td>';
        htm += '    <td>' + item.TieuDe + '</td>';
        htm += '    <td><a href="/Admin/Kenh/Detail/' + item.ID_kenh + '"  target="_blank" title="info" class="label label-default">' + item.TenKenh + '</a></td>';
        htm += '    <td>' + formatMoney(item.LuotXem) + '</td>';
        htm += '    <td>' + formatMoney(item.SoTien) + '</td>';
        if (item.ThinhHanh) {
            htm += '    <td><label class="label label-success">Thịnh hành</label></td>';
        } else {
            htm += '    <td><label class="label label-default">Mặc định</label></td>';
        }
        if (item.DeXuat) {
            htm += '    <td><label class="label label-success">Đề xuất</label></td>';
        } else {
            htm += '    <td><label class="label label-default">Mặc định</label></td>';
        }
        if (item.TrangThai) {
            htm += '    <td><label class="label label-success">đang hoạt động</label></td>';
        } else {
            htm += '    <td><label class="label label-danger">ngừng hoạt động</label></td>';
        }

        htm += '    <td>';
        htm += '         <a href="/Admin/Video/Detail/' + item.IDVideo + '" title="info" class="btn-primary btn btn-sm fa fa-info-circle"style="margin-top:2px"></a>';
        htm += '        <a href="/Admin/Video/Edit/' + item.IDVideo + '" title="edit" class="edit btn-warning btn btn-sm fa fa-pencil"style="margin-top:2px"></a>';
        htm += '        <button onclick="return Delete(' + item.IDVideo + ');" title="delete" class="btn-danger btn btn-sm fa fa-trash-o"style="margin-top:2px"></button>';
        htm += '    </td>';
        htm += '</tr>';
    });
    htm += '</tbody>';
    $("#example-dataTable").html(htm);
}