﻿///lưu các hàm dùng chung cho cả dự án
var active = null;
function showPage(active) {
    if (active == true) {
        document.getElementById("loader").style.display = "inline";
    } else {
        document.getElementById("loader").style.display = "none";
    }
}

function titleControlModel(title) {
    $("#modal-create").modal('show');
    $("#modal-title").text(title);
    $("#btnupdate").text(title);
}

function notification(notifi) {
    if (notifi != null&&notifi!="") {
        $("#alert-body").html(notifi);
        $("#modal-alert").modal("show");
    }
}

function formatDate(date) {
    var hours = date.getHours();
    var minutes = date.getMinutes();
    var ampm = hours >= 12 ? 'pm' : 'am';
    hours = hours % 12;
    hours = hours ? hours : 12; // the hour '0' should be '12'
    minutes = minutes < 10 ? '0' + minutes : minutes;
    var strTime = hours + ':' + minutes + ' ' + ampm;
    return (date.getMonth() + 1) + "/" + date.getDate() + "/" + date.getFullYear() + "  " + strTime;
}

function Fomatdate(createDate) {
    var resDate = createDate.substring(6, createDate.length - 2);
    var d = new Date(parseInt(resDate));
    var e = formatDate(d);
    return e;
}

function formatDateyymmdd(date) {
    var hours = date.getHours();
    var minutes = date.getMinutes();
    var ampm = hours >= 12 ? 'pm' : 'am';
    hours = hours % 12;
    hours = hours ? hours : 00; // the hour '0' should be '12'
    minutes = minutes < 10 ? '0' + minutes : minutes;
    var strTime = hours + ':' + minutes + ' ' + ampm;
    return date.getFullYear() + "-" + ("0" + (date.getMonth() + 1)).slice(-2) + "-" + ("0" + date.getDate()).slice(-2);
}

function Fomatdateyymmdd(createDate) {
    var resDate = createDate.substring(6, createDate.length - 2);
    var d = new Date(parseInt(resDate));
    var e = formatDateyymmdd(d);
    return e;
}

//fomat số tiền
function formatMoney(amount) {
    var decimalCount = 0,
    decimal = ".",
    thousands = ",";
    try {
        decimalCount = Math.abs(decimalCount);
        decimalCount = isNaN(decimalCount) ? 2 : decimalCount;

        const negativeSign = amount < 0 ? "-" : "";

        let i = parseInt(amount = Math.abs(Number(amount) || 0).toFixed(decimalCount)).toString();
        let j = (i.length > 3) ? i.length % 3 : 0;

        return negativeSign + (j ? i.substr(0, j) + thousands : '') + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + thousands) + (decimalCount ? decimal + Math.abs(amount - i).toFixed(decimalCount).slice(2) : "");
    } catch (e) {
        console.log(e)
    }
};

//lấy param url
function GetURLParameter(sParam) {
    var sPageURL = window.location.search.substring(1);
    var sURLVariables = sPageURL.split('&');
    for (var i = 0; i < sURLVariables.length; i++) {
        var sParameterName = sURLVariables[i].split('=');
        if (sParameterName[0] == sParam) {
            return sParameterName[1];
        }
    }
}

$(function () {
    showPage(false);
});