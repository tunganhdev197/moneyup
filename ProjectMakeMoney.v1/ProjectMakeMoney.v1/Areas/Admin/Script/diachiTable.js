﻿var currentPage = 1, lastPage = 1, perPage = 10, infoAction = 1, total = 100, TenPhuongXa = '', tenquanhuyen = '', tentinhthanh = '', trangthai = '';

/////////////////////////////////////////////////////////////////////////////////
$(function () {
    $("#TenPhuongXa-search").on("change", function () {
        TenPhuongXa = this.value;
        currentPage = 1;
        LoadData();
    });
    $("#diachi-search").on("change", function () {
        infoAction = this.value;
        currentPage = 1;
        LoadData();
    });
    $("#tenquanhuyen-search").on("change", function () {
        tenquanhuyen = this.value;
        currentPage = 1;
        LoadData();

    });
    $("#tentinhthanh-search").on("change", function () {
        tentinhthanh = this.value;
        currentPage = 1;
        LoadData();
    });
    $("#trangthai-search").on("change", function () {
        trangthai = this.value;
        currentPage = 1;
        LoadData();
    });
});

/////////////////////////////////////////////////////////////////////////////////
$(function () {
    LoadData();
    $('#example-length').on('change', function () {
        perPage = this.value;
        currentPage = 1
        LoadData();
    });
    $("#example_previous").on("click", function () {
        if (total > 0) {
            if (currentPage > 1) {
                currentPage--;

            } else {
                currentPage = lastPage;
            }
        }
        LoadData();
    });
    $("#example_next").on("click", function () {
        if (currentPage * perPage < total) {
            currentPage++;

        } else {
            currentPage = 1
        }
        LoadData();
    });
});
/////////////////////////////////////////////////////////////////////////////////

function LoadData() {
    showPage(true);
    var url = '';
    if (infoAction == 1) {
        url = "/Admin/MD_PhuongXa/GET?perPage=" + perPage + "&currentPage=" + currentPage + "&TenPhuongXa=" + TenPhuongXa + "&tenquanhuyen=" + tenquanhuyen + "&tentinhthanh=" + tentinhthanh + "&TrangThai=" + trangthai;
    }
    if (infoAction == 2) {
        url = "/Admin/MD_QuanHuyen/GET?perPage=" + perPage + "&currentPage=" + currentPage + "&tenquanhuyen=" + tenquanhuyen + "&tentinhthanh=" + tentinhthanh + "&TrangThai=" + trangthai;
    }
    if (infoAction == 3) {
        url = "/Admin/MD_TinhThanh/GET?perPage=" + perPage + "&currentPage=" + currentPage + "&tentinhthanh=" + tentinhthanh + "&TrangThai=" + trangthai;
    }
    $.ajax({
        url: url,
        type: "GET",
        success: function (data) {
            total = data.data.total;
            lastPage = data.data.lastPage;
            currentPage = data.data.currentPage;
            $("#example-curent").html('<a href="#">' + data.data.currentPage + '/' + data.data.lastPage + '</a>');
            $("#example-info").html('Showing ' + data.data.currentPage + ' to ' + data.data.lastPage + ' of ' + data.data.total + ' entries')
            if (infoAction == 1) {
                datatable(data.data.apiResult);
            }
            if (infoAction == 2) {
                datatableQuanHuyen(data.data.apiResult);
            }
            if (infoAction == 3) {
                datatableTinhthanh(data.data.apiResult);
            }

            showPage(false);
        },
        error: function (jqXHR, textStatus, errorThrown) {
            console.log(jqXHR);
            console.log(textStatus);
            console.log(errorThrown);
        }
    });
}


function datatable(data) {
    var htm = '';
    htm += '<thead>';
    htm += '    <tr role="row">';
    htm += '        <th class="sorting_asc"style="width: 3%;">ID</th>';
    htm += '        <th class="sorting"style="width: 30%;">Tên</th>';
    htm += '        <th class="sorting"style="width: 30%;">Ghi chú</th>';
    htm += '        <th class="sorting"style="width: 10%;">TrangThai</th>';
    htm += '        <th class="sorting"style="width: 8%;"></th>';
    htm += '    </tr>';
    htm += '</thead>';
    htm += '<tbody>';
    $.each(data, function (key, item) {
        htm += '<tr role="row" class="odd">';
        htm += '    <td class="sorting_1">' + item.MaPhuongXa + '</td>';
        htm += '    <td>' + item.TenPhuongXa + ' / ' + item.TenQuanHuyen + ' / ' + item.TenTinhThanh + '</td>';
        htm += '    <td>' + item.GhiChu + '</td>';
        if (item.TrangThai) {
            htm += '    <td><label class="label label-success">đang hoạt động</label></td>';
        } else {
            htm += '    <td><label class="label label-danger">ngừng hoạt động</label></td>';
        }

        htm += '    <td>';
        htm += '        <button onclick="return Delete(' + item.MaPhuongXa + ');" title="delete" class="delete btn-dange btn btn-sm fa fa-trash-o"style="margin-top:2px"></button>';

        htm += '    </td>';
        htm += '</tr>';
    });
    htm += '</tbody>';
    $("#example-dataTable").html(htm);
}


function datatableQuanHuyen(data) {
    var htm = '';
    htm += '<thead>';
    htm += '    <tr role="row">';
    htm += '        <th class="sorting_asc"style="width: 3%;">ID</th>';
    htm += '        <th class="sorting"style="width: 30%;">Tên</th>';
    htm += '        <th class="sorting"style="width: 30%;">Ghi chú</th>';
    htm += '        <th class="sorting"style="width: 10%;">TrangThai</th>';
    htm += '        <th class="sorting"style="width: 8%;"></th>';
    htm += '    </tr>';
    htm += '</thead>';
    htm += '<tbody>';
    $.each(data, function (key, item) {
        htm += '<tr role="row" class="odd">';
        htm += '    <td class="sorting_1">' + item.MaQuanHuyen + '</td>';
        htm += '    <td>' + item.TenQuanHuyen + ' / ' + item.TenTinhThanh + '</td>';
        htm += '    <td>' + item.GhiChu + '</td>';
        if (item.TrangThai) {
            htm += '    <td><label class="label label-success">đang hoạt động</label></td>';
        } else {
            htm += '    <td><label class="label label-danger">ngừng hoạt động</label></td>';
        }

        htm += '    <td>';
        htm += '        <button onclick="return Delete(' + item.MaQuanHuyen + ');" title="delete" class="delete btn-dange btn btn-sm fa fa-trash-o"style="margin-top:2px"></button>';
        htm += '    </td>';
        htm += '</tr>';
    });
    htm += '</tbody>';
    $("#example-dataTable").html(htm);
}


function datatableTinhthanh(data) {
    var htm = '';
    htm += '<thead>';
    htm += '    <tr role="row">';
    htm += '        <th class="sorting_asc"style="width: 3%;">ID</th>';
    htm += '        <th class="sorting"style="width: 30%;">Tên</th>';
    htm += '        <th class="sorting"style="width: 30%;">Ghi chú</th>';
    htm += '        <th class="sorting"style="width: 10%;">TrangThai</th>';
    htm += '        <th class="sorting"style="width: 8%;"></th>';
    htm += '    </tr>';
    htm += '</thead>';
    htm += '<tbody>';
    $.each(data, function (key, item) {
        htm += '<tr role="row" class="odd">';
        htm += '    <td class="sorting_1">' + item.MaTinhThanh + '</td>';
        htm += '    <td>' + item.TenTinhThanh + '</td>';
        htm += '    <td>' + item.GhiChu + '</td>';
        if (item.TrangThai) {
            htm += '    <td><label class="label label-success">đang hoạt động</label></td>';
        } else {
            htm += '    <td><label class="label label-danger">ngừng hoạt động</label></td>';
        }

        htm += '    <td>';
        htm += '        <button onclick="return Delete(' + item.MaTinhThanh + ');" title="delete" class="delete btn-dange btn btn-sm fa fa-trash-o"style="margin-top:2px"></button>';

        htm += '    </td>';
        htm += '</tr>';
    });
    htm += '</tbody>';
    $("#example-dataTable").html(htm);
}