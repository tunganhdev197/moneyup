﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using System.Data.Entity;
using System.Text.RegularExpressions;
using Library.YTB.BO.Models;
using Library.YTB.Utils;

namespace ProjectMakeMoney.v1.Areas.Admin.Controllers
{
    [Authorize(Roles = "TL0013")]
    public class SanphamController : Controller
    {
        // GET: Admin/Sanpham
        private Library.YTB.DA.Sanpham.DA_Sanpham da_sanpham = null;
        private Library.YTB.Utils.ApiResult apiResult = null;
        public ActionResult Create()
        {
            return View();
        }

        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public ActionResult Detail(int? id)
        {
            da_sanpham = new Library.YTB.DA.Sanpham.DA_Sanpham();
            if (!da_sanpham.GetItem(id))
                return RedirectToAction("Index", "Home");
            ViewBag.MaSanPham = id;
            return View();
        }

        [HttpGet]
        public ActionResult Edit(int? id)
        {
            da_sanpham = new Library.YTB.DA.Sanpham.DA_Sanpham();
            if (!da_sanpham.GetItem(id))
                return RedirectToAction("Index", "Home");
            ViewBag.MaSanPham = id;
            return View();
        }

        [HttpGet]
        public ActionResult GET(Library.YTB.Utils.PagingParam pagingParam, int? masanpham, string tensanpham, bool? trangthai)
        {
            da_sanpham = new Library.YTB.DA.Sanpham.DA_Sanpham();
            apiResult = da_sanpham.GET(pagingParam, masanpham, tensanpham, trangthai);
            return Json(apiResult, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult GETID(int? id)
        {
            da_sanpham = new Library.YTB.DA.Sanpham.DA_Sanpham();
            apiResult = da_sanpham.GETID(id);
            return Json(apiResult, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult Create(SanPham sp)
        {
            if (!ModelState.IsValid)
            {
                return Json(new ApiResult() { success = false, message = "không thể tạo yêu cầu", data = null }, JsonRequestBehavior.AllowGet);
            }
            da_sanpham = new Library.YTB.DA.Sanpham.DA_Sanpham(User.Identity.Name);
            apiResult = da_sanpham.Create(sp);
            return Json(apiResult, JsonRequestBehavior.AllowGet);
        }

        [HttpPut]
        public ActionResult Edit(int id, SanPham sp)
        {
            if (!ModelState.IsValid)
            {
                return Json(new ApiResult() { success = false, message = "không thể tạo yêu cầu", data = null }, JsonRequestBehavior.AllowGet);
            }
            da_sanpham = new Library.YTB.DA.Sanpham.DA_Sanpham();
            apiResult = da_sanpham.Edit(id, sp);
            return Json(apiResult, JsonRequestBehavior.AllowGet);
        }

        [HttpPut]
        public ActionResult isActive(int id)
        {
            da_sanpham = new Library.YTB.DA.Sanpham.DA_Sanpham();
            apiResult = da_sanpham.isActive(id);
            return Json(apiResult, JsonRequestBehavior.AllowGet);
        }

        [HttpDelete]
        public ActionResult DELETE(int id)
        {
            da_sanpham = new Library.YTB.DA.Sanpham.DA_Sanpham();
            apiResult = da_sanpham.DELETE(id);
            return Json(apiResult, JsonRequestBehavior.AllowGet);
        }
    }
}