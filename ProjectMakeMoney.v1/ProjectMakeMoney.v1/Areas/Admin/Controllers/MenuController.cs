﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using System.Text.RegularExpressions;
using Library.YTB.Utils;
using Library.YTB.BO.Models;

namespace ProjectMakeMoney.v1.Areas.Admin.Controllers
{
    [Authorize(Roles = "TL0011")]
    public class MenuController : Controller
    {
        private Library.YTB.DA.Menu.DA_Menu da_menu = null;
        private Library.YTB.Utils.ApiResult apiResult = null;
        // GET: Admin/Menu

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Create()
        {
            return View();
        }

        public ActionResult Edit(int? idMenu)
        {
            da_menu = new Library.YTB.DA.Menu.DA_Menu();
            if (!da_menu.MenuGetItem(idMenu))
                return RedirectToAction("Index", "Home");
            ViewBag.id = idMenu;
            return View();
        }

        [HttpGet]
        public ActionResult GET(PagingParam pagingParam, string TenMenu, bool? trangthai)
        {
            da_menu = new Library.YTB.DA.Menu.DA_Menu();
            apiResult = da_menu.GET(pagingParam, TenMenu, trangthai);
            return Json(apiResult, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult GETID(int? idMenu)
        {
            da_menu = new Library.YTB.DA.Menu.DA_Menu();
            apiResult = da_menu.GETID(idMenu);
            return Json(apiResult, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult Create(Menu sp)
        {
            da_menu = new Library.YTB.DA.Menu.DA_Menu(User.Identity.Name);
            apiResult = da_menu.Create(sp);
            return Json(apiResult, JsonRequestBehavior.AllowGet);
        }

        [HttpPut]
        public ActionResult Edit(int idMenu, Menu sp)
        {
            if (!ModelState.IsValid)
            {
                return Json(new ApiResult() { success = false, message = "không thể tạo yêu cầu", data = null }, JsonRequestBehavior.AllowGet);
            }
            da_menu = new Library.YTB.DA.Menu.DA_Menu(User.Identity.Name);
            apiResult = da_menu.Edit(idMenu,sp);
            return Json(apiResult, JsonRequestBehavior.AllowGet);
        }

        [HttpDelete]
        public ActionResult DELETE(int? idMenu)
        {
            da_menu = new Library.YTB.DA.Menu.DA_Menu(User.Identity.Name);
            apiResult = da_menu.DELETE(idMenu);
            return Json(apiResult, JsonRequestBehavior.AllowGet);
        }

        [HttpPut]
        public ActionResult isActive(int? idMenu)
        {
            da_menu = new Library.YTB.DA.Menu.DA_Menu(User.Identity.Name);
            apiResult = da_menu.isActive(idMenu);
            return Json(apiResult, JsonRequestBehavior.AllowGet);
        }

    }
}