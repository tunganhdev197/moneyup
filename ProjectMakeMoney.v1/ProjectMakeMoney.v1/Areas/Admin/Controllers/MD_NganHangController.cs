﻿using Library.YTB.BO.Models;
using Library.YTB.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace ProjectMakeMoney.v1.Areas.Admin.Controllers
{
    [Authorize(Roles = "TL0009")]
    public class MD_NganHangController : Controller
    {
        // GET: Admin/MD_NganHang
        private Library.YTB.DA.NganHang.DA_NganHang da_nganhnag = null;

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Create()
        {
            return View();
        }

        [HttpGet]
        public ActionResult GET(PagingParam pagingParam, string tennganhang = null, bool? isActive = null)
        {
            da_nganhnag = new Library.YTB.DA.NganHang.DA_NganHang();
            ApiResult apiResult = da_nganhnag.GET(pagingParam, tennganhang, isActive);
            return Json(apiResult, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult GETID(int? id)
        {
            da_nganhnag = new Library.YTB.DA.NganHang.DA_NganHang();
            ApiResult apiResult = da_nganhnag.GETID(id);
            return Json(apiResult, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult Create(MD_NganHang nh)
        {
            if (!ModelState.IsValid)
            {
                return Json(new ApiResult() { success = false, message = "không thể tạo yêu cầu", data = null }, JsonRequestBehavior.AllowGet);
            }
            da_nganhnag = new Library.YTB.DA.NganHang.DA_NganHang(User.Identity.Name);
            ApiResult apiResult = da_nganhnag.Create(nh);
            return Json(apiResult, JsonRequestBehavior.AllowGet);
        }

        [HttpDelete]
        public ActionResult DELETE(int? id)
        {
            da_nganhnag = new Library.YTB.DA.NganHang.DA_NganHang();
            ApiResult apiResult = da_nganhnag.DELETE(id);
            return Json(apiResult, JsonRequestBehavior.AllowGet);
        }

        [HttpPut]
        public ActionResult IsActive(int? id)
        {
            da_nganhnag = new Library.YTB.DA.NganHang.DA_NganHang();
            ApiResult apiResult = da_nganhnag.IsActive(id);
            return Json(apiResult, JsonRequestBehavior.AllowGet);
        }
    }
}