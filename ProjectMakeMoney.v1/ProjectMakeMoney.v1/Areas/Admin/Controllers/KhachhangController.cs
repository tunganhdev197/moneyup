﻿using Library.YTB.BO.Models;
using Library.YTB.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace ProjectMakeMoney.v1.Areas.Admin.Controllers
{
    [Authorize(Roles = "TL0010")]
    public class KhachhangController : Controller
    {
        private Library.YTB.DA.Khachhang.DA_Khachhang da_khachhang = null;
        // GET: Admin/Khachhang

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Create()
        {
            return View();
        }

        public ActionResult Edit(long? idKhachhang)
        {
            da_khachhang = new Library.YTB.DA.Khachhang.DA_Khachhang();
            if (da_khachhang.CheckQuantityInID(idKhachhang)==false)
                return RedirectToAction("Index", "Home");
            ViewBag.id = idKhachhang;
            ViewData["ID"] = idKhachhang;
            return View();
        }

        [HttpGet]
        public ActionResult GET(PagingParam pagingParam, string UserName, string Email, bool? IsActive, bool? TTKiemTien, bool? TTXacThucTK)
        {
            da_khachhang = new Library.YTB.DA.Khachhang.DA_Khachhang();
            ApiResult apiResult = da_khachhang.GET(pagingParam, UserName, Email, IsActive, TTKiemTien, TTXacThucTK);
            return Json(apiResult, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult GETID(long? idKhachhang)
        {
            da_khachhang = new Library.YTB.DA.Khachhang.DA_Khachhang();
            ApiResult apiResult = da_khachhang.GETID(idKhachhang);
            return Json(apiResult, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult Create(KhachHang kh, string comfirmpassword)
        {
            da_khachhang = new Library.YTB.DA.Khachhang.DA_Khachhang();
            ApiResult apiResult = da_khachhang.Create(kh, comfirmpassword, Url.Action("Index", "Khachhang"));
            return Json(apiResult, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult EditPUT(long idKhachhang, KhachHang kh)
        {
            da_khachhang = new Library.YTB.DA.Khachhang.DA_Khachhang(User.Identity.Name);
            ApiResult apiResult = da_khachhang.EditPUT(idKhachhang, kh, Url.Action("Index", "Khachhang"));
            return Json(apiResult, JsonRequestBehavior.AllowGet);
        }

        [HttpDelete]
        public ActionResult Delete(long idKhachhang)
        {
            da_khachhang = new Library.YTB.DA.Khachhang.DA_Khachhang();
            ApiResult apiResult = da_khachhang.Delete(idKhachhang);
            return Json(apiResult, JsonRequestBehavior.AllowGet);
        }

        [HttpPut]
        public ActionResult isActive(int idKhachhang)
        {
            da_khachhang = new Library.YTB.DA.Khachhang.DA_Khachhang();
            ApiResult apiResult = da_khachhang.isActive(idKhachhang);
            return Json(apiResult, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult ChkTTKiemtien(int idKhachhang)
        {
            da_khachhang = new Library.YTB.DA.Khachhang.DA_Khachhang();
            ApiResult apiResult = da_khachhang.ChkTTKiemtien(idKhachhang);
            return Json(apiResult, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult TurnOnMakeMoney(long idKhachhang, string Nganhang, string TenChuThe, string Sotaikhoan)
        {
            da_khachhang = new Library.YTB.DA.Khachhang.DA_Khachhang(User.Identity.Name);
            ApiResult apiResult = da_khachhang.TurnOnMakeMoney(idKhachhang, Nganhang, TenChuThe, Sotaikhoan);
            return Json(apiResult, JsonRequestBehavior.AllowGet);
        }
    }
}