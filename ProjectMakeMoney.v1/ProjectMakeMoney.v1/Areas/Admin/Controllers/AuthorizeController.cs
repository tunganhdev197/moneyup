﻿using Library.YTB.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace ProjectMakeMoney.v1.Areas.Admin.Controllers
{
    public class AuthorizeController : Controller
    {
        private Library.YTB.BO.Models.AppDbContext db = new Library.YTB.BO.Models.AppDbContext();
        // GET: Admin/Authorize
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Decentralization(string id)
        {
            if (User.Identity.IsAuthenticated)
            {
                int? RoleID = db.Users.Where(x => x.username == User.Identity.Name).Select(x => x.role_id).FirstOrDefault();
                if (db.Decentralizations.Count(x => x.RoleID == RoleID && x.Authority.Code == id) > 0)
                {
                    return Json(new ApiResult() { success = true, message = string.Empty, data = null }, JsonRequestBehavior.AllowGet);
                }
            }
            return Json(new ApiResult() { success = false, message = string.Empty, data = null }, JsonRequestBehavior.AllowGet);
        }
    }
}