﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using System.Text.RegularExpressions;
using Library.YTB.Utils;
using Library.YTB.BO.Models;

namespace ProjectMakeMoney.v1.Areas.Admin.Controllers
{
    [Authorize(Roles = "TL0007")]
    public class VideoController : Controller
    {
        private Library.YTB.DA.Videos.Videos da_video = null;
        private Library.YTB.Utils.ApiResult apiResult = null;
        // GET: Admin/Video

        [HttpGet]
        public ActionResult Edit(int? ID)
        {
            da_video = new Library.YTB.DA.Videos.Videos();
            if (!da_video.GetItem(ID))
                return RedirectToAction("Index", "Home");
            ViewBag.id = ID;
            return View();
        }

        [HttpGet]
        public ActionResult Detail(int? ID)
        {
            da_video = new Library.YTB.DA.Videos.Videos();
            if (!da_video.GetItem(ID))
                return RedirectToAction("Index", "Home");
            ViewBag.id = ID;
            return View();
        }

        public ActionResult Index(long? idkenh)
        {
            ViewBag.idkenh = idkenh;
            return View();
        }

        public ActionResult Create()
        {
            return View();
        }

        [HttpGet]
        public ActionResult GET(PagingParam pagingParam, string tieude, bool? thinhhanh, bool? dexuat, bool? trangthai)
        {
            da_video = new Library.YTB.DA.Videos.Videos();
            apiResult = da_video.GET(pagingParam, tieude, thinhhanh, dexuat, trangthai);
            return Json(apiResult, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult GetListKenh(long? id)
        {
            da_video = new Library.YTB.DA.Videos.Videos();
            apiResult = da_video.GetListKenh(id);
            return Json(apiResult, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult GETID(long? id)
        {
            da_video = new Library.YTB.DA.Videos.Videos();
            apiResult = da_video.GETID(id);
            return Json(apiResult, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult Create(Video vd)
        {
            da_video = new Library.YTB.DA.Videos.Videos(User.Identity.Name);
            apiResult = da_video.Create(vd);
            return Json(apiResult, JsonRequestBehavior.AllowGet);
        }

        [HttpPut]
        public ActionResult Edit(long id, Video vd)
        {
            if (!ModelState.IsValid)
                return Json(new ApiResult() { success = false, message = "không thể tạo yêu cầu", data = null }, JsonRequestBehavior.AllowGet);

            da_video = new Library.YTB.DA.Videos.Videos(User.Identity.Name);
            apiResult = da_video.Edit(id,vd);
            return Json(apiResult, JsonRequestBehavior.AllowGet);
        }

        [HttpPut]
        public ActionResult isActive(long id)
        {
            if (!ModelState.IsValid)
                return Json(new ApiResult() { success = false, message = "không thể tạo yêu cầu", data = null }, JsonRequestBehavior.AllowGet);

            da_video = new Library.YTB.DA.Videos.Videos(User.Identity.Name);
            apiResult = da_video.isActive(id);
            return Json(apiResult, JsonRequestBehavior.AllowGet);
        }

        [HttpDelete]
        public ActionResult DELETE(long id)
        {
            da_video = new Library.YTB.DA.Videos.Videos(User.Identity.Name);
            apiResult = da_video.DELETE(id);
            return Json(apiResult, JsonRequestBehavior.AllowGet);
        }
    }
}