﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ProjectMakeMoney.v1.Areas.Admin.Controllers
{
   
    public class HomeController : Controller
    {
        // GET: Admin/Home
        [Authorize(Roles = "TL0012")]
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult Diachi()
        {
            return View();
        }
    }
}