﻿using Library.YTB.BO.Models;
using Library.YTB.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace ProjectMakeMoney.v1.Areas.Admin.Controllers
{
    [Authorize(Roles = "TL0003")]
    public class UserController : Controller
    {
        // GET: Admin/User
        private Library.YTB.DA.User.DA_User da_user = null;
        private Library.YTB.Utils.ApiResult apiResult = null;
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Create()
        {
            return View();
        }

        public ActionResult Edit(int? id)
        {

            da_user = new Library.YTB.DA.User.DA_User();
            if (!da_user.GetItem(id))
                return RedirectToAction("Index", "Home");
            ViewBag.ID = id;

            return View(da_user.actionEdit(id));
        }

        public ActionResult UserProfiles()
        {
            return View();
        }

        public ActionResult Resetpass()
        {
            return View();
        }

        [HttpGet]
        public ActionResult GETProfile()
        {
            da_user = new Library.YTB.DA.User.DA_User(User.Identity.Name);
            apiResult = da_user.GETProfile();
            return Json(apiResult, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult GET(PagingParam pagingParam, string code , string username, string email, string fullname, int? roleid, bool? active)
        {
            da_user = new Library.YTB.DA.User.DA_User(User.Identity.Name);
            apiResult = da_user.GET(pagingParam, code, username, email, fullname, roleid, active);
            return Json(apiResult, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult GETID(int? id)
        {
            da_user = new Library.YTB.DA.User.DA_User();
            apiResult = da_user.GETID(id);
            return Json(apiResult, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult GETRoleID()
        {
            da_user = new Library.YTB.DA.User.DA_User();
            apiResult = da_user.GETRoleID();
            return Json(apiResult, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult Create(User user, string comfirmpassword)
        {
            if (!ModelState.IsValid)
            {
                return Json(new ApiResult() { success = false, message = "không thể tạo yêu cầu", data = null }, JsonRequestBehavior.AllowGet);
            }
            da_user = new Library.YTB.DA.User.DA_User(User.Identity.Name);
            apiResult = da_user.Create(user, comfirmpassword, Url.Action("Index", "User"));
            return Json(apiResult, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult Edit(User user)
        {
            if (!ModelState.IsValid)
            {
                return Json(new ApiResult() { success = false, message = "không thể tạo yêu cầu", data = null }, JsonRequestBehavior.AllowGet);
            }
            da_user = new Library.YTB.DA.User.DA_User(User.Identity.Name);
            apiResult = da_user.Edit(user, Url.Action("Index", "User"));
            return Json(apiResult, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult EditProfile(User user)
        {
            da_user = new Library.YTB.DA.User.DA_User(User.Identity.Name);
            apiResult = da_user.EditProfile(user, Url.Action("UserProfiles", "User"));
            return Json(apiResult, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult Delete(int id)
        {
            da_user = new Library.YTB.DA.User.DA_User(User.Identity.Name);
            apiResult = da_user.Delete(id);
            return Json(apiResult, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult Resetpass(string pass, string passnew, string comfirmpass)
        {
            da_user = new Library.YTB.DA.User.DA_User(User.Identity.Name);
            apiResult = da_user.Resetpass(pass, passnew, comfirmpass, Url.Action("Index", "Home"));
            return Json(apiResult, JsonRequestBehavior.AllowGet);
        }
    }
}