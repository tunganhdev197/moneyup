﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ProjectMakeMoney.v1.Areas.Admin.Controllers
{
    [Authorize(Roles = "TL0001")]
    public class CaidatchungController : Controller
    {
        private Library.YTB.DA.Caidatchung.Caidatchung caidatchung = null;

        // GET: Admin/Caidatchung
        public ActionResult Index()
        {
            return View();
        }

        [HttpPut]
        public ActionResult Edit(string Logo_TrangChu, string Gioithieu, string Email_1, string Email_2, string Email_3, string Hotline_1, string Hotline_2, string Hotline_3, string LinkFanpage
           , string LinkZalo, string LinkInstagram, string LinkYoutube, string LinkGoogleAnalytic, string LinkGoogleMaster, string AnhQR_1, string AnhQR_2, string AnhQR_3, string TruSo, string ggMap)
        {
            caidatchung = new Library.YTB.DA.Caidatchung.Caidatchung(User.Identity.Name);
            Library.YTB.Utils.ApiResult apiResult = caidatchung.Edit(Logo_TrangChu, Gioithieu, Email_1, Email_2, Email_3, Hotline_1, Hotline_2, Hotline_3, LinkFanpage,
            LinkZalo, LinkInstagram, LinkYoutube, LinkGoogleAnalytic, LinkGoogleMaster, AnhQR_1, AnhQR_2, AnhQR_3, TruSo, ggMap);
            return Json(apiResult, JsonRequestBehavior.AllowGet);
        }
    }
}