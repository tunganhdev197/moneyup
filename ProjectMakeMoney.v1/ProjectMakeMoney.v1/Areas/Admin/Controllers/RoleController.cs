﻿using Library.YTB.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace ProjectMakeMoney.v1.Areas.Admin.Controllers
{
    [Authorize(Roles = "TL0002")]
    public class RoleController : Controller
    {
        private Library.YTB.DA.Role.DA_Role da_role = new Library.YTB.DA.Role.DA_Role();
        private Library.YTB.Utils.ApiResult apiResult = null;
        // GET: Admin/Role
        public ActionResult Index()
        {
            return View(da_role.actionIndex());
        }

        public ActionResult Create()
        {
            return View();
        }

        public ActionResult Edit(int id)
        {
            return View(da_role.actionEdit(id));
        }

        [HttpPost]
        public ActionResult Edit(int RoleID, int[] AuthorID)
        {
            return View(da_role.Edit(RoleID,AuthorID));
        }

        [HttpPost]
        public ActionResult Create(string code, string name, int[] AuthorID)
        {
            Library.YTB.BO.Models.Role rl = da_role.Create(code, name, AuthorID);
            if (rl != null)
                return View(rl);
            return View();
        }

        [HttpPost]
        public ActionResult Delete(int RoleID)
        {
            apiResult = da_role.Delete(RoleID);
            return Json(apiResult, JsonRequestBehavior.AllowGet);
        }
    }
}