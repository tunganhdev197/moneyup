﻿using Library.YTB.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ProjectMakeMoney.v1.Areas.Admin.Controllers
{
    [Authorize(Roles = "TL0006")]
    public class KenhController : Controller
    {
        private Library.YTB.DA.Kenh.DA_Kenh da_kenh = null;
        private Library.YTB.Utils.ApiResult apiResult = null;
        // GET: Admin/Kenh
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult Create()
        {
            return View();
        }

        [HttpGet]
        public ActionResult Edit(int? idKenh)
        {
            da_kenh = new Library.YTB.DA.Kenh.DA_Kenh();
            if (idKenh == null)
                return RedirectToAction("Index", "Home");
            if (!da_kenh.CheckQuantityInID(idKenh))
                return RedirectToAction("Index", "Home");

            ViewBag.id = idKenh;
            return View();
        }

        [HttpGet]
        public ActionResult Detail(int? idKenh)
        {
            da_kenh = new Library.YTB.DA.Kenh.DA_Kenh();
            if (idKenh == null)
                return RedirectToAction("Index", "Home");
            if (!da_kenh.CheckQuantityInID(idKenh))
                return RedirectToAction("Index", "Home");
            ViewBag.id = idKenh;
            return View();
        }

        [HttpGet]
        public ActionResult GET(Library.YTB.Utils.PagingParam pagingParam, string tenkenh, string EmailKenh, string sdt, bool? isPopular, bool? isPrioritize, bool? isactive)
        {
            da_kenh = new Library.YTB.DA.Kenh.DA_Kenh();
            apiResult = da_kenh.GET(pagingParam, tenkenh, EmailKenh, sdt, isPopular, isPrioritize, isactive);
            return Json(apiResult, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult GETID(int? idKenh)
        {
            da_kenh = new Library.YTB.DA.Kenh.DA_Kenh();
            apiResult = da_kenh.GETID(idKenh);
            return Json(apiResult, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult Create(Library.YTB.BO.Models.Kenh ks)
        {
            if (!ModelState.IsValid)
            {
                return Json(new ApiResult() { success = false, message = "không thể tạo yêu cầu", data = null }, JsonRequestBehavior.AllowGet);
            }

            da_kenh = new Library.YTB.DA.Kenh.DA_Kenh(User.Identity.Name);
            apiResult = da_kenh.Create(ks);
            return Json(apiResult, JsonRequestBehavior.AllowGet);
        }

        [HttpPut]
        public ActionResult Edit(int idKenh, Library.YTB.BO.Models.Kenh ks)
        {
            if (!ModelState.IsValid)
                return Json(new ApiResult() { success = false, message = "không thể tạo yêu cầu", data = null }, JsonRequestBehavior.AllowGet);

            da_kenh = new Library.YTB.DA.Kenh.DA_Kenh(User.Identity.Name);
            apiResult = da_kenh.Edit(idKenh, ks);
            return Json(apiResult, JsonRequestBehavior.AllowGet);
        }

        [HttpPut]
        public ActionResult isActive(int? idKenh)
        {
            da_kenh = new Library.YTB.DA.Kenh.DA_Kenh(User.Identity.Name);
            apiResult = da_kenh.isActive(idKenh);
            return Json(apiResult, JsonRequestBehavior.AllowGet);
        }

        [HttpDelete]
        public ActionResult DELETE(int? idKenh)
        {
            da_kenh = new Library.YTB.DA.Kenh.DA_Kenh(User.Identity.Name);
            apiResult = da_kenh.DELETE(idKenh);
            return Json(apiResult, JsonRequestBehavior.AllowGet);
        }
    }
}