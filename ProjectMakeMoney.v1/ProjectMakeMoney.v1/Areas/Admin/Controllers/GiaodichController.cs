﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using System.Data.Entity;
using System.Text.RegularExpressions;

namespace ProjectMakeMoney.v1.Areas.Admin.Controllers
{
    [Authorize(Roles = "TL0005")]
    public class GiaodichController : Controller
    {
        //<option value = "1" > Chờ xác nhân</option>
        //<option value = "2" > Chờ duyệt</option>
        //<option value = "3" > Đã duyệt</option>
        //<option value = "4" > Đã chuyển tiền</option>
        //<option value = "0" > Hủy giao dịch</option>
        private Library.YTB.DA.Giaodich.DA_Giaodich giaodich =null;

        // GET: Admin/Giaodich
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Giaodichdaduyet()
        {
            return View();
        }

        public ActionResult Info(long? id)
        {
            Library.YTB.DA.Giaodich.DA_Giaodich giaodich = new Library.YTB.DA.Giaodich.DA_Giaodich();
            if (id == null)
            {
                return RedirectToAction("Index", "Home");
            }

            if (!giaodich.CheckQuantityInID(id))
            {
                return RedirectToAction("Index", "Home");
            }
            ViewBag.id = id;
            return View();
        }

        [HttpGet]
        public ActionResult GetRole()
        {
            giaodich = new Library.YTB.DA.Giaodich.DA_Giaodich(User.Identity.Name);
            Library.YTB.Utils.ApiResult apiResult = giaodich.GetRole();
            return Json(apiResult, JsonRequestBehavior.AllowGet);
        }
        [HttpGet]
        public ActionResult GET(Library.YTB.Utils.PagingParam pagingParam, long? magiaodich, long? makhachhang, string NhanVienDuyet, DateTime? formdate, DateTime? todate, int? trangthaiduyet)
        {
            if (pagingParam == null)
            {
                pagingParam = new Library.YTB.Utils.PagingParam();
            }
            giaodich = new Library.YTB.DA.Giaodich.DA_Giaodich(User.Identity.Name);
            Library.YTB.Utils.ApiResult apiResult = giaodich.GET(pagingParam, magiaodich, makhachhang, NhanVienDuyet, formdate, todate, trangthaiduyet);
            return Json(apiResult, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult GetID(int? id)
        {
            giaodich = new Library.YTB.DA.Giaodich.DA_Giaodich(User.Identity.Name);
            Library.YTB.Utils.ApiResult apiResult = giaodich.GetID(id);
            return Json(apiResult, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult isActive(long[] listGiaodich, int? trangthai, string Node)
        {
            giaodich = new Library.YTB.DA.Giaodich.DA_Giaodich(User.Identity.Name);
            Library.YTB.Utils.ApiResult apiResult = giaodich.isActive(listGiaodich, trangthai, Node);
            return Json(apiResult, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult GETGiaodichduyet(Library.YTB.Utils.PagingParam pagingParam, long? magiaodich, long? makhachhang, string nhanvienduyet, DateTime? formdate, DateTime? todate)
        {
            if (pagingParam == null)
            {
                pagingParam = new Library.YTB.Utils.PagingParam();
            }
            giaodich = new Library.YTB.DA.Giaodich.DA_Giaodich(User.Identity.Name);
            Library.YTB.Utils.ApiResult apiResult = giaodich.GETGiaodichduyet(pagingParam, magiaodich, makhachhang, nhanvienduyet, formdate, todate);
            return Json(apiResult, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult isActive2(long Magiaodich, int? trangthai, string Node)
        {
            giaodich = new Library.YTB.DA.Giaodich.DA_Giaodich(User.Identity.Name);
            Library.YTB.Utils.ApiResult apiResult = giaodich.isActive2(Magiaodich, trangthai, Node);
            return Json(apiResult, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult isActive3(int Magiaodich, string Node)
        {
            giaodich = new Library.YTB.DA.Giaodich.DA_Giaodich(User.Identity.Name);
            Library.YTB.Utils.ApiResult apiResult = giaodich.isActive3(Magiaodich, Node);
            return Json(apiResult, JsonRequestBehavior.AllowGet);
        }
    }
}