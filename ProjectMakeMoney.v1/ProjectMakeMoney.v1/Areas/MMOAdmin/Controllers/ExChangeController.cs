﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ProjectMakeMoney.v1.Areas.MMOAdmin.Controllers
{
    [Authorize(Roles = "TL0000")]
    public class ExChangeController : Controller
    {
        private Library.YTB.BO.Models.AppDbContext db = new Library.YTB.BO.Models.AppDbContext();
        private Library.YTB.DA.Exchange.ExChange objExchange = null;
        // GET: ExChange
        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public ActionResult GET(Library.YTB.Utils.PagingParam pagingParam, long? magiaodich, DateTime? formdate, DateTime? todate)
        {
            objExchange = new Library.YTB.DA.Exchange.ExChange(User.Identity.Name);
            Library.YTB.Utils.ApiResult apiResult = objExchange.GET(pagingParam, magiaodich, formdate, todate);
            return Json(apiResult, JsonRequestBehavior.AllowGet);
        }

    }
}