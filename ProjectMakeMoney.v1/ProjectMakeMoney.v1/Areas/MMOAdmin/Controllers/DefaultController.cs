﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Google.Apis.Auth.OAuth2;
using System.Threading.Tasks;
using System.Threading;
using Google.Apis.Auth.OAuth2.Mvc;
using Library.YTB.Utils;

namespace ProjectMakeMoney.v1.Areas.MMOAdmin.Controllers
{
    [Authorize(Roles = "TL0000")]
    public class DefaultController : Controller
    {
        private Library.YTB.DA.ChannelsToUser.DA_ChannelsToUser objChannelsToUser = null;
        private Library.YTB.DA.Shared.Notification objNotification = null;
        private Google.Apis.Auth.OAuth2.Mvc.FlowMetadata Flow = null;
        private int? ProjectID = null;
        public DefaultController()
        {
            this.ProjectID = Library.YTB.Utils.Utils.MyProjectID();
            this.Flow = GoogleAuth2.Flow.MyProject.Flow(this.ProjectID);
        }
        // GET: MMOAdmin/Default
        [HttpGet]
        public ActionResult GET()
        {
            objChannelsToUser = new Library.YTB.DA.ChannelsToUser.DA_ChannelsToUser(User.Identity.Name);
            Library.YTB.Utils.ApiResult apiResult = objChannelsToUser.GET();
            return Json(apiResult, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public async Task<ActionResult> GETAsync(CancellationToken cancellationToken)
        {
            objChannelsToUser = new Library.YTB.DA.ChannelsToUser.DA_ChannelsToUser(User.Identity.Name);
            var result = await new AuthorizationCodeMvcApp(this, this.Flow).AuthorizeAsync(cancellationToken);

            if (result.Credential != null)
            {
                string user = this.Session["user"].ToString();
                string ChangeID = null;
                string Title = null;

                bool bolChannel = GoogleAuth2.Flow.YoutubeData.GetChannel(result.Credential.Token.AccessToken, ref ChangeID, ref Title);

                if (string.IsNullOrEmpty(ChangeID))
                {
                    await result.Credential.RevokeTokenAsync(cancellationToken);
                    return Content("<script>alert('google liên kết cần được đăng kí kênh trên youtobe');window.close();</script>");
                }
                if (!objChannelsToUser.UpsetChanelsToUser(this.ProjectID, ChangeID, Title, result.Credential.Token.AccessToken, result.Credential.Token.RefreshToken))
                {
                    await result.Credential.RevokeTokenAsync(cancellationToken);
                    return Content("<script>alert('lỗi hệ thống, vui lòng thử lại'); window.close();</script>");
                }
                return Content("<script>alert('liên kết thành công');window.close();</script>");
            }
            else
                return Json(new ApiResult() { success = true, message = string.Empty, data = null, action = result.RedirectUri }, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public async Task<ActionResult> GETCheckLogin(CancellationToken cancellationToken)
        {
            var result = await new AuthorizationCodeMvcApp(this, this.Flow).AuthorizeAsync(cancellationToken);
            if (result.Credential != null)
            {
                return Json(new ApiResult() { success = false, message = "Bạn cần khởi động lại trình duyệt hoặc xóa cookie để tiếp tục liên kết, hướng dẫn xóa cookie <a href='#'>tại đây...</a>", data = null, action = null }, JsonRequestBehavior.AllowGet);
            }

            return Json(new ApiResult() { success = true, message = string.Empty, data = null, action = null }, JsonRequestBehavior.AllowGet);
        }

        #region thông báo
        [HttpGet]
        public ActionResult HistoryNotify()
        {
            objNotification = new Library.YTB.DA.Shared.Notification(User.Identity.Name);
            Library.YTB.Utils.ApiResult apiResult = objNotification.HistoryNotifi();
            return Json(apiResult, JsonRequestBehavior.AllowGet);
        }
        [HttpGet]
        public ActionResult NotifyInfo(long? id)
        {
            objNotification = new Library.YTB.DA.Shared.Notification(User.Identity.Name);
            Library.YTB.Utils.ApiResult apiResult = objNotification.NotfifiInfo(id);
            return Json(apiResult, JsonRequestBehavior.AllowGet);
        }
        [HttpGet]
        public ActionResult NotifyCount()
        {
            objNotification = new Library.YTB.DA.Shared.Notification(User.Identity.Name);
            Library.YTB.Utils.ApiResult apiResult = objNotification.NotfifiInfoCount();
            return Json(apiResult, JsonRequestBehavior.AllowGet);
        }
        #endregion
    }
}