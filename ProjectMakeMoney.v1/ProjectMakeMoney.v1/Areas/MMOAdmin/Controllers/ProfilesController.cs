﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.Entity;
using System.Data;
using Library.YTB.DA.Profiles;
using Library.YTB.Utils;

namespace ProjectMakeMoney.v1.Areas.MMOAdmin.Controllers
{
    [Authorize(Roles = "TL0000")]
    public class ProfilesController : Controller
    {
        private Profiles objProfile = null;
        private string message = string.Empty;
        // GET: MMOAdmin/Profiles
        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public ActionResult GetInfoProfile()
        {
            objProfile = new Profiles(User.Identity.Name);
            return Json(new ApiResult() { success = true, message = string.Empty, data = objProfile.GetInfoProfile() }, JsonRequestBehavior.AllowGet);
        }
        #region lấy thông tin tinh thành - ngân hàng
        [HttpGet]
        public ActionResult GetNganhang()
        {
            objProfile = new Profiles();
            return Json(new ApiResult() { success = true, message = string.Empty, data = objProfile.GetNganhang() }, JsonRequestBehavior.AllowGet);
        }
        #endregion
        [HttpGet]
        public ActionResult GETID()
        {
            objProfile = new Profiles(User.Identity.Name);
            return Json(new ApiResult() { success = true, message = string.Empty, data = objProfile.GETID() }, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult PutProfile(Library.YTB.BO.Models.KhachHang kh)
        {
            objProfile = new Profiles(User.Identity.Name);
            if (!objProfile.PutProfile(kh, ref message))
                return Json(new ApiResult() { success = false, message = message, data = null, action = null }, JsonRequestBehavior.AllowGet);
            return Json(new ApiResult() { success = true, message = "<p>Cập nhập thông tin thành công</p>", data = null, action = null }, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult Resetpass(string pass, string passnew, string comfirmpass)
        {
            objProfile = new Profiles(User.Identity.Name);
            Library.YTB.Utils.ApiResult apiresult = objProfile.Resetpass(pass, passnew, comfirmpass);
            return Json(apiresult, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult transactionRequest(Library.YTB.BO.Models.YeuCauGiaoDich yc)
        {
            objProfile = new Profiles(User.Identity.Name);
            Library.YTB.Utils.ApiResult apiresult = objProfile.transactionRequest(yc);
            return Json(apiresult, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult KhangnghiPost(Library.YTB.BO.Models.KhangNghi x)
        {
            objProfile = new Profiles(User.Identity.Name);
            Library.YTB.Utils.ApiResult apiresult = objProfile.KhangnghiPost(x);
            return Json(apiresult, JsonRequestBehavior.AllowGet);
        }
    }
}