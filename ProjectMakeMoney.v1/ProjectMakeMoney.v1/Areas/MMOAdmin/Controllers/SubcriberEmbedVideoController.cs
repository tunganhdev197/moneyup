﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Library.YTB.Utils;

namespace ProjectMakeMoney.v1.Areas.MMOAdmin.Controllers
{
    [Authorize(Roles = "TL0000")]
    public class SubcriberEmbedVideoController : Controller
    {
        private Library.YTB.DA.Subcriber.DA_Subcriber objSubcriber = null;
        private Library.YTB.DA.Secon.DA_Secon objSecon = null;
        // GET: MMOAdmin/SubcriberEmbedVideo
        public ActionResult Index(long? id, int? idkenh)
        {
            if (id == null)
                return RedirectToAction("Index", "TrangChu");
            string connect = System.Configuration.ConfigurationManager.ConnectionStrings["AppDbContext"].ConnectionString;
            objSubcriber = new Library.YTB.DA.Subcriber.DA_Subcriber(User.Identity.Name, id, connect);
            long? idkenhCopy = null;
            if (!objSubcriber.GETIndex(idkenh, ref idkenhCopy))
                return RedirectToAction("Index", "TrangChu");
            ViewBag.IdKenh = idkenhCopy;
            ViewBag.channelsToUserID = id;
            Session["channelsToUserID"] = id;
            return View();
        }

        [HttpGet]
        public ActionResult GETID(long? id)
        {
            long channelsUserID = Convert.ToInt32(Session["channelsToUserID"]);
            string connect = System.Configuration.ConfigurationManager.ConnectionStrings["AppDbContext"].ConnectionString;
            objSubcriber = new Library.YTB.DA.Subcriber.DA_Subcriber(User.Identity.Name, channelsUserID, connect);
            Library.YTB.Utils.ApiResult apiResult = objSubcriber.GETID(id, Url.Action("Index", "TrangChu"));
            return Json(apiResult, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult GET(Library.YTB.Utils.PagingParam pagingParam)
        {
            long channelsUserID = Convert.ToInt32(Session["channelsToUserID"]);
            string connect = System.Configuration.ConfigurationManager.ConnectionStrings["AppDbContext"].ConnectionString;
            objSubcriber = new Library.YTB.DA.Subcriber.DA_Subcriber(User.Identity.Name, channelsUserID, connect);
            Library.YTB.Utils.ApiResult apiResult = objSubcriber.GET(pagingParam);
            return Json(apiResult, JsonRequestBehavior.AllowGet);
        }

        public ActionResult NextSecons()
        {
            objSecon = new Library.YTB.DA.Secon.DA_Secon(User.Identity.Name,2);
            if (objSecon.GetSecon() < 10)
                objSecon.UpsetSecon();

            return Json(new ApiResult() { success = true, message = null, data = null }, JsonRequestBehavior.AllowGet);

        }

        [HttpPost]
        public ActionResult PostSubcriber(long idKenh)
        {
            #region session error
            if (Session["channelsToUserID"] == null)
                return Json(new ApiResult() { success = false, message = "Hết phiên làm việc,Yêu cầu load lại trang", data = null }, JsonRequestBehavior.AllowGet);
            if (string.IsNullOrEmpty(Session["channelsToUserID"].ToString()))
                return Json(new ApiResult() { success = false, message = "Hết phiên làm việc,Yêu cầu load lại trang", data = null, action = "" }, JsonRequestBehavior.AllowGet);
            long channelsToUserID = Convert.ToInt64(Session["channelsToUserID"]);
            #endregion
            decimal Pricesub = 0;
            objSubcriber = new Library.YTB.DA.Subcriber.DA_Subcriber(User.Identity.Name);
            Library.YTB.Utils.ApiResult apiResult = objSubcriber.PostSubcriber(idKenh, channelsToUserID, ref Pricesub);

            return Json(apiResult, JsonRequestBehavior.AllowGet);
        }
    }
}