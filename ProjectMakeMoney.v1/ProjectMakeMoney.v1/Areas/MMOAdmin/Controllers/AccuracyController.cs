﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Library.YTB.DA.Accuracy;

namespace ProjectMakeMoney.v1.Areas.MMOAdmin.Controllers
{
    [Authorize(Roles = "TL0000")]
    public class AccuracyController : Controller
    {
        private Accuracy objAccuracy = null;
        // GET: MMOAdmin/Accuracy
        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public ActionResult AccountVerification()
        {
            objAccuracy = new Accuracy(User.Identity.Name);
            Library.YTB.Utils.ApiResult apiResult = objAccuracy.AccountVerification();
            return Json(apiResult, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult Accuracyxttk()
        {
            objAccuracy = new Accuracy(User.Identity.Name);
            Library.YTB.Utils.ApiResult apiResult = objAccuracy.Accuracyxttk();
            return Json(apiResult, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult isEmptyMaAnToan()
        {
            objAccuracy = new Accuracy(User.Identity.Name);
            Library.YTB.Utils.ApiResult apiResult = objAccuracy.isEmptyMaAnToan();
            return Json(apiResult, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult createsecuritycode(int? MaAnToan, int? MaAnToancf, string Password)
        {
            objAccuracy = new Accuracy(User.Identity.Name);
            Library.YTB.Utils.ApiResult apiResult = objAccuracy.createsecuritycode(MaAnToan, MaAnToancf, Password);
            return Json(apiResult, JsonRequestBehavior.AllowGet);
        }

        [HttpPut]
        public ActionResult ForgotTheSecurityCode(string password)
        {
            objAccuracy = new Accuracy(User.Identity.Name);
            Library.YTB.Utils.ApiResult apiResult = objAccuracy.ForgotTheSecurityCode(password);
            return Json(apiResult, JsonRequestBehavior.AllowGet);
        }
    }
}