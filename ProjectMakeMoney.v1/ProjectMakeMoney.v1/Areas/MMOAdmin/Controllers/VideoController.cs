﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using System.Data.Entity;
using System.Data;
using Newtonsoft.Json.Linq;
using Library.YTB.BO.Models;
using Library.YTB.Utils;

namespace ProjectMakeMoney.v1.Areas.MMOAdmin.Controllers
{
    [Authorize(Roles = "TL0000")]
    public class VideoController : Controller
    {
        private AppDbContext db = new AppDbContext();
        private Library.YTB.DA.Videos.Videos objVideos = null;
        private Library.YTB.DA.Secon.DA_Secon objSecon = null;
        private string connectionstring = System.Configuration.ConfigurationManager.ConnectionStrings["AppDbContext"].ConnectionString;
        // GET: Video
        public ActionResult Index(long? id)
        {
            objVideos = new Library.YTB.DA.Videos.Videos(User.Identity.Name);
            long? idkenhCopy = null;           
            if (!objVideos.GetIndex(id,connectionstring,ref idkenhCopy))
                return RedirectToAction("Index", "TrangChu");
            ViewBag.ID = idkenhCopy;
            return View();
        }

        [HttpGet]
        public ActionResult GETOffer(Library.YTB.Utils.PagingParam pagingParam)
        {
            objVideos = new Library.YTB.DA.Videos.Videos(User.Identity.Name);
            Library.YTB.Utils.ApiResult apiResult = objVideos.GETOffer(pagingParam, connectionstring);
            if(apiResult.success==false)
                return RedirectToAction("Index", "TrangChu");
            return Json(apiResult, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult GETID(long? id)
        {
            objVideos = new Library.YTB.DA.Videos.Videos(User.Identity.Name);
            Library.YTB.Utils.ApiResult apiResult = objVideos.GETID(id, connectionstring,Url.Action("Index", "Video"));
           
            return Json(apiResult, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult PostVideo(long idVideo)
        {
            objVideos = new Library.YTB.DA.Videos.Videos(User.Identity.Name);
            string iserror = objVideos.isErrorPostVideo(idVideo);
            if (!string.IsNullOrEmpty(iserror))
                return Json(new ApiResult() { success = false, message = iserror, data = null }, JsonRequestBehavior.AllowGet);
            decimal? Price = 0;
            string insert = objVideos.InsertVideoMoney(idVideo, ref Price);
            if (!string.IsNullOrEmpty(insert))
            {
                return Json(new ApiResult() { success = false, message = insert, data = null }, JsonRequestBehavior.AllowGet);
            }
            return Json(new ApiResult() { success = true, message = NotificationMessage.AccountAddMoney(Price), data = null }, JsonRequestBehavior.AllowGet);
        }
        public ActionResult NextSecons()
        {
            objSecon = new Library.YTB.DA.Secon.DA_Secon(User.Identity.Name, 1);
            if (objSecon.GetSecon() < 30)
                objSecon.UpsetSecon();

            return Json(new ApiResult() { success = true, message = null, data = null }, JsonRequestBehavior.AllowGet);

        }
    }
}