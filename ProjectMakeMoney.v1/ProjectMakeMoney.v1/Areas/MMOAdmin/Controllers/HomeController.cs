﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ProjectMakeMoney.v1.Areas.MMOAdmin.Controllers
{
    [Authorize(Roles = "TL0000")]
    public class HomeController : Controller
    {
        // GET: MMOAdmin/Home
        public ActionResult Index()
        {
            return View();
        }
    }
}