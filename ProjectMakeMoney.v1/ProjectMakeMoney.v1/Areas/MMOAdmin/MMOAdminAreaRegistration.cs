﻿using System.Web.Mvc;

namespace ProjectMakeMoney.v1.Areas.MMOAdmin
{
    public class MMOAdminAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "MMOAdmin";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
                "MMOAdmin-Video-index",
                "kiem-tien-online-xem-video-kiem-xu/{id}",
                new { controller = "Video", action = "Index", id = UrlParameter.Optional }
            );
            context.MapRoute(
                "MMOAdmin-SubcriberEmbedVideo-index",
                "kiem-tien-online-subcribe-kiem-xu-{id}-{idkenh}",
                new { controller= "SubcriberEmbedVideo", action = "Index", id = UrlParameter.Optional }
            );
            context.MapRoute(
                "MMOAdmin-ExChange-index",
                "chi-tiet-giao-dich",
                new { controller = "ExChange", action = "Index", id = UrlParameter.Optional }
            );
            context.MapRoute(
                "MMOAdmin-Profiles-index",
                "cai-dat-tai-khoan",
                new { controller = "Profiles", action = "Index", id = UrlParameter.Optional }
            );
            context.MapRoute(
                "MMOAdmin-Home-index",
                "dashboard",
                new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            );
            context.MapRoute(
                "MMOAdmin_default",
                "MMOAdmin/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}