﻿var currentPage_offer = 1, lastPage_offer = 1, perPage_offer = 12, total_offer = 100;
/////////////////
function changeSize(target, id) {
    if (target.value == 'Nhận tiền') {
        subscriber(id);
    }
    else {
        getidLocation(id);
        target.style.background = '#868686';
        target.value = 'Nhận tiền';
    }
}

function subscriber(subid) {
    $(".loading").show();
    $.ajax({
        url: "/MMOAdmin/SubcriberEmbedVideo/PostSubcriber",
        type: "POST",
        data: {
            idKenh: subid
        },
        success: function (data) {
            if (data.success) {
                alert(data.message)
                window.location = data.action;
            } else
                notification(data.message);
            $(".loading").hide();
        },
        error: function (jqXHR, textStatus, errorThrown) {
            console.log(jqXHR);
            console.log(textStatus);
            console.log(errorThrown);
        }
    });
}

function getidLocation(id) {
    $(".loading").show();
    $.ajax({
        url: "/SubcriberEmbedVideo/GETID/" + id,
        type: "GET",
        success: function (data) {
            window.open('https://www.youtube.com/channel/' + data.data.ChannelsID, '_blank');
            $(".loading").hide();
        },
    });
}

$(function () {
    LoadDataOffer();
    $("#example_previous_offer").on("click", function () {
        if (total_offer > 0) {
            if (currentPage_offer > 1) {
                currentPage_offer--;
            } else {
                currentPage_offer = lastPage_offer;
            }
        }
        LoadDataOffer();
    });
    $("#example_next_offer").on("click", function () {
        if (currentPage_offer * perPage_offer < total_offer) {
            currentPage_offer++;

        } else {
            currentPage_offer = 1
        }
        LoadDataOffer();
    });
});
/////////////////////////////////////////////////////////////////////////////////

function LoadDataOffer() {
    $(".loading").show();
    $.ajax({
        url: "/SubcriberEmbedVideo/GET?perPage=" + perPage_offer + "&currentPage=" + currentPage_offer,
        type: "GET",
        success: function (data) {
            total_offer = data.data.total;
            lastPage_offer = data.data.lastPage;
            currentPage_offer = data.data.currentPage;
            $("#example-curent_offer").html('<a href="#">' + data.data.currentPage + '/' + data.data.lastPage + '</a>');
            $("#example-info_offer").html('Showing ' + data.data.currentPage + ' to ' + data.data.lastPage + ' of ' + data.data.total + ' entries')
            datatableOffer(data.data.apiResult);
            $(".loading").hide();
        },
        error: function (jqXHR, textStatus, errorThrown) {
            console.log(jqXHR);
            console.log(textStatus);
            console.log(errorThrown);
        }
    });
}

function datatableOffer(data) {
    var htm = '';
    var index = 0;
    $.each(data, function (key, item) {
        index += 1;
        htm += '    <div class="col-md-3">';
        htm += '       <div class="box box-danger">';
        htm += '           <div class="box-body">';
        htm += '               <span class="mailbox-attachment-icon has-img"><img src="' + item.Image + '" alt="Attachment"></span>';
        htm += '               <div class="mailbox-attachment-info">';
        htm += '                   <a href="#" class="mailbox-attachment-name"><i class="fa fa-youtube"></i> ' + item.TenKenh + '</a>';
        htm += '                   <span class="mailbox-attachment-size">';
        htm += '                       ' + item.GiatienSub + ' xu';
        htm += '                       <a href="/kiem-tien-online-subcribe-kiem-xu-' + channelsToUserID + '-' + item.ID + '" class="btn btn-default btn-xs pull-right"><i class="fa fa-eye"></i></a>';
        htm += '                   </span>';
        htm += '               </div>';
        htm += '           </div>';
        htm += '       </div>';
        htm += '   </div>';
        if (index % 4 == 0) {
            htm += '    <div class="clearfix"></div>';
        }
    });
    $("#video-offer").html(htm);
}

function videoShow(id) {
    $.ajax({
        url: "/SubcriberEmbedVideo/GETID/" + id,
        type: "GET",
        success: function (data) {
            if (!data.success) {
                alert('kênh này đã subcriber');
                window.location = data.action;
            }
            var htm = '';
            htm += '   <div class="box-body">';
            htm += '              <iframe id="existing-iframe-example" width="100%" height="auto"';
            htm += '                       src="https://www.youtube.com/embed/' + data.data.EmbedDemo + '?enablejsapi=1&rel=0&fs=0&showinfo=0&cc_load_policy=1&iv_load_policy=3&loop=1" frameborder="0"';
            htm += '                         style="border: solid 4px #37474F"></iframe>';
            htm += '        </div>';
            htm += '       <div class="box-footer">';
            htm += '           <div id="setSecons">';
            htm += '                <span class="text-muted">' + data.data.TenKenh + ' | ' + data.data.GiatienSub + ' xu</span> | <span> xem video sau đó đăng kí</span>';
            htm += '               <span class="pull-right text-muted"><span id="h">00</span> : <span id="m">00</span> : <span id="s">00</span></span>';
            htm += '           </div>';

            htm += '       </div>';
            $("#video-view").html(htm);
            onYouTubeIframeAPIReady();
        },
    });
}

function nextSecons() {
    $.ajax({
        url: "/SubcriberEmbedVideo/NextSecons",
        type: "POST",
        success: function (data) {

        }
    });
}
/////////////////////////////////////////////////////////////////////////////////
var tag = document.createElement('script');
tag.id = 'iframe-demo';
tag.src = 'https://www.youtube.com/iframe_api';
var firstScriptTag = document.getElementsByTagName('script')[0];
firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

var player;
function onYouTubeIframeAPIReady() {
    player = new YT.Player('existing-iframe-example', {
        events: {
            'onReady': onPlayerReady,
            'onStateChange': onPlayerStateChange
        }
    });
}
function onPlayerReady(event) {
    document.getElementById('existing-iframe-example').style.borderColor = '#21ab25';
}
function changeBorderColor(playerStatus) {
    var color;
    if (playerStatus == -1) {
        color = "#37474F"; // unstarted = gray
    } else if (playerStatus == 0) {
        stop();
        color = "#FFFF00"; // ended = yellow
    } else if (playerStatus == 1) {
        start();
        color = "#33691E"; // playing = green
    } else if (playerStatus == 2) {
        stop();
        color = "#DD2C00"; // paused = red
    } else if (playerStatus == 3) {
        stop();
        color = "#AA00FF"; // buffering = purple
    } else if (playerStatus == 5) {
        stop();
        color = "#0f0"; // video cued = orange
    }
    if (color) {
        document.getElementById('existing-iframe-example').style.borderColor = color;
    }

    console.log(playerStatus);
}
function onPlayerStateChange(event) {
    changeBorderColor(event.data);

}

function stopVideo() {
    player.stopVideo();
    stop();

}

function playVideo() {
    player.playVideo();
    start();
}

function pauseVideo() {
    player.pauseVideo();
    stop();
}


var h = null; // Giờ
var m = null; // Phút
var s = null; // Giây
var timeout = null; // Timeout

function start() {
    /*BƯỚC 1: LẤY GIÁ TRỊ BAN ĐẦU*/

    if (h === null) {
        h = 0;
        m = 0;
        s = 11;
    }

    /*BƯỚC 1: CHUYỂN ĐỔI DỮ LIỆU*/
    // Nếu số giây = -1 tức là đã chạy ngược hết số giây, lúc này:
    //  - giảm số phút xuống 1 đơn vị
    //  - thiết lập số giây lại 59
    if (s === -1) {
        m -= 1;
        s = 59;
    }

    // Nếu số phút = -1 tức là đã chạy ngược hết số phút, lúc này:
    //  - giảm số giờ xuống 1 đơn vị
    //  - thiết lập số phút lại 59
    if (m === -1) {
        h -= 1;
        m = 59;
    }

    // Nếu số giờ = -1 tức là đã hết giờ, lúc này:
    //  - Dừng chương trình
    if (h == -1) {
        clearTimeout(timeout);
        var htm = ' <button type="button" class="btn btn-default btn-xs" style="margin-right: 10px" onclick="subscriber(' + IdKenh + ')"><i class="fa fa-share"></i> Nhận xu</button>';
        htm += '<button type="button" class="btn btn-default btn-xs" onclick="getidLocation(' + IdKenh + ')"><i class="fa fa-thumbs-o-up"></i> Đăng kí</button>';
        htm += '<span class="pull-right text-muted"><span id="h">00</span> : <span id="m">00</span> : <span id="s">00</span></span>';
        $("#setSecons").html(htm);
        //getidLocation(IdKenh);
        return false;
    }

    /*BƯỚC 1: HIỂN THỊ ĐỒNG HỒ*/
    document.getElementById('h').innerText = h.toString();
    document.getElementById('m').innerText = m.toString();
    document.getElementById('s').innerText = s.toString();

    /*BƯỚC 1: GIẢM PHÚT XUỐNG 1 GIÂY VÀ GỌI LẠI SAU 1 GIÂY */
    timeout = setTimeout(function () {
        s--;
        nextSecons();
        start();
    }, 1000);
}

function stop() {
    clearTimeout(timeout);
}