﻿$(function () {
    LoadDataInfoUser();
    GetNganhang();
    GetID();
});


function GetNganhang() {
    $(".loading").show();
    $.ajax({
        type: 'GET',
        url: '/MMOAdmin/Profiles/GetNganhang',
        success: function (data) {
            if (data.success) {
                html = '    <option selected="selected" value="">-- chọn ngân hàng --</option>';
                $.each(data.data, function (key, item) {
                    html += '<option value="' + item.TenNganHang + '">' + item.TenNganHang + '</option>';
                });
                $("#TenNganHang-cbo").html(html);

                $('#TenNganHang-cbo').select2();

            } else { notification(data.message); }
            $(".loading").hide();
        },
        error: function (data) {
            notification("lỗi hệ thống");
        },
    });
}

function GetID() {
    $(".loading").show();
    $.ajax({
        url: "/MMOAdmin/Profiles/GETID",
        type: "GET",
        success: function (data) {
            GetIDInput(data.data);
            $(".loading").hide();
        },
        error: function (jqXHR, textStatus, errorThrown) {
            console.log(jqXHR);
            console.log(textStatus);
            console.log(errorThrown);
        }
    });
}


function GetIDInput(item) {
    $('input[type="text"][name=first_name]').val(item.first_name);
    $('input[type="text"][name=mid_name]').val(item.mid_name);
    $('input[type="text"][name=last_name]').val(item.last_name);
    $('input[type="email"][name=Email]').val(item.Email);
    $('input[type="text"][name=DiaChi]').val(item.DiaChi);
    $('input[type="text"][name=SDT]').val(item.SDT);
    if (item.GioiTinh == 1)
        $('[name=GioiTinh]input[value="1"]').attr('checked', true);
    if (item.GioiTinh == 2)
        $('[name=GioiTinh]input[value="2"]').attr('checked', true);
    if (item.GioiTinh == 0)
        $('[name=GioiTinh]input[value="0"]').attr('checked', true);
    $('input[type="date"][name=NgaySinh]').val(Fomatdateyymmdd(item.NgaySinh));
    $('input[type="text"][name=SoCMT]').val(item.SoCMT);
    $('textarea[name=GioiThieu]').val(item.GioiThieu);
}

var frmuserput = $("#frmuserput");
frmuserput.submit(function (e) {
    $(".loading").show();
    e.preventDefault();
    $.ajax({
        type: 'POST',
        url: frmuserput.attr('action'),
        data: frmuserput.serialize(),
        success: function (data) {
            if (data.success) {
                LoadDataInfoUser();
                GetID();
                notification(data.message);
            } else { notification(data.message); }
            $(".loading").hide();
        },
        error: function (data) {
            notification("lỗi hệ thống");
        },
    });
});

var frmResetpass = $("#frmResetpass");
frmResetpass.submit(function (e) {
    $(".loading").show();
    e.preventDefault();
    $.ajax({
        type: 'POST',
        url: frmResetpass.attr('action'),
        data: frmResetpass.serialize(),
        success: function (data) {
            if (data.success) {
                frmResetpass[0].reset()
                notification(data.message);
            } else { notification(data.message); }
            $(".loading").hide();
        },
        error: function (data) {
            notification("lỗi hệ thống");
        },
    });
});


var frmTransactionRequest = $("#frmTransactionRequest");
frmTransactionRequest.submit(function (e) {
    $(".loading").show();
    e.preventDefault();
    $.ajax({
        type: 'POST',
        url: frmTransactionRequest.attr('action'),
        data: frmTransactionRequest.serialize(),
        success: function (data) {
            if (data.success) {
                frmTransactionRequest[0].reset()
                notification(data.message);
            } else { notification(data.message); }
            $(".loading").hide();
        },
        error: function (data) {
            notification("lỗi hệ thống");
        },
    });
});


var frmkhangnghi = $("#frmkhangnghi");
frmkhangnghi.submit(function (e) {
    $(".loading").show();
    e.preventDefault();
    $.ajax({
        type: 'POST',
        url: frmkhangnghi.attr('action'),
        data: frmkhangnghi.serialize(),
        success: function (data) {
            if (data.success) {
                frmkhangnghi[0].reset()
                notification(data.message);
            } else { notification(data.message); }
            $(".loading").hide();
        },
        error: function (data) {
            notification("lỗi hệ thống");
        },
    });
});