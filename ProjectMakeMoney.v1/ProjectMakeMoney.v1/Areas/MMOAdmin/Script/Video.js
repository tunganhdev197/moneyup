﻿
////
var currentPage_offer = 1, lastPage_offer = 1, perPage_offer = 12, total_offer = 100;
$(function () {
    LoadDataOffer();
    $("#example_previous_offer").on("click", function () {
        if (total_offer > 0) {
            if (currentPage_offer > 1) {
                currentPage_offer--;

            } else {
                currentPage_offer = lastPage_offer;
            }
        }
        LoadDataOffer();
    });
    $("#example_next_offer").on("click", function () {
        if (currentPage_offer * perPage_offer < total_offer) {
            currentPage_offer++;

        } else {
            currentPage_offer = 1
        }
        LoadDataOffer();
    });
});

function LoadDataOffer() {
    $(".loading").show();
    $.ajax({
        url: "/MMOAdmin/Video/GETOffer?perPage=" + perPage_offer + "&currentPage=" + currentPage_offer,
        type: "GET",
        success: function (data) {
            total_offer = data.data.total;
            lastPage_offer = data.data.lastPage;
            currentPage_offer = data.data.currentPage;
            $("#example-curent_offer").html('<a href="#">' + data.data.currentPage + '/' + data.data.lastPage + '</a>');
            $("#example-info_offer").html('Showing ' + data.data.currentPage + ' to ' + data.data.lastPage + ' of ' + data.data.total + ' entries')
            datatableOffer(data.data.apiResult);
            $(".loading").hide();
        },
        error: function (jqXHR, textStatus, errorThrown) {
            console.log(jqXHR);
            console.log(textStatus);
            console.log(errorThrown);
        }
    });
}


function datatableOffer(data) {
    var htm = '';
    var index = 0;
    $.each(data, function (key, item) {
        index += 1;
        htm += ' <div class="col-md-2">';
        htm += '              <div class="box box-primary">';
        htm += '                   <div class="box-body box-profile">';
        htm += '                      <img class="img-thumbnail" style="width: 100%;max-height: 201px;"';
        htm += '                     src="' + item.Anhdaidien + '"';
        htm += '                     alt="User profile picture">';
        htm += '                <p class="text-center">' + item.TieuDe + '</p>';
        htm += '                <p class="text-muted text-center">' + item.SoTien + ' vnd</p>';
        htm += '                <a href="/kiem-tien-online-xem-video-kiem-xu/' + item.IDVideo + '" class="my-btn btn9 btn-block btn-block" onclick="return videoShow(' + item.IDVideo + ')">';
        htm += '                    <b>Xem video</b>';
        htm += '                </a>';
        htm += '            </div>';
        htm += '        </div>';
        htm += '    </div>';
        if (index % 6 == 0) {
            htm += '    <div class="clearfix"></div>';
        }
    });
    $("#video-offer").html(htm);
}


/////////////////////////////////////////////////////////////////////////////////
//sự kiện
function videoShow(id) {
    $.ajax({
        url: "/MMOAdmin/Video/GETID/" + id,
        type: "GET",
        success: function (data) {
            if (!data.success) {
                alert('không có video để xem');
                window.location = data.action;
            }
            var htm = '';
            htm += '<div class="box-body">';
            htm += '    <iframe id="existing-iframe-example" width="100%" height="auto"';
            htm += '        src="https://www.youtube.com/embed/' + data.data.Embed + '?enablejsapi=1&rel=0&fs=0&showinfo=0&cc_load_policy=1&iv_load_policy=3&loop=1" frameborder="0"';
            htm += '    style="border: solid 4px #37474F"></iframe>';
            htm += '</div>';
            htm += '<div class="box-footer">';
            htm += '    <div id="setSecons">';
            htm += '        <span class="pull-right text-muted" id="s">00</span>';
            htm += '        <span class="pull-right text-muted" id="m">00 : </span>';
            htm += '        <span class="pull-right text-muted" id="h">00 : </span>';
            htm += '    </div>';
            htm += '    <span class="text-muted">' + data.data.TieuDe + ' | ' + data.data.SoTien + ' xu</span> ';
            htm += '</div>';
            $("#video-view").html(htm);
            onYouTubeIframeAPIReady();
        },
    });
}

function videoView(videoID) {
    $(".loading").show();
    $.ajax({
        url: "/MMOAdmin/Video/PostVideo",
        type: "POST",
        data: {
            idVideo: videoID
        },
        success: function (data) {
            $(".loading").hide();
            if (data.success) {
                alertify.confirm('Thông báo', data.message,
                    function () {
                        window.location = "/kiem-tien-online-xem-video-kiem-xu";
                    },
                    function () {
                        window.location = "/kiem-tien-online-xem-video-kiem-xu";
                    }
                );
            }
            else
                notification(data.message);

        },
        error: function (jqXHR, textStatus, errorThrown) {
            console.log(jqXHR);
            console.log(textStatus);
            console.log(errorThrown);
        }
    });
}

function nextSecons() {
    $.ajax({
        url: "/MMOAdmin/Video/NextSecons",
        type: "POST"
    });
}
/////////////////////////////////////////////////////////////////////////////////
var tag = document.createElement('script');
tag.id = 'iframe-demo';
tag.src = 'https://www.youtube.com/iframe_api';
var firstScriptTag = document.getElementsByTagName('script')[0];
firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

var player;
function onYouTubeIframeAPIReady() {
    player = new YT.Player('existing-iframe-example', {
        events: {
            'onReady': onPlayerReady,
            'onStateChange': onPlayerStateChange
        }
    });
}
function onPlayerReady(event) {
    document.getElementById('existing-iframe-example').style.borderColor = '#21ab25';
}
function changeBorderColor(playerStatus) {
    var color;
    if (playerStatus == -1) {
        color = "#37474F"; // unstarted = gray
    } else if (playerStatus == 0) {
        stop();
        color = "#FFFF00"; // ended = yellow
    } else if (playerStatus == 1) {
        start();
        color = "#33691E"; // playing = green
    } else if (playerStatus == 2) {
        stop();
        color = "#DD2C00"; // paused = red
    } else if (playerStatus == 3) {
        stop();
        color = "#AA00FF"; // buffering = purple
    } else if (playerStatus == 5) {
        stop();
        color = "#0f0"; // video cued = orange
    }
    if (color) {
        document.getElementById('existing-iframe-example').style.borderColor = color;
    }
}
function onPlayerStateChange(event) {
    changeBorderColor(event.data);

}

function stopVideo() {
    player.stopVideo();
    stop();

}

function playVideo() {
    player.playVideo();
    start();
}

function pauseVideo() {
    player.pauseVideo();
    stop();
}

var h = null; // Giờ
var m = null; // Phút
var s = null; // Giây
var timeout = null; // Timeout

function start() {
    /*BƯỚC 1: LẤY GIÁ TRỊ BAN ĐẦU*/

    if (h === null) {
        h = 0;
        m = 0;
        s = 31;
    }

    /*BƯỚC 1: CHUYỂN ĐỔI DỮ LIỆU*/
    // Nếu số giây = -1 tức là đã chạy ngược hết số giây, lúc này:
    //  - giảm số phút xuống 1 đơn vị
    //  - thiết lập số giây lại 59
    if (s === -1) {
        m -= 1;
        s = 59;
    }

    // Nếu số phút = -1 tức là đã chạy ngược hết số phút, lúc này:
    //  - giảm số giờ xuống 1 đơn vị
    //  - thiết lập số phút lại 59
    if (m === -1) {
        h -= 1;
        m = 59;
    }

    // Nếu số giờ = -1 tức là đã hết giờ, lúc này:
    //  - Dừng chương trình
    if (h == -1) {
        clearTimeout(timeout);
        $("#setSecons").html('<button type="button" class="my-btn-success" onclick="return videoView(' + idvideo + ')" class="btn btn-sm btn-default">Nhận tiền</button>');
        return false;
    }

    /*BƯỚC 1: HIỂN THỊ ĐỒNG HỒ*/
    document.getElementById('h').innerText = h.toString() + ' : ';
    document.getElementById('m').innerText = m.toString() + ' : ';
    document.getElementById('s').innerText = s.toString();

    /*BƯỚC 1: GIẢM PHÚT XUỐNG 1 GIÂY VÀ GỌI LẠI SAU 1 GIÂY */
    timeout = setTimeout(function () {
        s--;
        nextSecons();
        start();
    }, 1000);
}

function stop() {
    clearTimeout(timeout);
}