﻿
$(function () {
    $(".loading").hide();
    LoadData_Notify();
    //NotifyData();
    LoadDataChannelsToUser();
});

//function NotifyData() {
//    LoadData_Notify();
//    var timeoutHome = null; // Timeout
//    timeoutHome = setTimeout(function () {
//        NotifyData();
//        LoadDataChannelsToUser();
//    }, 5000);
//}

function titleControlModel(title) {
    $("#modal-create").modal('show');
    $("#modal-title").text(title);
    $("#btnupdate").text(title);
}

function notification(notifi) {
    if (notifi != null && notifi != "") {
        $("#alert-title").html("Thông báo");
        $("#alert-body").html(notifi);
        $("#modal-alert").modal("show");
    }
}

function formatDate(date) {
    var hours = date.getHours();
    var minutes = date.getMinutes();
    var ampm = hours >= 12 ? 'pm' : 'am';
    hours = hours % 12;
    hours = hours ? hours : 00; // the hour '0' should be '12'
    minutes = minutes < 10 ? '0' + minutes : minutes;
    var strTime = hours + ':' + minutes + ' ' + ampm;
    return (date.getMonth() + 1) + "/" + date.getDate() + "/" + date.getFullYear() + "  " + strTime;
}

function Fomatdate(createDate) {
    var resDate = createDate.substring(6, createDate.length - 2);
    var d = new Date(parseInt(resDate));
    var e = formatDate(d);
    return e;
}

//fomat số tiền
function formatMoney(amount) {
    var decimalCount = 0,
    decimal = ".",
    thousands = ",";
    try {
        decimalCount = Math.abs(decimalCount);
        decimalCount = isNaN(decimalCount) ? 2 : decimalCount;

        const negativeSign = amount < 0 ? "-" : "";

        let i = parseInt(amount = Math.abs(Number(amount) || 0).toFixed(decimalCount)).toString();
        let j = (i.length > 3) ? i.length % 3 : 0;

        return negativeSign + (j ? i.substr(0, j) + thousands : '') + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + thousands) + (decimalCount ? decimal + Math.abs(amount - i).toFixed(decimalCount).slice(2) : "");
    } catch (e) {
        console.log(e)
    }
};

//lấy param url
function GetURLParameter(sParam) {
    var sPageURL = window.location.search.substring(1);
    var sURLVariables = sPageURL.split('&');
    for (var i = 0; i < sURLVariables.length; i++) {
        var sParameterName = sURLVariables[i].split('=');
        if (sParameterName[0] == sParam) {
            return sParameterName[1];
        }
    }
}

function formatDateyymmdd(date) {
    var hours = date.getHours();
    var minutes = date.getMinutes();
    var ampm = hours >= 12 ? 'pm' : 'am';
    hours = hours % 12;
    hours = hours ? hours : 00; // the hour '0' should be '12'
    minutes = minutes < 10 ? '0' + minutes : minutes;
    var strTime = hours + ':' + minutes + ' ' + ampm;
    return date.getFullYear() + "-" + ("0" + (date.getMonth() + 1)).slice(-2) + "-" + ("0" + date.getDate()).slice(-2);
}

function Fomatdateyymmdd(createDate) {
    if (createDate == null)
        return null;
    var resDate = createDate.substring(6, createDate.length - 2);
    var d = new Date(parseInt(resDate));
    var e = formatDateyymmdd(d);
    return e;
}
////////////////////////////////////////////////////////

function LoadData_Notify() {
    $.ajax({
        url: "/MMOAdmin/Default/HistoryNotify",
        type: "GET",
        success: function (data) {
            var htm = '';
            $.each(data.data, function (key, item) {
                if (item.DaXem) {
                    htm += '<li>';
                    htm += '<a href="#" onclick="return NotifyInfo(' + item.ID + ')">';
                    htm += '<i class="fa fa-user text-light-blue"></i>' + item.TieuDe;
                    htm += '</a>';
                    htm += '</li>';
                } else {
                    htm += '<li style="background:#00000026">';
                    htm += '<a href="#"onclick="return NotifyInfo(' + item.ID + ')">';
                    htm += '<i class="fa fa-user text-light-blue"></i>' + item.TieuDe;
                    htm += '</a>';
                    htm += '</li>';
                }
            });

            $("#NotifyData").html(htm);
        },
        error: function (jqXHR, textStatus, errorThrown) {
            console.log(jqXHR);
            console.log(textStatus);
            console.log(errorThrown);
        }
    });

    $.ajax({
        url: "/MMOAdmin/Default/NotifyCount",
        type: "GET",
        success: function (data) {
            $("#notifycount").html('Bạn có ' + data.data + ' thông báo chưa xem');
            $('#notifycount2').html(data.data);
        },
        error: function (jqXHR, textStatus, errorThrown) {
            console.log(jqXHR);
            console.log(textStatus);
            console.log(errorThrown);
        }
    });
}


function NotifyInfo(id) {
    LoadData_Notify();
    $.ajax({
        url: "/MMOAdmin/Default/NotifyInfo/" + id,
        type: "GET",
        success: function (data) {
            if (data.success)
                alertify.alert('Thông báo', data.data.MoTa);
            else
                alertify.alert('Thông báo', data.message, function () { alertify.error('Ok'); });
        },
        error: function (jqXHR, textStatus, errorThrown) {
            console.log(jqXHR);
            console.log(textStatus);
            console.log(errorThrown);
        }
    });
}


function LoadDataChannelsToUser() {
    $.ajax({
        url: "/MMOAdmin/Default/GET",
        type: "GET",
        success: function (data) {
            DataChannelsToUser(data.data);
        },
        error: function (jqXHR, textStatus, errorThrown) {
            console.log(jqXHR);
            console.log(textStatus);
            console.log(errorThrown);
        }
    });
}

function DataChannelsToUser(data) {
    var htm = '';
    var index = 0;
    $.each(data, function (key, item) {
        if (item.isActive == true)
            htm += '<li><a href="/kiem-tien-online-subcribe-kiem-xu-' + item.id + '-default"> ' + item.ChannelsTitle + ' - Sẵn sàng hoạt động </a></li>';
        else
            htm += '<li><a href="#" onclick="GETCheckLoginGoogleApi()" > ' + item.ChannelsTitle + ' - Ngưng hoạt động</a></li>';
        htm += '<li class="divider"></li>';
    });
    htm += '<li><a href="#" onclick="GETCheckLoginGoogleApi()">Tạo liên kết google</a></li>';
    $("#youtobe-subcribe").html(htm);
}

function GoogleApi() {

    $(".loading").show();
    $.ajax({
        url: "/MMOAdmin/Default/GETAsync",
        type: "GET",
        success: function (data) {
            var myWindow;
            if (data.success) {
                myWindow = window.open(data.action, 'incognito', 'width = 500, height = 400,left=600,top=200');
            }
            $(".loading").hide();

        },
        error: function (jqXHR, textStatus, errorThrown) {
            console.log(jqXHR);
            console.log(textStatus);
            console.log(errorThrown);
        }
    });
}

function GETCheckLoginGoogleApi() {
    $(".loading").show();
    $.ajax({
        url: "/MMOAdmin/Default/GETCheckLogin",
        type: "GET",
        success: function (data) {
            if (!data.success) {
                alertify.alert('Thông báo', data.message);
            } else
                GoogleApi();
                //window.location.assign('/GoogleApiSample/IndexAsync');
            $(".loading").hide();
        },
        error: function (jqXHR, textStatus, errorThrown) {
            console.log(jqXHR);
            console.log(textStatus);
            console.log(errorThrown);
        }
    });
}