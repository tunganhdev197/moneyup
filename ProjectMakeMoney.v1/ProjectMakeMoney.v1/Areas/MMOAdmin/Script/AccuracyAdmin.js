﻿$(function () {
    LoadDataInfoUser();
});

function LoadDataInfoUser() {
    $(".loading").show();
    $.ajax({
        url: "/MMOAdmin/Profiles/GetInfoProfile",
        type: "GET",
        success: function (data) {
            DataInfoUser(data.data);

            $(".loading").hide();
        },
        error: function (jqXHR, textStatus, errorThrown) {
            console.log(jqXHR);
            console.log(textStatus);
            console.log(errorThrown);
        }
    });
}

function DataInfoUser(item) {
    var htm = '';
    htm += '<div class="box-body">';
    htm += '    <div class="box box-widget widget-user-2">';
    htm += '        <div class="widget-user-header">';
    htm += '            <div class="user-block">';
    htm += '                <img class="img-circle" src="/Lib/dist/img/user1-128x128.jpg" alt="User Image">';
    htm += '                <span class="username"><a href="#">' + item.full_name + '</a></span>';
    htm += '                <span class="description">7:30 PM Today</span>';
    htm += '            </div>';
    htm += '        </div>';
    htm += '        <div class="box-footer no-padding">';
    htm += '            <ul class="nav nav-stacked">';
    htm += '                <li><a href="#">Số dư <span class="pull-right badge">' + item.SoduThucte + '</span></a></li>';
    htm += '                <li><a href="#">Video đã xem <span class="pull-right badge">' + item.videocount + '</span></a></li>';
    htm += '                <li><a href="#">Kênh đã đăng kí <span class="pull-right badge">' + item.subcount + '</span></a></li>';
    if (item.TTKiemTien == true)
        htm += '                <li><a href="#">Kiếm tiền <span class="pull-right badge bg-green">Đã bật</span></a></li>';
    else
        htm += '                <li><a href="#" onclick="return btnAccuracykhtt()">Kiếm tiền <span class="pull-right badge bg-danger">Kích hoạt kiếm tiền</span></a></li>';
    if (item.TTXacThucTK == true)
        htm += '                <li><a href="#">Xác thực tài khoản <span class="pull-right badge bg-green">đã xác thực</span></a></li>';
    else
        htm += '                <li><a href="#">Xác thực tài khoản <span class="pull-right badge bg-danger">Chưa xác thực</span></a></li>';
    htm += '            </ul>';
    htm += '        </div>';
    htm += '    </div>';
    htm += '</div>';
    htm += '<div class="box-body">';
    htm += '    <strong><i class="fa fa-book margin-r-5"></i> Gửi link giới thiệu cho bạn bè</strong>';
    htm += '    <p class="text-muted">    <a href="' + item.linkgioithieu + '" target="blank">' + item.linkgioithieu + '</a> </p>';
    htm += '    <p class="text-muted">Bạn bè khi truy cập vào đường link trên để đăng ký tài khoản,.... </p>';
    htm += '    <hr>';
    htm += '</div>';
    htm += '<div class="box-body">';
    if (item.TTXacThucTK == true) {
        $('#Accuracyxttk').hide();
        //htm += '    <div class="box-body" id="Accuracyxttk">';
        //htm += '       <button type="button" class="btn btn-block btn-xs btn-danger" style="border-radius: 0px;" onclick="return btnAccuracyxttk()">Xác thực tài khoản</button>';
        //htm += '    </div>';
    }
    if (item.TTKiemTien == true) {
        $('#Accuracykhtt').hide();
        //htm += '    <div class="box-body" id="Accuracykhtt">';
        //htm += '       <button type="button" class="btn btn-block btn-xs btn-danger" style="border-radius: 0px;" onclick="return btnAccuracykhtt()">Kích hoạt kiếm tiền</button>';
        //htm += '    </div>';
    }
    //htm += '    <div class="box-body" id="Accuracymat">';
    //htm += '        <button type="button" class="btn btn-block btn-xs btn-primary" style="border-radius: 0px;" onclick="return btnAccuracymat()">Tạo mã an toàn</button>';
    //htm += '    </div>';
    htm += '</div>';
    $("#infoprofile").html(htm);
}


function btnAccuracyxttk() {
    $('#modal-Accuracy .modal-title').html('Xác thực tài khoản');
    $('#modal-Accuracy .modal-body').html('<p>Chúng rôi sẽ gửi cho bạn một email, hãy vào xác nhận nó</p>');
    $('#modal-Accuracy .modal-footer').html('<button type="button" class="btn btn-default pull-left" data-dismiss="modal">Bỏ qua</button>  <button type="button" class="btn btn-primary" onclick="return btnAccuracyxttkOK()">Đồng ý</button>');
    $('#modal-Accuracy').modal('show');
}

function btnAccuracykhtt() {
    $('#modal-Accuracy .modal-title').html('Kích hoạt kiếm tiền');
    $('#modal-Accuracy .modal-body').html('<p>Để kích hoạt kiếm tiền bạn cần chuyển </p>');
    $('#modal-Accuracy .modal-footer').html('<button type="button" class="btn btn-default pull-left" data-dismiss="modal">Bỏ qua</button>');
    $('#modal-Accuracy').modal('show');
}

function btnAccuracymat() {
    $.ajax({
        url: "/MMOAdmin/Accuracy/isEmptyMaAnToan",
        type: "POST",
        success: function (data) {
            var htm = '';
            if (data.success) {
                var footerhtm = '<button type="button" class="btn btn-sm btn-default pull-left" data-dismiss="modal">Bỏ qua</button>';
                footerhtm += '<button type="button" class="btn btn-sm btn-default pull-left" onclick="return btnForgotTheSecurityCode()">Quên mã an toàn</button>';
                footerhtm += '<button type="button" class="btn btn-sm btn-primary" onclick="return btnCreatesecuritycode()">Đồng ý</button>';
                $('#modal-Accuracy .modal-footer').html(footerhtm);

                $('#modal-Accuracy .modal-title').html('Đặt lại mã an toàn');

                htm += ' <form action="/Accuracy/createsecuritycode" class="form-horizontal" id="frmCreatesecuritycode" method="post">';
                htm += '      <div class="form-group">';
                htm += '   <label for="inputName" class="col-sm-2 control-label">Mã an toàn cũ</label>';
                htm += '   <div class="col-sm-10">';
                htm += '       <input type="text" class="form-control" id="MaAnToan" name="MaAnToan"';
                htm += '              placeholder="Mã an toàn">';
                htm += '   </div>';
                htm += '</div>';
                htm += '<div class="form-group">';
                htm += '   <label for="inputName" class="col-sm-2 control-label">Mã an toàn mới</label>';
                htm += '   <div class="col-sm-10">';
                htm += '       <input type="text" class="form-control" id="MaAnToancf" name="MaAnToancf"';
                htm += '              placeholder="Mã an toàn mới">';
                htm += '   </div>';
                htm += '</div>';
                htm += '<div class="form-group">';
                htm += '   <label for="inputName" class="col-sm-2 control-label">Mật khẩu truy cập</label>';
                htm += '   <div class="col-sm-10">';
                htm += '       <input type="password" class="form-control" id="Password" name="Password"';
                htm += '              placeholder="Mật khẩu truy cập">';
                htm += '   </div>';
                htm += '</div>';
                htm += '</form>';
            }
            else {
                var footerhtm = '<button type="button" class="btn btn-sm btn-default pull-left" data-dismiss="modal">Bỏ qua</button>';
                footerhtm += '<button type="button" class="btn btn-sm btn-primary" onclick="return btnCreatesecuritycode()">Đồng ý</button>';
                $('#modal-Accuracy .modal-footer').html(footerhtm);

                $('#modal-Accuracy .modal-title').html('Tạo mã an toàn');

                htm += ' <form action="/Accuracy/createsecuritycode" class="form-horizontal" id="frmCreatesecuritycode" method="post">';
                htm += '      <div class="form-group">';
                htm += '   <label for="inputName" class="col-sm-2 control-label">Mã an toàn</label>';
                htm += '   <div class="col-sm-10">';
                htm += '       <input type="text" class="form-control" id="MaAnToan" name="MaAnToan"';
                htm += '              placeholder="Mã an toàn">';
                htm += '   </div>';
                htm += '</div>';

                htm += '<div class="form-group">';
                htm += '   <label for="inputName" class="col-sm-2 control-label">Mật khẩu truy cập</label>';
                htm += '   <div class="col-sm-10">';
                htm += '       <input type="password" class="form-control" id="Password" name="Password"';
                htm += '              placeholder="Mật khẩu truy cập">';
                htm += '   </div>';
                htm += '</div>';
                htm += '</form>';
            }

            $('#modal-Accuracy .modal-body').html(htm);
            $('#modal-Accuracy').modal('show');
        },
        error: function (jqXHR, textStatus, errorThrown) {
            console.log(jqXHR);
            console.log(textStatus);
            console.log(errorThrown);
        }
    });


}

function btnAccuracyxttkOK() {
    $(".loading").show();
    $('#modal-Accuracy').modal('hide');
    $.ajax({
        url: "/MMOAdmin/Accuracy/Accuracyxttk",
        type: "POST",
        success: function (data) {
            if (!data.success)
                notification(data.message);
            $(".loading").hide();
        },
        error: function (jqXHR, textStatus, errorThrown) {
            console.log(jqXHR);
            console.log(textStatus);
            console.log(errorThrown);
        }
    });
}


function btnCreatesecuritycode() {

    $(".loading").show();
    var MaAnToan = $('input[type="text"][name=MaAnToan]').val();
    var MaAnToancf = $('input[type="text"][name=MaAnToancf]').val();
    var Password = $('input[type="password"][name=Password]').val();
    $.ajax({
        type: 'POST',
        url: '/MMOAdmin/Accuracy/createsecuritycode',
        data: {
            MaAnToan: MaAnToan,
            MaAnToancf: MaAnToancf,
            Password: Password
        },
        success: function (data) {
            $(".loading").hide();
            if (data.success) {
                btnAccuracymat();
                $('input[type="text"][name=MaAnToan]').val('');
                $('input[type="text"][name=MaAnToancf]').val('');
                $('input[type="text"][name=Password]').val('');
            }
            alertify.alert('Thông báo', data.message);
        },
        error: function (data) {
            notification("lỗi hệ thống");
        },
    });
}

function btnForgotTheSecurityCode() {

    alertify.confirm("Quên mã an toàn", "Chúng tôi sẽ làm mới lại mã an toàn của bạn và gửi đến Email mà bạn đã đăng kí trên hệ thống của chúng tôi... <p>Hãy OK để tiếp tục</p><p>Hoặc CANCEL để hủy bỏ</p>",
                          function () {
                              promptForgotTheSecurityCode();
                          },
                          function () {
                          });
}

function promptForgotTheSecurityCode() {
    alertify.prompt('Quên mã an toàn', 'Nhập mật khẩu của bạn', ''
                                           , function (evt, value) {
                                               $(".loading").show();
                                               $.ajax({
                                                   url: "/MMOAdmin/Accuracy/ForgotTheSecurityCode",
                                                   type: "PUT",
                                                   data: {
                                                       password: value
                                                   },
                                                   success: function (data) {
                                                       $(".loading").hide();
                                                       if (data.success)
                                                           alertify.alert('Thông báo', data.message);
                                                       else {
                                                           alertify.confirm("Quên mã an toàn", data.message,
                                                               function () {
                                                                   promptForgotTheSecurityCode();
                                                               },
                                                               function () {
                                                               });
                                                       }
                                                   },
                                                   error: function (jqXHR, textStatus, errorThrown) {
                                                       console.log(jqXHR);
                                                       console.log(textStatus);
                                                       console.log(errorThrown);
                                                   }
                                               });
                                           },
                                           function () { });
}