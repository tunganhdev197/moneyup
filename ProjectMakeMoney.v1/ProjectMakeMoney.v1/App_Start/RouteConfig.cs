﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace ProjectMakeMoney.v1
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
                name: "LoginHome-Index",
                url: "login",
                defaults: new { controller = "LoginHome", action = "Index", id = UrlParameter.Optional },
                namespaces: new[] { "ProjectMakeMoney.v1.Controllers" }
            );
            routes.MapRoute(
                name: "Registration-regisTration",
                url: "regisTration-post",
                defaults: new { controller = "Registration", action = "regisTration", id = UrlParameter.Optional },
                namespaces: new[] { "ProjectMakeMoney.v1.Controllers" }
            );
            routes.MapRoute(
                name: "Registration-Index",
                url: "registration/{magioithieu}",
                defaults: new { controller = "Registration", action = "Index", id = UrlParameter.Optional },
                namespaces: new[] { "ProjectMakeMoney.v1.Controllers" }
            );

            routes.MapRoute(
                name: "ForgotPassword-ChangeThePassword",
                url: "change-the-password-{username}-{otp}",
                defaults: new { controller = "ForgotPassword", action = "ChangeThePassword", id = UrlParameter.Optional },
                namespaces: new[] { "ProjectMakeMoney.v1.Controllers" }
            );

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional },
                namespaces: new[] { "ProjectMakeMoney.v1.Controllers" }
            );
        }
    }
}
