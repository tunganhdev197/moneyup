﻿using Library.YTB.BO.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;

namespace ProjectMakeMoney.v1.CustomRoleProvider
{
    public class SiteRole : RoleProvider
    {
        public override string ApplicationName
        {
            get
            {
                throw new NotImplementedException();
            }

            set
            {
                throw new NotImplementedException();
            }
        }

        public override void AddUsersToRoles(string[] usernames, string[] roleNames)
        {
            throw new NotImplementedException();
        }

        public override void CreateRole(string roleName)
        {
            throw new NotImplementedException();
        }

        public override bool DeleteRole(string roleName, bool throwOnPopulatedRole)
        {
            throw new NotImplementedException();
        }

        public override string[] FindUsersInRole(string roleName, string usernameToMatch)
        {
            throw new NotImplementedException();
        }

        public override string[] GetAllRoles()
        {
            throw new NotImplementedException();
        }

        public override string[] GetRolesForUser(string username)
        {
            //throw new NotImplementedException();
            string[] result = { "role"};
            try
            {
                AppDbContext db = new AppDbContext();
                if (db.KhachHangs.Where(x => x.UserName == username && x.IsActive == true).Count() > 0)
                {
                    int RoleID = db.KhachHangs.Where(x => x.UserName == username).Select(x => x.Role_id).FirstOrDefault();

                    result = db.Decentralizations.Where(x => x.RoleID == RoleID && x.Role.isDelete == false).Select(x => x.Authority.Code).ToArray();
                }
                else
                {
                    if (db.Users.Where(x => x.username == username && x.active == true).Count() > 0)
                    {
                        int? RoleID = db.Users.Where(x => x.username == username).Select(x => x.role_id).FirstOrDefault();

                        result = db.Decentralizations.Where(x => x.RoleID == RoleID && x.Role.isDelete == false).Select(x => x.Authority.Code).ToArray();
                    }
                }
            }
            catch
            {
                return result;
            }
            return result;
        }

        public override string[] GetUsersInRole(string roleName)
        {
            throw new NotImplementedException();
        }

        public override bool IsUserInRole(string username, string roleName)
        {
            throw new NotImplementedException();
        }

        public override void RemoveUsersFromRoles(string[] usernames, string[] roleNames)
        {
            throw new NotImplementedException();
        }

        public override bool RoleExists(string roleName)
        {
            throw new NotImplementedException();
        }
    }
}