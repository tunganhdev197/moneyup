﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ProjectMakeMoney.v1.Controllers
{
    public class HomeController : Controller
    {
        // GET: Home
        public ActionResult Index()
        {
            ViewBag.SiteName = System.Configuration.ConfigurationSettings.AppSettings["Sitename"].ToString();
            ViewBag.Host= System.Configuration.ConfigurationSettings.AppSettings["localhost"].ToString();
            return View();
        }

        public ActionResult Default()
        {
            return View();
        }
    }
}