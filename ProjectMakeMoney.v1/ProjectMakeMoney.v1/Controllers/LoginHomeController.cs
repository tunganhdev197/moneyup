﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Library.YTB.BO.Models;
using System.Data.Entity;
using System.Data;
using System.Web.Security;

namespace ProjectMakeMoney.v1.Controllers
{
    public class LoginHomeController : Controller
    {
        private string message = string.Empty;
        private Library.YTB.DA.Login.LoginHome objloginHome = null;
        // GET: LoginHome
        public ActionResult Index()
        {
            if (User.Identity.IsAuthenticated)
            {
                objloginHome = new Library.YTB.DA.Login.LoginHome(User.Identity.Name);
                if (objloginHome.UserGetLogin() == 2)
                    return  Content("<script>window.location.assign('/dashboard');</script>");
                if (objloginHome.UserGetLogin() == 1)
                    return RedirectToRoute("Admin-Home-Index");
            }
            return View();
        }

        [HttpPost]
        public ActionResult LoginUser(string username, string password)
        {
            objloginHome = new Library.YTB.DA.Login.LoginHome(string.Empty);
            if (!objloginHome.login(username, password, ref message))
                return Json(new { success = false, message = message }, JsonRequestBehavior.AllowGet);
            return Json(new { success = true, action = "/dashboard", message = "Đăng nhập thành công" }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult Logout()
        {
            FormsAuthentication.SignOut();
            Response.Cookies.Remove(FormsAuthentication.FormsCookieName);
            Response.Cache.SetExpires(DateTime.Now.AddSeconds(-1));
            HttpCookie cookie = HttpContext.Request.Cookies[FormsAuthentication.FormsCookieName];
            if (cookie != null)
            {
                cookie.Expires = DateTime.Now.AddDays(-1);
                Response.Cookies.Add(cookie);
            }

            return RedirectToAction("Index");
        }
    }
}