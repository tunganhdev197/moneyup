﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.Entity;
using System.Data;

namespace ProjectMakeMoney.v1.Controllers
{
    public class ForgotPasswordController : Controller
    {
        private Library.YTB.DA.Login.ForgotPassword objForgotPassword = new Library.YTB.DA.Login.ForgotPassword();
        private Library.YTB.Utils.ApiResult apiResult = null;
        // GET: ForgotPassword
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult ChangeThePassword(string username, string otp)
        {
            if (!objForgotPassword.CheckYeucaudoimatkhau(username, otp))
                return RedirectToAction("Index", "Home");
            ViewBag.Username = username;
            ViewBag.OTP = otp;
            return View();
        }
        [HttpPost]
        public ActionResult Post(string username, string Email)
        {
            apiResult = objForgotPassword.EmailRequestCode(username, Email);
            return Json(apiResult, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult ChangePassword(string OTP, string username, string Password, string PasswordComfirm)
        {
            apiResult = objForgotPassword.ChangePassword(OTP, username, Password, PasswordComfirm);
            return Json(apiResult, JsonRequestBehavior.AllowGet);
        }
    }
}