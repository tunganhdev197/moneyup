﻿using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Mvc;
using Google.Apis.Auth.OAuth2.Mvc;
using Library.YTB.BO.Models;

namespace ProjectMakeMoney.v1.Controllers
{
    [Authorize(Roles = "TL0000")]
    public class GoogleApiSampleController : Controller
    {
        private Library.YTB.DA.ChannelsToUser.DA_ChannelsToUser objChannelsToUser = null;
        private Google.Apis.Auth.OAuth2.Mvc.FlowMetadata Flow = null;
        private int? ProjectID = null;
        private AppDbContext db = new AppDbContext();

        public GoogleApiSampleController()
        {
            this.ProjectID = Library.YTB.Utils.Utils.MyProjectID();
            this.Flow = GoogleAuth2.Flow.MyProject.Flow(this.ProjectID);
        }
        public async Task<ActionResult> IndexAsync(CancellationToken cancellationToken)
        {
            objChannelsToUser = new Library.YTB.DA.ChannelsToUser.DA_ChannelsToUser(User.Identity.Name);
            var result = await new AuthorizationCodeMvcApp(this, this.Flow).
                AuthorizeAsync(cancellationToken);

            if (result.Credential != null)
            {
                string user = this.Session["user"].ToString();
                string ChangeID = null;
                string Title = null;

                bool bolChannel = GoogleAuth2.Flow.YoutubeData.GetChannel(result.Credential.Token.AccessToken, ref ChangeID, ref Title);

                if (string.IsNullOrEmpty(ChangeID))
                {
                    await result.Credential.RevokeTokenAsync(cancellationToken);
                    return Content("<script>alert('google liên kết cần được đăng kí kênh trên youtobe');window.location.assign('/dashboard');</script>");
                }
                if (!objChannelsToUser.UpsetChanelsToUser(this.ProjectID, ChangeID, Title, result.Credential.Token.AccessToken, result.Credential.Token.RefreshToken))
                {
                    await result.Credential.RevokeTokenAsync(cancellationToken);
                    return Content("<script>alert('lỗi hệ thống, vui lòng thử lại'); window.location.assign('/dashboard');</script>");
                }
                return Content("<script>alert('liên kết thành công');window.location.assign('/dashboard');</script>");
            }
            else
            {
                return new RedirectResult(result.RedirectUri);
                //return Content("<script>window.open('"+ result.RedirectUri + "', 'Google', 'width = 600, height = 400');</script>");
                //return Content("<script>windows.create({'url': " + result.RedirectUri + ", 'incognito': true});</script>");
            }
        }


    }
}