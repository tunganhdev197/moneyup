﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ProjectMakeMoney.v1.Controllers
{
    public class RegistrationController : Controller
    {
        private Library.YTB.DA.Login.Registration objRegistration = new Library.YTB.DA.Login.Registration();
        private Library.YTB.Utils.ApiResult apiResult = null;
        // GET: Registration
        public ActionResult Index(string magioithieu)
        {
            ViewBag.Magioithieu = magioithieu;
            return View();
        }

        [HttpGet]
        public ActionResult activated(string stractiveed)
        {
            objRegistration.CheckActivated(stractiveed);
            return RedirectToAction("Index", "Home");
        }

        [HttpPost]
        public ActionResult regisTration(string MaGioiThieu, string full_name, string Email, string Username, string Password, string comfirmpassword)
        {
            apiResult = objRegistration.regisTration(MaGioiThieu, full_name, Email, Username, Password, comfirmpassword);
            return Json(apiResult, JsonRequestBehavior.AllowGet);
        }
    }
}