﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ProjectMakeMoney.v1.Controllers
{
    public class AuthCallbackController : Google.Apis.Auth.OAuth2.Mvc.Controllers.AuthCallbackController
    {
        private Library.YTB.BO.Models.AppDbContext db = new Library.YTB.BO.Models.AppDbContext();
        private readonly Google.Apis.Auth.OAuth2.Mvc.FlowMetadata Flow;
        private int? ProjectID = null;
        public AuthCallbackController()
        {
            this.ProjectID = Library.YTB.Utils.Utils.MyProjectID();
            if (this.ProjectID == 1)
                this.Flow = new GoogleAuth2.Flow.Project1.AppFlowMetadata();
            if (this.ProjectID == 2)
                this.Flow = new GoogleAuth2.Flow.Project1.AppFlowMetadata();
        }
        protected override Google.Apis.Auth.OAuth2.Mvc.FlowMetadata FlowData
        {
            get { return Flow; }
        }
    }
}